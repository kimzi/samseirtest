if(__DEV__) {
  import('./src/ReactotronConfig').then(() => console.log('Reactotron Configured'))
}
navigator.geolocation = require('@react-native-community/geolocation');
import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import AppNavigator from "./src/utilities/navigations/Navigations";
import { Provider } from "react-redux";
import store from "./src/store";
import MenuDrawerNavigation from "./src/utilities/navigations/MenuDrawerNavigation";
// import Root from './src/utilities/navigations/SwitchNavigator'
// import reducer from './src/reducers/index'
// import { createStore } from 'redux'

// const store = createStore(reducer)
// alert(store.getState())
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <MenuDrawerNavigation />
      </Provider>
    );
  }
}