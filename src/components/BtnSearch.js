import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default class BtnSearch extends Component {
  render() {
    return (
      <View style={styles.btnSearch}>
        <Text style={styles.btnText}>جستجو</Text>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  btnSearch: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    borderRadius: 30,
    backgroundColor: "#F63323"
  },
  btnText: {
    color: "#fff",
    textAlign: "center",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2.6),
    includeFontPadding: false
  }
});
