import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import LinearGradient from "react-native-linear-gradient";
export default class BtnGradient extends Component {
  render() {
    return (
      <LinearGradient
        start={{
          x: 0,
          y: 0
        }}
        end={{
          x: 1,
          y: 0.5
        }}
        colors={["#F63323", "#15479F"]}
        style={styles.linearGradient}
      >
        <View style={styles.buttonContainer}>
          <Text style={styles.okBotton}>{this.props.type}</Text>
        </View>
      </LinearGradient>
    );
  }
}

export const styles = StyleSheet.create({
  linearGradient: {
    width: "100%",
    height: "100%",
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  okBotton: {
    color: "#fff",
    textAlign: "center",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2.9)
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center"
  }
});
