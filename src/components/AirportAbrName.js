import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default class AirportAbrName extends Component {
  render() {
    return (
      <View
        style={{
          alignItems: "center",
          width: "28%",
          // backgroundColor: "red",
          height: "100%",
          justifyContent: "flex-start"
          // justifyContent: "flex-start"
        }}
      >
        <Text style={styles.time}>{this.props.time}</Text>
        <Text style={styles.airport}>{this.props.airport}</Text>
        <Text style={styles.city}>{this.props.city}</Text>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  time: {
    fontFamily: "Lato-Black",
    fontSize: RFPercentage(2.7),
    color: "#55535b",
    textAlign: "center",
    includeFontPadding: false,
  },
  airport: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(2.7),
    color: "#8A8795",
    textAlign: "center",
    includeFontPadding: false,
  },
  city: {
    fontFamily: "Montserrat-Medium",
    includeFontPadding: false,
    fontSize: RFPercentage(1.6),
    color: "#8A8795",
    textAlign: "center"
  }
});
