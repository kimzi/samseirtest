import React, { Component } from "react";
import { StyleSheet, FlatList, Text, View } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default class PassengersCountMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      array: [],
      number: 9,
      itemFontColor: "#333",
      itemPressed: 0
    };
  }

  typeSelected(value) {
    console.log("valu", value);
    this.setState({
      itemPressed: value
    });
    // console.log("this.state.itemPressed", this.state.itemPressed);
  }

  componentDidMount() {
    let index = 0;
    let len = this.state.number;
    let count;
    for (index; index < len; index++) {
      switch (index) {
        case 0:
          count = "اول";
          break;
        case 1:
          count = "دوم";
          break;
        case 2:
          count = "سوم";
          break;
        case 3:
          count = "چهارم";
          break;
        case 4:
          count = "پنجم";
          break;
        case 5:
          count = "ششم";
          break;
        case 6:
          count = "هفتم";
          break;
        case 7:
          count = "هشتم";
          break;
        case 8:
          count = "نهم";
          break;
        default:
          count = item + 1;
      }
      this.state.array.push({ index, count });
    }
    this.setState({ array: this.state.array });
    // console.log(this.state.array);
  }

  render() {
    return (
      <View>
        <FlatList
          style={styles.flatList}
          inverted="true"
          horizontal="true"
          data={this.state.array}
          extraData={this.state}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => (
            <View style={{ marginHorizontal: 30 }}>
              <Text
                style={[
                  styles.menuOptionText,
                  {
                    color:
                      this.state.itemPressed === item.index ? "white" : "grey"
                  }
                ]}
                onPress={() => this.typeSelected(item.index)}
              >
                مسافر {item.count}
              </Text>
            </View>
          )}
        />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  flatList: {
    backgroundColor: "#0D3477"
  },
  menuOptionText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(2.1),
    marginBottom: 5
  }
});
