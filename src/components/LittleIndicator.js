import {
    View,
    Animated,
    Easing
  } from "react-native";
  import React, { Component } from "react";
  import Icon from 'react-native-vector-icons/Entypo';
  export default class LittleIndicator extends Component {
    constructor(props) {
      super(props);
      this.state = {
  
      };
      this.yellow = new Animated.Value(0);
      this.red = new Animated.Value(0);
      this.blue = new Animated.Value(0);
    }
    componentDidMount() {
      // Animated.timing(
      //   this.blue,
      //   {
      //     toValue: 100,
      //     easing: Easing.linear,
      //     duration: 5000
      //   },
      // //   this.setState({ animationStart: 1 })
      // ).start();
  
  
  
  
      Animated.sequence([
          Animated.timing(this.blue, {
            toValue: 50,
            easing: Easing.linear,
            duration: 1000
          }),
  
          Animated.timing(this.blue, {
            toValue: 0,
            easing: Easing.linear,
            duration: 1000
          })
        ]).start();
  
    }
  
  
    componentDidMount() {
      this.animation();
      setTimeout(() => {
        this.redAnimation()
      }, 200)
      setTimeout(() => {
        this.yellowAnimation() 
      }, 400)
  
         
  
      }
  
    animation() {
      this.blue.setValue(0);
      Animated.sequence([
          Animated.timing(this.blue, {
            toValue: 50,
            easing: Easing.linear,
            duration: 400
          }),
  
          Animated.timing(this.blue, {
            toValue: 0,
            easing: Easing.linear,
            duration: 400
          })
        ]).start(() => this.animation());
    }
  
    redAnimation() {
      this.red.setValue(0);
      Animated.sequence([
          Animated.timing(this.red, {
            toValue: 50,
            easing: Easing.linear,
            duration: 400
          }),
  
          Animated.timing(this.red, {
            toValue: 0,
            easing: Easing.linear,
            duration: 400
          })
        ]).start(() => this.redAnimation());
    }
  
    yellowAnimation() {
      this.yellow.setValue(0);
      Animated.sequence([
          Animated.timing(this.yellow, {
            toValue: 50,
            easing: Easing.linear,
            duration: 400
          }),
  
          Animated.timing(this.yellow, {
            toValue: 0,
            easing: Easing.linear,
            duration: 400
          })
        ]).start(() => this.yellowAnimation());
    }
  
  
  
  
    render() {
      return (
          <View style={{flex:1 , justifyContent : 'center' , alignItems : 'center'}}>
        <View style={{ flexDirection:'row' , width : 200 , height:200,justifyContent : 'center' , alignItems : 'center'}}>
          <Animated.View  style={{marginBottom : this.blue }}>
            <Icon name='circle' color='#15479F' size={30}/>
          </Animated.View>
  
          <Animated.View style={{marginBottom : this.red }}>
            <Icon  name='circle' color='#E7342A' size={30}/>
          </Animated.View>
  
          <Animated.View style={{marginBottom : this.yellow }}>
            <Icon name='circle' color ='#FFBA00' size={30}/>
          </Animated.View>
        </View>
        </View>
      );
    }
  }
  