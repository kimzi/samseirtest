import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue }  from "react-native-responsive-fontsize";
const Icon = createIconSetFromFontello(fontelloConfig);
export default class AppbarTitle extends Component {

  showPassengerModal(passengerModalVisible) {
    this.props.showPassengerModal(passengerModalVisible);
  }

  navigateTo(){
    this.props.navigateTo()
  }

  render() {
    return (
      <View style={styles.pageTitleContainer}>
        <View style={styles.iconContainer}>
          <Icon
            name="back"
            style={[styles.backIcon, {color: this.props.backIconColor}]}
            onPress={() => this.navigateTo()}
          />
        </View>

        {/* <View style={styles.settingContainer}>
          <Icon
            name={this.props.menuIcon}
            style={styles.backIcon}
            onPress={() => this.showPassengerModal(true)}
          />
        </View> */}

        <View style={styles.titleContainer}>
          <Text style={[styles.pageTitleStyle, {color: this.props.titleTextColor}]}>{this.props.pageTitle}</Text>
        </View>
        <View />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  pageTitleContainer: {
    justifyContent: "center",
    flexDirection: "row",
    width: "100%",
    // backgroundColor: "yellow",
    aspectRatio: 9
  },
  titleContainer: {
    justifyContent: "center"
    // backgroundColor: "white"
  },
  pageTitleStyle: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.7),
    includeFontPadding: false
    // backgroundColor: "black"
  },
  iconContainer: {
    justifyContent: "center",
    position: "absolute",
    start: "5%",
    height: "100%"
    // backgroundColor: "orange"
  },
  settingContainer: {
    justifyContent: "center",
    position: "absolute",
    end: "5%",
    height: "100%"
    // backgroundColor: "orange"
  },
  backIcon: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2),
    // color: "#ffffff",
    // textAlign: "center",
    // alignItems: "center",
    includeFontPadding: false,
    padding: 5
    // backgroundColor: "blue"
  }
});
