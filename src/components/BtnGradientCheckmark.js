import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const Icon = createIconSetFromFontello(fontelloConfig);
import LinearGradient from "react-native-linear-gradient";
export default class BtnGradient extends Component {
  render() {
    return (
      <LinearGradient
        start={{
          x: 0,
          y: 0
        }}
        end={{
          x: 1,
          y: 0.5
        }}
        colors={["#F63323", "#15479F"]}
        style={styles.linearGradient}
      >
        <View style={styles.buttonContainer}>
          <Icon name='checkmark' style={styles.okBotton}/>
        </View>
      </LinearGradient>
    );
  }
}

export const styles = StyleSheet.create({
  linearGradient: {
    width: "100%",
    height: "100%",
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  okBotton: {
    color: "#ffffff",
    fontSize: RFPercentage(5)
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center"
  }
});
