import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Image,
  TouchableOpacity,
  PixelRatio,
  FlatList,
  AsyncStorage
} from "react-native";
import { withNavigation } from "react-navigation";
import { createIconSetFromFontello } from "react-native-vector-icons";
const Icon = createIconSetFromFontello(fontelloConfig);
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import AirlinePic from "../Images/TS.png";
import { PIXEL_RATIO } from "../constants/DeviceParameters";
import TicketDetails from "../pages/TicketDetails";
import AirportAbrName from "./AirportAbrName.js";
import TicketDivider from "./TicketDivider.js";
import TicketArrow from "./TicketArrow.js";
import {REQUEST_INDEX} from '../constants/UserParams'
class TwoWayListItemTicketContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 20
        }}
      />
    );
  };
  async savetoCache(reqId, index) {
    try {
      // await AsyncStorage.multiSet([
      //   [REQUEST_ID, reqId],
      //   [REQUEST_INDEX, index]
      // ]);
      await AsyncStorage.setItem(REQUEST_INDEX, index);
    } catch (error) {
      console.warn(error);
    }
  }


  render() {
    console.log(this.props.type , 'type is =======')
    return (
      <FlatList
        style={{
          padding: 0
          //  backgroundColor: "yellow"
        }}
        data={this.props.type}
        extraData={this.state}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={this.renderSeparator}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.ticketContainer}
            // onPress={() =>
            //   this.props.navigation.navigate("TwoWayTicketDetails", {
            //     title: item
            //   })
            // }
            onPress={
              () => (
                this.props.navigation.navigate("TwoWayTicketDetails", {
                  Req_ID: this.props.reqId,
                  Index: item.Index,
                  airlineName: item.AirlineName,
                  airlineLogo: item.AirlineLogo
                }),
                this.savetoCache(this.props.reqId, item.Index)
              )
              // console.warn(this.props.reqId)
            }

          >
            <View style={styles.ticket}>
              <View style={styles.flightPack}>
                <View
                  style={{
                    width: "60%",
                    marginStart: 4,
                    justifyContent: "space-around",
                    alignItems: "center"
                  }}
                >
                  <Image
                    source={{uri : item.AirlineLogo}}
                    style={{
                      transform: [{ rotate: "0deg" }] , width:32, height:32
                      // position: "absolute",
                      // top: "63%"
                    }}
                  />
                  <Text
                    style={{
                      fontFamily: "Montserrat-Bold",
                      fontSize: RFPercentage(1.5),
                      // position: "absolute",
                      // top: "35%",
                      color: "#55535b",
                    }}
                  >
                    {item.AirlineName}
                  </Text>
                </View>
              </View>

              <TicketDivider />

              <View style={styles.ticketSummary}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "34%"
                  }}
                >
                  <AirportAbrName
                    time={item.Leave.DepartureTime}
                    airport={item.Leave.Origin}
                    city={item.Leave.OriginCity}
                  />

                  <TicketArrow
                    flightDuration={item.Leave.Duration}
                    stopCount={item.Leave.Stop}
                  />

                  <AirportAbrName
                    time={item.Leave.ArrivalTime}
                    airport={item.Leave.Destination}
                    city={item.Leave.DestinationCity}
                  />
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "34%",
                    marginTop: 15
                  }}
                >
                  <AirportAbrName
                    time={item.Return.DepartureTime}
                    airport={item.Return.Origin}
                    city={item.Return.OriginCity}
                  />

                  <TicketArrow
                    flightDuration={item.Return.Duration}
                    stopCount={item.Return.Stop}
                  />

                  <AirportAbrName
                    time={item.Return.ArrivalTime}
                    airport={item.Return.Destination}
                    city={item.Return.DestinationCity}
                  />
                </View>

                <View style={styles.priceContainer}>
                  <View style={styles.singleTicketPrice}>
                    <Text style={styles.singleTicketText}>{item.Adult_Title}</Text>
                    <Text style={styles.singleTicketPriceText}>
                    {item.Adult_Price}
                    </Text>
                  </View>
                  <View style={styles.totalTicketPrice}>
                    <Text style={styles.totalTicketText}>{item.Total_Title}</Text>
                    <Text style={styles.totalTicketPriceText}>
                    {item.Total_Price}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={[styles.ticketTypeContainer , {backgroundColor : !item.IsCharter ? '#DCE1FF' : '#AFFDFF'}]}>
              <Text style={[styles.ticketTypeText , {color : !item.IsCharter ? '#3B2889' : '#1098AE'}]}>{!item.IsCharter ? 'سیستمی' : 'چارتر'}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    );
  }
}
export default withNavigation(TwoWayListItemTicketContainer);

export const styles = StyleSheet.create({
  ticketTypeText: {
    transform: [{ rotate: "90deg" }],
    // justifyContent: "center",
    // alignItems: "center",
    width: 60,
    fontSize: RFPercentage(1.6),
    paddingTop: 0,
    paddingRight: 4,
    color: "#3B2889",
    includeFontPadding: false,
    textAlign : 'center'
  },
  ticketContainer: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    width: "100%",
    height: PIXEL_RATIO >= 3 ? "100%" : "42%",
    flex: 1,
    marginBottom: 0
  },
  flightPack: {
    width: "27%",
    alignItems: "center",
    justifyContent: "center"
  },
  ticket: {
    width: "90%",
    // height: "78%",
    backgroundColor: "#ffffff",
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 10,
    flex: 1,
    alignSelf: "center"
    // marginBottom: 24
  },
  airLineLogoContainer: {
    width: "5%",
    justifyContent: "center",
    alignItems: "center"
  },
  dividerContainer: {
    flexDirection: "column",
    width: "1%",
    alignItems: "center",
    justifyContent: "space-between"
  },
  ticketSummary: {
    height: "100%",
    padding: 16,
    alignContent: "center",
    flex: 1,
    justifyContent: "space-around"
  },
  ticketTypeContainer: {
    width: 20,
    height: 60,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: "#DCE1FF",
    position: "absolute",
    left: "5%",
    zIndex: 1,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    top: "35%"
  },
  price: {
    fontSize: RFPercentage(2),
    fontFamily: "Sahel-Bold",
    color: "#F09819"
  },
  numberOfPeople: {
    writingDirection: "rtl",
    fontSize: RFPercentage(2),
    fontFamily: "Sahel",
    color: "#F09819",
    marginTop: -10
  },
  destinationCountry: {
    fontSize: RFPercentage(1.8),
    fontFamily: "Montserrat-Bold",
    color: "#707070",
    marginTop: 8
  },
  time: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(1.9),
    color: "#8A8795"
  },
  city: {
    fontFamily: "Lato-Black",
    fontSize: RFPercentage(2.5),
    color: "#55535b"
  },
  duration: {},
  textDuration: {
    color: "#8A8795",
    fontSize: RFPercentage(1.8)
  },
  infoDuration: {
    color: "#8A8795",
    fontSize: RFPercentage(1.4)
  },
  airlineName: {
    color: "#313841",
    fontFamily: "Montserrat-Medium",
    zIndex: 1,
    fontSize: RFPercentage(1.8)
  },
  singleTicketPrice: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  totalTicketPrice: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  singleTicketText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(1.7),
    color: "#55535b",
    includeFontPadding: false
  },
  singleTicketPriceText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(1.7),
    color: "#55535b",
    includeFontPadding: false
  },
  totalTicketText: {
    color: "#F09819",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2),
    includeFontPadding: false
  },
  totalTicketPriceText: {
    color: "#F09819",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2),
    includeFontPadding: false
  },
  priceContainer: {
    marginTop: 8
  }
});
