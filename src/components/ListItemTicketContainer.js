import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Image,
  TouchableOpacity,
  PixelRatio,
  AsyncStorage,
  FlatList
} from "react-native";
import { withNavigation } from "react-navigation";
import { createIconSetFromFontello } from "react-native-vector-icons";
const Icon = createIconSetFromFontello(fontelloConfig);
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import AirlinePic from "../Images/TS.png";
import { PIXEL_RATIO } from "../constants/DeviceParameters";
import TicketDetails from "../pages/TicketDetails";
import AirportAbrName from "./AirportAbrName.js";
import TicketArrow from "./TicketArrow.js";
import TicketDivider from "./TicketDivider.js";
import { REQUEST_ID, REQUEST_INDEX } from "../constants/UserParams";

class ListItemTicketContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 20
        }}
      />
    );
  };

  // renderSeparator = ({leadingItem, section})=>{
  //   if (section.noSeparator || !leadingItem.noSeparator)
  //     return null;
  //   return <Separator />;
  // };
  async savetoCache(reqId, index) {
    try {
      // await AsyncStorage.multiSet([
      //   [REQUEST_ID, reqId],
      //   [REQUEST_INDEX, index]
      // ]);
      await AsyncStorage.setItem(REQUEST_INDEX, index);
    } catch (error) {
      console.warn(error);
    }
  }

  render() {
    return (
      <FlatList
        style={{
          padding: 0
        }}
        data={this.props.type}
        contentContainerStyle={{ paddingTop: 20, paddingBottom: 20 }}
        ItemSeparatorComponent={this.renderSeparator}
        extraData={this.state}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <TouchableOpacity
            style={styles.ticketContainer}
            onPress={
              () => (
                this.props.navigation.navigate("OneWayTicketDetails", {
                  Req_ID: this.props.reqId,
                  Index: item.Index,
                  airlineName: item.AirlineName,
                  airlineLogo: item.AirlineLogo
                }),
                this.savetoCache(this.props.reqId, item.Index)
              )
              // console.warn(this.props.reqId)
            }
          >
            <View style={styles.ticket}>
              <View style={styles.flightPack}>
                <View
                  style={{
                    width: "60%",
                    marginStart: 4,
                    justifyContent: "space-around",
                    alignItems: "center"
                  }}
                >
                  <Image
                    source={{ uri: item.AirlineLogo }}
                    style={{
                      transform: [{ rotate: "0deg" }],
                      width: 32,
                      height: 32
                      // position: "absolute",
                      // top: "34%"
                    }}
                  />
                  <Text
                    style={{
                      transform: [{ rotate: "0deg" }],
                      fontFamily: "Montserrat-Bold",
                      fontSize: RFPercentage(1.5),
                      textAlign: "center",
                      // position: "absolute",
                      // top: "60%",
                      color: "#55535b"
                    }}
                  >
                    {item.AirlineName}
                  </Text>
                </View>
              </View>

              <TicketDivider />

              <View style={styles.ticketSummary}>
                <View
                  style={{
                    flexDirection: "row-reverse",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "60%"
                  }}
                >
                  <AirportAbrName
                    time={item.Leave.ArrivalTime}
                    airport={item.Leave.Destination}
                    city={item.Leave.DestinationCity}
                  />

                  <TicketArrow
                    flightDuration={item.Leave.Duration}
                    stopCount={item.Leave.Stop}
                  />

                  <AirportAbrName
                    time={item.Leave.DepartureTime}
                    airport={item.Leave.Origin}
                    city={item.Leave.OriginCity}
                  />
                </View>

                <View style={styles.priceContainer}>
                  <View style={styles.singleTicketPrice}>
                    <Text style={styles.singleTicketText}>
                      {item.Adult_Title}
                    </Text>
                    <Text style={styles.singleTicketPriceText}>
                      {item.Adult_Price}
                    </Text>
                  </View>

                  <View style={styles.totalTicketPrice}>
                    <Text style={styles.totalTicketText}>
                      {item.Total_Title}
                    </Text>
                    <Text style={styles.totalTicketPriceText}>
                      {item.Total_Price}
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View
              style={[
                styles.ticketTypeContainer,
                { backgroundColor: !item.IsCharter ? "#DCE1FF" : "#AFFDFF" }
              ]}
            >
              <Text
                style={[
                  styles.ticketTypeText,
                  { color: !item.IsCharter ? "#3B2889" : "#1098AE" }
                ]}
              >
                {item.IsCharter == false ? "سیستمی" : "چارتر"}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      />
    );
  }
}
export default withNavigation(ListItemTicketContainer);

export const styles = StyleSheet.create({
  ticketTypeText: {
    transform: [{ rotate: "90deg" }],
    justifyContent: "center",
    alignItems: "center",
    width: 46,
    fontSize: RFPercentage(1.6),
    paddingTop: 0,
    // paddingRight: 4,
    color: "#3B2889",
    textAlign: "center"
  },
  flightPack: {
    width: "27%",
    alignItems: "center",
    justifyContent: "center"
  },
  ticketContainer: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    width: "100%",
    height: PIXEL_RATIO >= 3 ? "100%" : "42%",
    flex: 1,
    marginBottom: 0
  },
  ticket: {
    width: "90%",
    // height: "70%",
    backgroundColor: "#ffffff",
    flexDirection: "row",
    justifyContent: "center",
    borderRadius: 10,
    flex: 1,
    alignSelf: "center"
  },
  ticketSummary: {
    height: "100%",
    padding: 16,
    alignContent: "center",
    flex: 1,
    justifyContent: "space-around"
  },
  ticketTypeContainer: {
    width: 20,
    height: 60,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: "#DCE1FF",
    position: "absolute",
    left: "5%",
    zIndex: 1,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    top: "31%"
  },
  singleTicketText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(1.7),
    color: "#55535b",
    includeFontPadding: false
  },
  singleTicketPriceText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(1.7),
    color: "#55535b",
    includeFontPadding: false
  },
  totalTicketText: {
    color: "#F09819",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2),
    includeFontPadding: false
  },
  totalTicketPriceText: {
    color: "#F09819",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2),
    includeFontPadding: false
  },
  priceContainer: {
    marginTop: 8
  },
  singleTicketPrice: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  totalTicketPrice: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  }
});
