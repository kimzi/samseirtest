import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Image
} from "react-native";
// import Hotel, {styles} from '../pages/MainSearch/Hotel';
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize"
const Icon = createIconSetFromFontello(fontelloConfig);

export default class HotelFamilyContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adults: 1,
      child: 0,
      newBorn: 0
    };
  }

  counterPlus(method) {
    if (method === "child") {
      this.setState({ child: this.state.child + 1 });
    } else if (method === "adults") {
      this.setState(
        { adults: this.state.adults + 1 },
        console.log(this.state.adults)
      );
    } else {
      this.setState({ newBorn: this.state.newBorn + 1 });
    }
  }
  counterMinus(method) {
    if (method === "child") {
      this.state.child !== 0
        ? this.setState({ child: this.state.child - 1 })
        : null;
    } else if (method === "adults") {
      this.state.adults !== 1
        ? this.setState({ adults: this.state.adults - 1 })
        : null;
    } else {
      this.state.newBorn !== 0
        ? this.setState({ newBorn: this.state.newBorn - 1 })
        : null;
    }
  }

  render() {
    return (
      <View style={styles.familyContainer}>
        <View style={styles.containerHeader}>
          <Icon name="family" style={styles.containerIcon} />
          <Text style={styles.containerTitle}>مسافران</Text>
        </View>

        <View
          style={{ flex: 0.8, justifyContent: "space-between", marginTop: 0 }}
        >
          <View style={styles.familyHorizontalLayout}>
            <View style={styles.familyOptions}>
              <Text style={styles.type}>بزرگسالان</Text>
              <Text style={styles.info}>بالای 18 سال</Text>
            </View>

            <View style={styles.numberContainer}>
              <TouchableHighlight
                underlayColor="transparent"
                onPress={this.counterMinus.bind(this, "adults")}
              >
                <Icon name="minus" style={styles.btnCounter} />
              </TouchableHighlight>

              <Text style={styles.number}> {this.state.adults} نفر</Text>

              <TouchableHighlight
                underlayColor="transparent"
                onPress={this.counterPlus.bind(this, "adults")}
              >
                <Icon name="plus" style={styles.btnCounter} />
              </TouchableHighlight>
            </View>
          </View>

          <View style={styles.familyHorizontalLayout}>
            <View style={styles.familyOptions}>
              <Text style={styles.type}>کودک</Text>
              <Text style={styles.info}>از 2 تا 12 سال</Text>
            </View>

            <View style={styles.numberContainer}>
              <TouchableHighlight
                underlayColor="transparent"
                onPress={this.counterMinus.bind(this, "child")}
              >
                <Icon name="minus" style={styles.btnCounter} />
              </TouchableHighlight>

              <Text style={styles.number}> {this.state.child} نفر</Text>

              <TouchableHighlight
                underlayColor="transparent"
                onPress={this.counterPlus.bind(this, "child")}
              >
                <Icon name="plus" style={styles.btnCounter} />
              </TouchableHighlight>
            </View>
          </View>

          <View style={styles.familyHorizontalLayout}>
            <View style={styles.familyOptions}>
              <Text style={styles.type}>تعداد اتاق</Text>
            </View>

            <View style={styles.numberContainer}>
              <TouchableHighlight
                underlayColor="transparent"
                onPress={this.counterMinus.bind(this, "newBorn")}
              >
                <Icon name="minus" style={styles.btnCounter} />
              </TouchableHighlight>

              <Text style={styles.number}> {this.state.newBorn} </Text>

              <TouchableHighlight
                underlayColor="transparent"
                onPress={this.counterPlus.bind(this, "newBorn")}
              >
                <Icon name="plus" style={styles.btnCounter} />
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  familyContainer: {
    width: "90%",
    height: "100%",
    flex: 1
  },
  containerHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  containerIcon: {
    color: "#91A9CD"
  },
  containerTitle: {
    fontFamily: "Sahel",
    color: "#91A9CD",
    fontSize: RFPercentage(1.8)
  },
  familyHorizontalLayout: {
    // flex:1,
    flexDirection: "row-reverse",
    width: "100%",
    justifyContent: "space-between"
  },
  familyOptions: {
    width: "60%",
    height: "100%",
    flexDirection: "row-reverse",
    textAlign: "center"
  },
  type: {
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(1.8),
    color: "#15479F",
    width: "40%"
  },
  info: {
    textAlign: "right",
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.8),
    color: "#9EADC5",
    width: "60%"
  },
  numberContainer: {
    flex: 1,
    width: "40%",
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    alignItems: "center"
  },
  btnCounter: {
    color: "#F09819",
    padding: 7,
    width: 26,
    height: 26
  },
  number: {
    color: "#434A5E",
    textAlign: "center",
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.8),
    width: "55%"
  }
});
