import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity,
  Platform
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const Icon = createIconSetFromFontello(fontelloConfig);

export default class WatermarkIcon extends Component {
  render() {
    return (
      <Icon
        name="group"
        style={[styles.waterMark, { zIndex: this.props.zIndexWatermark }]}
      />
    );
  }
}

export const styles = StyleSheet.create({
  waterMark: {
    width: "100%",
    fontSize: RFPercentage(23),
    position: "absolute",
    bottom: 0,
    // zIndex: -1,
    color: "#fff",
    opacity: 0.15
  }
});
