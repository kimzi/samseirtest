import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import LinearGradient from "react-native-linear-gradient";
export default class BtnGradient extends Component {
  render() {
    return (
        <View style={styles.buttonContainer}>
          <Text style={styles.okBotton}>{this.props.type}</Text>
        </View>
    );
  }
}

export const styles = StyleSheet.create({
  linearGradient: {
      justifyContent: "center",
      alignItems: "center",
      
    },
    okBotton: {
        color: "#fff",
        textAlign: "center",
        fontFamily: "Sahel",
        fontSize: RFPercentage(2.9),
        includeFontPadding: false
    },
    buttonContainer: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F63323",
        borderRadius: 30,
        width: "100%",
        height: "100%",

  }
});
