import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const Icon = createIconSetFromFontello(fontelloConfig);

export default class FilterSearchResultOptionsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMenu: 1
    };
  }

  render() {
    return (
      <View style={styles.topMenuContainer}>
        <TouchableOpacity
          style={{
            borderBottomColor:
              this.state.selectedMenu == 1 ? "#fff" : "#0D3477",
            borderBottomWidth: 3
          }}
          onPress={() => this.setState({ selectedMenu: 1 })}
          activeOpacity={1}
        >
          <Text style={styles.menuOptionText}>ارزان ترین</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            borderBottomColor:
              this.state.selectedMenu == 2 ? "#fff" : "#0D3477",
            borderBottomWidth: 3
          }}
          onPress={() => this.setState({ selectedMenu: 2 })}
          activeOpacity={1}
        >
          <Text style={styles.menuOptionText}>بهترین</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            borderBottomColor:
              this.state.selectedMenu == 3 ? "#fff" : "#0D3477",
            borderBottomWidth: 3
          }}
          onPress={() => this.setState({ selectedMenu: 3 })}
          activeOpacity={1}
        >
          <Text style={styles.menuOptionText}>سریع ترین</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            borderBottomColor:
              this.state.selectedMenu == 4 ? "#fff" : "#0D3477",
            borderBottomWidth: 3
          }}
          onPress={() => this.setState({ selectedMenu: 4 })}
          activeOpacity={1}
        >
          <Text style={styles.menuOptionText}>زودترین</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  topMenuContainer: {
    marginTop: 10,
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    backgroundColor: "#0D3477",
    alignItems: "center"
  },
  menuOptionText: {
    color: "#fff",
    fontFamily: "Sahel",
    textAlign: "center",
    marginBottom: 5
  }
});
