import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity,
  PixelRatio
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const Icon = createIconSetFromFontello(fontelloConfig);

export default class flightOptionContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: "Economic"
    };
  }

  updateParentState(flightClass) {
    this.props.updateParentState(flightClass);
  }

  render() {
    return (
      <View style={styles.flightOptionContainer}>
        <View style={styles.flightOptionHorizontalLayout}>
          <TouchableOpacity
            style={[
              styles.flightOptions,
              this.state.selectedOption == "First"
                ? { backgroundColor: "#fff" }
                : null
            ]}
            onPress={() => (
              this.setState({ selectedOption: "First" }),
              this.updateParentState("First")
            )}
          >
            <Text
              style={[
                styles.flightOptionsText,
                this.state.selectedOption == "First"
                  ? { color: "#1B275A" }
                  : null
              ]}
            >
              First Class
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.flightOptions,
              this.state.selectedOption == "Business"
                ? { backgroundColor: "#fff" }
                : null
            ]}
            onPress={() => (
              this.setState({ selectedOption: "Business" }),
              this.updateParentState("Business")
            )}
          >
            <Text
              style={[
                styles.flightOptionsText,
                this.state.selectedOption == "Business"
                  ? { color: "#1B275A" }
                  : null
              ]}
            >
              Business
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.flightOptions,
              this.state.selectedOption == "Premium"
                ? { backgroundColor: "#fff" }
                : null
            ]}
            onPress={() => (
              this.setState({ selectedOption: "Premium" }),
              this.updateParentState("Premium")
            )}
          >
            <Text
              style={[
                styles.flightOptionsText,
                this.state.selectedOption == "Premium"
                  ? { color: "#1B275A" }
                  : null
              ]}
            >
              Premium
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.flightOptions,
              this.state.selectedOption == "Economic"
                ? { backgroundColor: "#fff" }
                : null
            ]}
            onPress={() => (
              this.setState({ selectedOption: "Economic" }),
              this.updateParentState("Economic")
            )}
          >
            <Text
              style={[
                styles.flightOptionsText,
                this.state.selectedOption == "Economic"
                  ? { color: "#1B275A" }
                  : null
              ]}
            >
              Economic
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  flightOptionContainer: {
    height: PixelRatio.get() >= 3 ? "4%" : "5%",
    width: "85%",
    flexDirection: "row",
    marginTop: 15
  },
  flightOptionHorizontalLayout: {
    justifyContent: "space-between",
    flex: 1,
    flexDirection: "row"
  },
  flightOptionsText: {
    fontFamily: "Sahel-Bold",
    color: "#fff",
    fontSize: RFPercentage(1.7),
    borderRadius: 10,
    marginHorizontal: 1,
    textAlign: "center",
    textAlignVertical: "center"
  },
  flightOptions: {
    width: "24%",
    borderRadius: 6,
    borderColor: "#fff",
    borderWidth: 1,
    justifyContent: "space-around"
  }
});
