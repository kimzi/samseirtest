import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default class TicketDivider extends Component {
  render() {
    return (
      <View style={styles.dividerContainer}>
        <View style={styles.topCircle} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.bottomCircle} />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  dividerContainer: {
    flexDirection: "column",
    width: "1%",
    alignItems: "center",
    justifyContent: "space-between"
  },
  dash: {
    width: "40%",
    height: "3%",
    backgroundColor: "#B2BAC5"
  },
  topCircle: {
    width: 30,
    height: 30,
    borderRadius: 100,
    backgroundColor: "#1B275A",
    marginTop: -15
  },
  bottomCircle: {
    width: 30,
    height: 30,
    borderRadius: 100,
    backgroundColor: "#1B275A",
    marginBottom: -15
  }
});
