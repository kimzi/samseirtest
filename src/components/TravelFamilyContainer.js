import React, { Component } from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Vibration
} from "react-native";
// import Hotel, {styles} from '../pages/MainSearch/Hotel';
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
const Icon = createIconSetFromFontello(fontelloConfig);
import PropTypes from "prop-types";
import { connect } from "react-redux";
import {
  COUNTER_PLUS_ADULT,
  COUNTER_MINUS_ADULT,
  COUNTER_PLUS_CHILD,
  COUNTER_MINUS_CHILD,
  COUNTER_PLUS_NEWBORN,
  COUNTER_MINUS_NEWBORN
} from "../constants/ActionTypes";

class TravelFamilyContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sum: this.props.adultsNumber + this.props.child + this.props.newBorn
    };
  }

  componentDidMount() {
    // console.warn(this.props.adultsNumber + this.props.child + this.props.newBorn)
  }

  render() {
    const vibrationDuration = 50;
    return (
      <View style={styles.familyContainer}>
        <View style={styles.containerHeader}>
          <Icon name="family" style={styles.containerIcon} />
          <Text style={styles.containerTitle}>{this.props.passangers}</Text>
        </View>

        <View
          style={{ flex: 0.8, justifyContent: "space-between", marginTop: 0 }}
        >
          <View style={styles.familyHorizontalLayout}>
            <View style={styles.familyOptions}>
              <Text style={styles.type}>{this.props.adults}</Text>
              <Text style={styles.info}>{this.props.legalAged}</Text>
            </View>

            <View style={styles.numberContainer}>
              <TouchableHighlight
                underlayColor="transparent"
                onPress={() => (
                  this.props.counterMinusAdult(),
                  Vibration.vibrate(vibrationDuration)
                )}
              >
                <Icon name="minus" style={styles.btnCounter} />
              </TouchableHighlight>

              <Text style={styles.number}>
                {" "}
                {this.props.adultsNumber} {this.props.individual}
              </Text>

              <TouchableHighlight
                underlayColor="transparent"
                onPress={() => {
                  if (
                    this.props.adultsNumber +
                      this.props.child +
                      this.props.newBorn <
                    9
                  ) {
                    this.props.counterPlusAdult();
                    Vibration.vibrate(vibrationDuration);
                  }
                }}
              >
                <Icon name="plus" style={styles.btnCounter} />
              </TouchableHighlight>
            </View>
          </View>

          <View style={styles.familyHorizontalLayout}>
            <View style={styles.familyOptions}>
              <Text style={styles.type}>{this.props.koodak}</Text>
              <Text style={styles.info}>{this.props.koodakAged}</Text>
            </View>

            <View style={styles.numberContainer}>
              <TouchableHighlight
                underlayColor="transparent"
                onPress={() => {
                  this.props.counterMinusChild();
                  Vibration.vibrate(vibrationDuration);
                }}
              >
                <Icon name="minus" style={styles.btnCounter} />
              </TouchableHighlight>

              <Text style={styles.number}>
                {" "}
                {this.props.child} {this.props.individual}
              </Text>

              <TouchableHighlight
                underlayColor="transparent"
                onPress={() => {
                  if (
                    this.props.adultsNumber +
                      this.props.child +
                      this.props.newBorn <
                    9
                  ) {
                    this.props.counterPlusChild();
                    Vibration.vibrate(vibrationDuration);
                  }
                }}
              >
                <Icon name="plus" style={styles.btnCounter} />
              </TouchableHighlight>
            </View>
          </View>

          <View style={styles.familyHorizontalLayout}>
            <View style={styles.familyOptions}>
              <Text style={styles.type}>{this.props.infant}</Text>
              <Text style={styles.info}>{this.props.infantAged}</Text>
            </View>

            <View style={styles.numberContainer}>
              <TouchableHighlight
                underlayColor="transparent"
                onPress={() => {
                  this.props.counterMinusNewBorn();
                  Vibration.vibrate(vibrationDuration);
                }}
              >
                <Icon name="minus" style={styles.btnCounter} />
              </TouchableHighlight>

              <Text style={styles.number}>
                {" "}
                {this.props.newBorn} {this.props.individual}
              </Text>

              <TouchableHighlight
                underlayColor="transparent"
                onPress={() => {
                  if (
                    this.props.adultsNumber +
                      this.props.child +
                      this.props.newBorn <
                    9
                  ) {
                    this.props.counterPlusNewBorn();
                    Vibration.vibrate(vibrationDuration);
                  }
                }}
              >
                <Icon name="plus" style={styles.btnCounter} />
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

TravelFamilyContainer.propTypes = {
  passangers: PropTypes.string.isRequired,
  individual: PropTypes.string.isRequired,
  adults: PropTypes.string.isRequired,
  legalAged: PropTypes.string.isRequired,
  koodak: PropTypes.string.isRequired,
  koodakAged: PropTypes.string.isRequired,
  infant: PropTypes.string.isRequired,
  infantAged: PropTypes.string.isRequired
};

TravelFamilyContainer.defaultProps = {
  passangers: "مسافران",
  individual: "نفر",
  adults: "بزرگسالان",
  legalAged: "بالاتر از 18 سال",
  koodak: "کودک",
  koodakAged: "از 2 تا 12 سال",
  infant: "نوزاد",
  infantAged: "تا 2 سال"
};

function mapStateToProps(state) {
  return {
    adultsNumber: state.counter.adultsNumber,
    child: state.counter.child,
    newBorn: state.counter.newBorn
  };
}

function mapDispatchToProps(dispatch) {
  return {
    counterPlusAdult: () => dispatch({ type: COUNTER_PLUS_ADULT }),
    counterMinusAdult: () => dispatch({ type: COUNTER_MINUS_ADULT }),
    counterPlusChild: () => dispatch({ type: COUNTER_PLUS_CHILD }),
    counterMinusChild: () => dispatch({ type: COUNTER_MINUS_CHILD }),
    counterPlusNewBorn: () => dispatch({ type: COUNTER_PLUS_NEWBORN }),
    counterMinusNewBorn: () => dispatch({ type: COUNTER_MINUS_NEWBORN })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TravelFamilyContainer);

export const styles = StyleSheet.create({
  familyContainer: {
    width: "90%",
    height: "100%",
    flex: 1
  },
  containerHeader: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  containerIcon: {
    color: "#91A9CD"
  },
  containerTitle: {
    fontFamily: "Sahel",
    color: "#91A9CD",
    fontSize: RFPercentage(1.8)
  },
  familyHorizontalLayout: {
    // flex:1,
    flexDirection: "row-reverse",
    width: "100%",
    justifyContent: "space-between"
  },
  familyOptions: {
    width: "60%",
    height: "100%",
    flexDirection: "row-reverse",
    textAlign: "center"
  },
  type: {
    writingDirection: "rtl",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(1.8),
    color: "#15479F",
    width: "40%"
  },
  info: {
    textAlign: "right",
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.7),
    color: "#9EADC5",
    width: "60%"
  },
  numberContainer: {
    flex: 1,
    width: "40%",
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    alignItems: "center"
  },
  btnCounter: {
    color: "#F09819",
    padding: 7,
    fontSize: RFPercentage(2)
  },
  number: {
    color: "#434A5E",
    textAlign: "center",
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.8),
    width: "55%"
  }
});
