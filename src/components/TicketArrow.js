import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { createIconSetFromFontello } from "react-native-vector-icons";
const Icon = createIconSetFromFontello(fontelloConfig);
import fontelloConfig from "../config.json";

export default class TicketArrow extends Component {
  render() {
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "space-around",
          flexDirection: "column",
          // backgroundColor: "yellow",
          width: "44%",
          height: "50%"
        }}
      >
        <View>
          <Text style={styles.textDuration}>{this.props.flightDuration}</Text>
        </View>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Icon
            color="#C7CFD8"
            name="substraction"
            size={12}
            style={{ transform: [{ rotate: "0deg" }] }}
          />

          <View
            style={{
              height: 1,
              borderWidth: 0.5,
              borderColor: "#C7CFD8",
              marginLeft: 3,
              marginRight: 3,
              width: "70%"
            }}
          />

          <Icon
            color="#C7CFD8"
            name="substraction"
            size={12}
            style={{ transform: [{ rotate: "0deg" }] }}
          />
        </View>
        <View>
          <Text style={styles.textDuration}>{this.props.stopCount}</Text>
        </View>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  textDuration: {
    color: "#8A8795",
    fontSize: RFPercentage(1.9)
  }
});
