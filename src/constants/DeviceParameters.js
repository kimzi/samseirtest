import { PixelRatio } from "react-native";

export const PIXEL_RATIO = PixelRatio.get();