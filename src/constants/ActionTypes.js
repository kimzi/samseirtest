export const COUNTER_PLUS_ADULT = "counter_plus_adult";
export const COUNTER_MINUS_ADULT = "counter_minus_adult";
export const COUNTER_PLUS_CHILD = "counter_plus_child";
export const COUNTER_MINUS_CHILD = "counter_minus_child";
export const COUNTER_PLUS_NEWBORN = "counter_plus_newBorn";
export const COUNTER_MINUS_NEWBORN = "counter_minus_newBorn";
