import { combineReducers } from "redux";
import counter from "./counter";
import DeviceParam from "./DeviceParam";
import DeparturePortReducer from "./DeparturePortReducer";
import MyLocationReducer from "./MyLocationReducer";
import SelectedDate from "./SelectedDate"

// const reducers = combineReducers({
//   counter: counter,
//   DeviceParam: DeviceParam
// })

// const rootReducer = counter;

// reducers.js
// export default (theDefaultReducer = (state = 0, action) => state)

// rootReducer.js
// import {createStore } from 'redux'

// Use ES6 object literal shorthand syntax to define the object shape
const rootReducer = combineReducers({
  //   theDefaultReducer,
  counter,
  DeviceParam,
  DeparturePortReducer,
  MyLocationReducer
});

export default rootReducer;

// export default store = createStore(rootReducer)
// console.warn(store.getState())
// {theDefaultReducer : 0, firstNamedReducer : 1, secondNamedReducer : 2}
