const DeparturePortReducer = (departurePort = "depa", action) => {
  switch (action.type) {
    case "save":
      return {
        departurePort: departurePort
      };
    default:
      return departurePort;
  }
};
export default DeparturePortReducer;
