let initialState = {
  departureCode: "مبدأ",
  departureCity: "",
  departureAirport: "",
  isLoading: false,
  error: null
};

export default (location = (state = initialState, action) => {
  switch (action.type) {
    case "LOAD_LOC_START":
      return Object.assign({}, state, { isLoading: true });
    case "LOAD_LOC_SUCCESS":
      return Object.assign({}, state, {
        departureCode: action.payload,
        departureCity: action.payload1,
        departureAirport: action.payload2,
        isLoading: false
      });
    case "LOAD_LOC_FAILURE":
      return Object.assign({}, state, {
        error: action.payload,
        isLoading: false
      });
    default:
      return state;
  }
});
