const SelectedDate = (gregDate = "greg", action) => {
    switch (action.type) {
      case "selectedGregDate":
        return {
            gregDate: gregDate
        };
      default:
        return gregDate;
    }
  };
  export default SelectedDate;
  