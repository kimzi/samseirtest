import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Text,
  Animated,
  Easing,
  Dimensions,
  StatusBar,
  ToastAndroid,
  KeyboardAvoidingView,
  ActivityIndicator,
  AsyncStorage
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import { baseUrl } from "../constants/BaseURL";
import BtnGradient from "../components/BtnGradient";
import WatermarkIconBlack from "../components/WatermarkIconBlack";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import texture from "../Images/texture.png";
import LoginAirPlane from "../Images/loginAirplane.png";
import PassengersInformation from "./PassengersInformation";
let deviceWidth = Dimensions.get("window").width;
let deviceHeight = Dimensions.get("window").height;
import { Input, Overlay, overlayBackgroundColor } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import axios from "axios";
import {
  USER_ID,
  IS_LOGIN_POSTPONED,
  USER_EMAIL
} from "../constants/UserParams";

export default class LoginTest extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      fromAvailable: "",
      selectedBtn: null,
      textInputLoginView: null,
      textInputRegisterView: null,
      animationStart: null,
      isVerifyRegisterVisible: false,
      isVerifyLoginVisible: false,
      isVerifyMobileVisible: false,
      isRetryPasswordVisible: false,
      isLoading: false,
      errorFirstName: "",
      errorLastName: "",
      errorMobile: "",
      errorEmail: "",
      errorPassword: "",
      errorRePassword: "",
      firstName: "",
      lastName: "",
      mobile: "",
      email: "",
      password: "",
      rePassword: "",
      loginMobile: "",
      loginPassword: "",
      errorLoginMobile: "",
      errorLoginPassword: "",
      pinCode: "",
      userId: "",
      forgetPassStage: 1,
      phEnterMobile: "شماره موبایل خود را وارد کنید",
      phEnterPinCode: "کد تائید دریافتی را وارد نمائید",
      btnLoginText: "عضو هستم",
      btnSignupText: "کاربر جدید"
    };
    this.textureAnimatedValue = new Animated.Value(0);
    this.airplaneSpinValue = new Animated.Value(0);
    this.airplaneAnimatedValue = new Animated.Value(0);
  }

  async LoginPostPoned() {
    try {
      await AsyncStorage.setItem(IS_LOGIN_POSTPONED, "true");
      this.props.navigation.navigate("FlightOptions");
    } catch (error) {
      // Error saving data
      console.log(error, "error");
    }
  }

  registerSelected() {
    this.setState({
      animationStart: 0,
      selectedBtn: "register",
      textInputLoginView: 0,
      btnSignupText: "دریافت کد تائید",
      btnLoginText: "عضو هستم"
    });
    this.state.textInputLoginView || this.state.textInputRegisterView == 1
      ? setTimeout(() => {
          this.setState({ textInputRegisterView: 1 });
        }, 100)
      : (this.textureAnimatedValue.setValue(0),
        Animated.sequence([
          Animated.timing(this.airplaneSpinValue, {
            toValue: 1,
            easing: Easing.linear,
            duration: 300
          }),

          Animated.timing(this.textureAnimatedValue, {
            toValue: 1,
            easing: Easing.linear,
            duration: 700
          })
        ]).start(),
        setTimeout(() => {
          this.setState({ textInputRegisterView: 1 });
        }, 1200));
  }

  loginSelected() {
    this.setState({
      animationStart: 0,
      selectedBtn: "login",
      textInputRegisterView: 0,
      btnLoginText: "دریافت کد تائید",
      btnSignupText: "کاربر جدید"
    });
    this.state.textInputLoginView || this.state.textInputRegisterView == 1
      ? setTimeout(() => {
          this.setState({ textInputLoginView: 1 });
        }, 100)
      : (this.textureAnimatedValue.setValue(0),
        Animated.sequence([
          Animated.timing(this.airplaneSpinValue, {
            toValue: 1,
            easing: Easing.linear,
            duration: 300
          }),

          Animated.timing(this.textureAnimatedValue, {
            toValue: 1,
            easing: Easing.linear,
            duration: 700
          })
        ]).start(),
        setTimeout(() => {
          this.setState({ textInputLoginView: 1 });
        }, 1200));
  }

  componentWillMount() {
    const { navigation } = this.props;
    const fromAvailable = navigation.getParam("fromAvailable", "fromAvailable");
    this.setState({ fromAvailable: fromAvailable });
  }

  componentWillUnmount() {
    StatusBar.setHidden(false);
  }

  componentDidMount() {
    StatusBar.setHidden(true);
    Animated.timing(
      this.textureAnimatedValue,
      {
        toValue: 1,
        easing: Easing.linear,
        duration: 500
      },
      this.setState({ animationStart: 1 })
    ).start();
  }

  loginForm() {
    return (
      <View
        style={{
          width: "100%",
          paddingHorizontal: "8%",
          marginTop: "1%"
        }}
      >
        <Input
          label="شماره موبایل"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.textInput}
          onChansge={text => this.setState({ loginMobile: text })}
          value={this.state.loginMobile}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorLoginMobile}
          keyboardType="phone-pad"
          textContentType="telephoneNumber"
          onChangeText={text =>
            text.startsWith("0", 0)
              ? (this.setState({ loginMobile: text.replace(" ", "") }),
                this.setState({ errorLoginMobile: "" }))
              : this.setState({
                  errorLoginMobile: "فرمت صحیح شماره:  09XXXXXXX"
                })
          }
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.loginPassword.focus();
          }}
          blurOnSubmit={false}
        />

        <Input
          label="رمز عبور"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.textInput}
          onChangeText={text => this.setState({ loginPassword: text })}
          value={this.state.loginPassword}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorLoginPassword}
          textContentType="password"
          secureTextEntry={true}
          onChangeText={text =>
            text.startsWith(" ")
              ? this.setState({
                  errorLoginPassword:
                    "لطفاٌ در ابتدای رمز عبور از فاصله استفاده نکنید"
                })
              : this.setState(
                  { errorLoginPassword: "" },
                  this.setState({ loginPassword: text.replace(" ", "") })
                )
          }
          ref={input => {
            this.loginPassword = input;
          }}
          returnKeyType={"send"}
          onSubmitEditing={() => {
            this.checkLoginInputs();
          }}
          blurOnSubmit={false}
        />

        <TouchableOpacity
          style={[
            styles.textInput,
            { marginTop: "8%", paddingHorizontal: "3.5%" }
          ]}
          onPress={() => this.setState({ isVerifyMobileVisible: true })}
        >
          <Text style={styles.skipText}>فراموشی رمز عبور</Text>
        </TouchableOpacity>
      </View>
    );
  }

  signupForm() {
    return (
      <View
        style={{
          width: "100%",
          paddingHorizontal: "8%",
          paddingBottom: "4%",
          marginTop: "1%"
        }}
      >
        <Input
          label="نام"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.InputContainerSign}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorFirstName}
          keyboardType="name-phone-pad"
          autoCapitalize="words"
          onChangeText={text =>
            text.startsWith(" ")
              ? this.setState({
                  errorFirstName: "لطفاٌ در ابتدای نام از فاصله استفاده نکنید"
                })
              : this.setState(
                  { errorFirstName: "" },
                  this.setState({ firstName: text })
                )
          }
          value={this.state.firstName}
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.lastName.focus();
          }}
          blurOnSubmit={false}
        />
        <Input
          label="نام خانوادگی"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.InputContainerSign}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorLastName}
          keyboardType="name-phone-pad"
          autoCapitalize="words"
          onChangeText={text =>
            text.startsWith(" ")
              ? this.setState({
                  errorLastName: "لطفاٌ در ابتدای نام از فاصله استفاده نکنید"
                })
              : this.setState(
                  { errorLastName: "" },
                  this.setState({ lastName: text })
                )
          }
          value={this.state.lastName}
          ref={input => {
            this.lastName = input;
          }}
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.mobile.focus();
          }}
          blurOnSubmit={false}
        />
        <Input
          label="شماره موبایل"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.InputContainerSign}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorMobile}
          keyboardType="phone-pad"
          textContentType="telephoneNumber"
          onChangeText={text =>
            text.startsWith("0", 0)
              ? (this.setState({ mobile: text.replace(" ", "") }),
                this.setState({ errorMobile: "" }))
              : this.setState({ errorMobile: "فرمت صحیح شماره:  09XXXXXXX" })
          }
          value={this.state.mobile}
          ref={input => {
            this.mobile = input;
          }}
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.email.focus();
          }}
          blurOnSubmit={false}
        />
        <Input
          label="ایمیل"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.InputContainerSign}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorEmail}
          keyboardType="email-address"
          textContentType="emailAddress"
          onChangeText={text => this.validateEmailText(text)}
          value={this.state.email}
          ref={input => {
            this.email = input;
          }}
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.password.focus();
          }}
          blurOnSubmit={false}
        />
        <Input
          label="رمزعبور"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.InputContainerSign}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorPassword}
          onChangeText={text =>
            text.startsWith(" ")
              ? this.setState({
                  errorPassword:
                    "لطفاٌ در ابتدای رمز عبور از فاصله استفاده نکنید"
                })
              : (text.length < 8
                  ? this.setState({
                      errorPassword: "رمز عبور نباید کمتر از 8 کاراکتر باشد"
                    })
                  : this.setState({ errorPassword: "" }),
                this.setState({ password: text.replace(" ", "") }))
          }
          textContentType="password"
          secureTextEntry={true}
          value={this.state.password}
          ref={input => {
            this.password = input;
          }}
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.rePassword.focus();
          }}
          blurOnSubmit={false}
        />
        <Input
          label="تکرار رمزعبور"
          labelStyle={styles.labelContainer}
          inputStyle={styles.textInputContainer}
          inputContainerStyle={styles.inputContainerText}
          containerStyle={styles.InputContainerSign}
          errorStyle={{ color: "red", alignSelf: "flex-start" }}
          errorMessage={this.state.errorRePassword}
          onChangeText={text => (
            this.setState({ rePassword: text.replace(" ", "") }),
            this.state.password == text
              ? this.setState(
                  { errorRePassword: "" },
                  this.setState({ rePassword: text.replace(" ", "") })
                )
              : this.setState({ errorRePassword: "با رمز عبور یکسان نیست" })
          )}
          textContentType="password"
          secureTextEntry={true}
          value={this.state.rePassword}
          ref={input => {
            this.rePassword = input;
          }}
          returnKeyType={"send"}
          onSubmitEditing={() => {
            this.checkInputs();
          }}
          blurOnSubmit={false}
        />
      </View>
    );
  }

  validateEmailText(text) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) == false) {
      this.setState({ errorEmail: "فرمت ایمیل درست نیست" });
      this.setState({ email: text.replace(" ", "") });
      return false;
    } else {
      this.setState({ email: text.replace(" ", "") });
      this.setState({ errorEmail: "" });
    }
  }

  checkInputs() {
    if (this.state.mobile.length < 11) {
      this.setState({
        errorMobile: "شماره تلفن همراه را به درستی وارد نمائید"
      });
    } else if (
      this.state.firstName != "" &&
      this.state.lastName != "" &&
      this.state.mobile != "" &&
      this.state.email != "" &&
      this.state.password != "" &&
      this.state.rePassword != "" &&
      this.state.password == this.state.rePassword &&
      this.state.errorFirstName == "" &&
      this.state.errorLastName == "" &&
      this.state.errorMobile == "" &&
      this.state.errorEmail == "" &&
      this.state.errorPassword == "" &&
      this.state.errorRePassword == ""
    ) {
      this.registerUser();
    } else if (
      this.state.firstName == "" ||
      this.state.lastName == "" ||
      this.state.mobile == "" ||
      this.state.email == "" ||
      this.state.password == "" ||
      this.state.rePassword == ""
    ) {
      ToastAndroid.show("ارسال تمام موارد الزامی است", ToastAndroid.SHORT);
    } else if (this.state.password != this.state.rePassword) {
      this.setState({ errorRePassword: "با رمز عبور یکسان نیست" });
    } else if (this.state.password == this.state.rePassword) {
      this.setState({ errorRePassword: "" });
    }
  }

  saveUserId = async userId => {
    try {
      await AsyncStorage.setItem(USER_ID, userId.toString());
      console.log(userId, "saved in cache");
    } catch (error) {
      console.log(error, "asyncError");
      //ToDo
      // Error saving data
    }
  };

  registerUser() {
    this.setState({ isLoading: true });
    var self = this;
    axios
      .post(baseUrl + "/User/Register", {
        FirstName: this.state.firstName,
        LastName: this.state.lastName,
        Mobile: this.state.mobile,
        Email: this.state.email,
        Password: this.state.password
      })
      .then(function(response) {
        if (response.data.Status) {
          self.setState({
            isLoading: false,
            isVerifyRegisterVisible: true,
            userId: response.data.Result.UserID
          });
          self.saveUserId(response.data.Result.UserID);
        } else {
          self.setState({
            isLoading: false
          });
          alert(response.data.Message);
        }
      })
      .catch(function(error) {
        self.setState({ isLoading: false });
        console.log(error);
        alert(error, "**no connection to server!**");
      });
  }

  verifyUserCache = async response => {
    this.props.navigation.navigate("FlightOptions");
    try {
      AsyncStorage.setItem(USER_EMAIL, response.data.Result.Email.toString());
    } catch (error) {
      console.log(error);
    }
  };

  verifyUser() {
    this.setState({ isLoading: true });
    var self = this;
    axios
      .post(baseUrl + "/User/RegisterVerification", {
        UserId: this.state.userId,
        PinCode: this.state.pinCode
      })
      .then(function(response) {
        self.setState({
          isLoading: false,
          isVerifyRegisterVisible: false,
          pinCode: ""
        });
        // console.warn(response.data.Message);
        response.data.Status
          ? self.verifyUserCache(response)
          : alert(response.data.Message);
      })
      .catch(function(error) {
        self.setState({ isLoading: false });
        console.log(error);
        alert(error, "**no connection to server!**");
      });
  }

  loginUser() {
    this.setState({ isLoading: true });
    var self = this;
    axios
      .post(baseUrl + "/User/Login", {
        Mobile: this.state.loginMobile,
        Password: this.state.loginPassword
      })
      .then(function(response) {
        self.setState({ isLoading: false });
        // console.warn(response.data.Message);
        if (response.data.Status) {
          try {
            AsyncStorage.setItem(
              USER_ID,
              response.data.Result.UserID.toString()
            );
          } catch (error) {
            // Error saving data
          }
          try {
            AsyncStorage.setItem(
              USER_EMAIL,
              response.data.Result.Email.toString()
            );
          } catch (error) {
            // Error saving data
          }

          if (self.state.fromAvailable != "") {
            if (self.state.fromAvailable == "true") {
              self.props.navigation.navigate("PassengersInformation");
            } else {
              self.props.navigation.navigate("FlightOptions");
            }
          }
        } else {
          alert(response.data.Message);
        }
      })
      .catch(function(error) {
        self.setState({ isLoading: false });
        console.log(error);
        alert(error, "**no connection to server!**");
      });
  }

  checkLoginInputs() {
    if (
      this.state.loginMobile != "" &&
      this.state.loginPassword != "" &&
      this.state.errorLoginMobile == "" &&
      this.state.errorLoginPassword == ""
    ) {
      this.loginUser();
    } else if (this.state.loginMobile == "" || this.state.loginPassword == "") {
      ToastAndroid.show("ارسال تمام موارد الزامی است", ToastAndroid.SHORT);
    }
  }

  footerOptions() {
    return (
      <View
        style={{
          flexDirection: "column",
          marginTop: 8
        }}
      >
        {/* {this.state.textInputRegisterView == 1 ? (
          <TouchableOpacity
            style={styles.nextContainer}
            onPress={() => this.checkInputs()}
          >
            <View>
              <Text
                style={{
                  color: "#164499",
                  textAlign: "center",
                  fontFamily: "Sahel-Bold",
                  borderBottomWidth: 1,
                  borderBottomColor: "#164499",
                  fontSize: RFPercentage(2.5),
                  padding: 4,
                  marginVertical: 4,
                  includeFontPadding: false
                }}
              >
                دریافت کد تائید
              </Text>
            </View>
          </TouchableOpacity>
        ) : this.state.textInputLoginView == 1 ? (
          <TouchableOpacity
            style={styles.nextContainer}
            onPress={() => this.checkLoginInputs()}
          >
            <View>
              <Text
                style={{
                  color: "#164499",
                  textAlign: "center",
                  fontFamily: "Sahel-Bold",
                  borderBottomWidth: 1,
                  borderBottomColor: "#164499",
                  fontSize: RFPercentage(2.5),
                  padding: 4,
                  marginVertical: 4,
                  includeFontPadding: false
                }}
              >
                ورود
              </Text>
            </View>
          </TouchableOpacity>
        ) : null} */}

        <View style={styles.BtnWrapper}>
          <TouchableOpacity
            style={[
              styles.loginContainer,
              {
                backgroundColor:
                  this.state.selectedBtn == "login" ? "#164499" : "#ffffff"
              }
            ]}
            onPress={() =>
              this.state.btnLoginText == "دریافت کد تائید"
                ? this.checkLoginInputs()
                : this.state.btnLoginText == "عضو هستم"
                ? this.loginSelected()
                : null
            }
          >
            <View>
              <Text
                style={[
                  styles.loginText,
                  {
                    color:
                      this.state.selectedBtn == "login" ? "#ffffff" : "#164499"
                  }
                ]}
              >
                {this.state.btnLoginText}
              </Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.registerContainer,
              {
                backgroundColor:
                  this.state.selectedBtn == "register" ? "#164499" : "#ffffff"
              }
            ]}
            onPress={() =>
              this.state.btnSignupText == "دریافت کد تائید"
                ? this.checkInputs()
                : this.state.btnSignupText == "کاربر جدید"
                ? this.registerSelected()
                : null
            }
          >
            <View>
              <Text
                style={[
                  styles.registerText,
                  {
                    color:
                      this.state.selectedBtn == "register"
                        ? "#ffffff"
                        : "#164499"
                  }
                ]}
              >
                {this.state.btnSignupText}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => this.LoginPostPoned()}
          style={{ marginBottom: 20 }}
        >
          {/* <Text style={styles.skipText}>بعدا</Text> */}
        </TouchableOpacity>
      </View>
    );
  }

  verifyMobileForForgetPassword() {
    this.setState({ isLoading: true });
    var self = this;
    axios
      .post(baseUrl + "/User/ForgetPassword/VerifyMobile", {
        Mobile: this.state.loginMobile
      })
      .then(function(response) {
        self.setState({ isLoading: false });
        // console.warn(response.data.Message);
        response.data.Status
          ? self.setState({
              userId: response.data.Result.UserID,
              loginMobile: "",
              phEnterMobile: "کد تائید دریافتی را وارد نمائید",
              forgetPassStage: 2
            })
          : alert(response.data.Message);
      })
      .catch(function(error) {
        self.setState({ isLoading: false });
        console.log(error);
        alert(error, "**no connection to server!**");
      });
  }

  verifyPinCodeForForgetPassword() {
    this.setState({ isLoading: true });
    var self = this;
    axios
      .post(baseUrl + "/User/ForgetPassword/VerifyPinCode", {
        UserID: this.state.userId,
        PinCode: this.state.pinCode
      })
      .then(function(response) {
        self.setState({ isLoading: false });
        // console.warn(response.data.Message);
        response.data.Status
          ? self.setState({
              forgetPassStage: 3,
              isVerifyMobileVisible: false,
              isRetryPasswordVisible: true
            })
          : alert(response.data.Message);
      })
      .catch(function(error) {
        self.setState({ isLoading: false });
        console.log(error);
        alert(error, "**no connection to server!**");
      });
  }

  sendNewPassword() {
    this.setState({ isLoading: true });
    var self = this;
    axios
      .post(baseUrl + "/User/ForgetPassword/VerifyPassword", {
        UserID: this.state.userId,
        PinCode: this.state.pinCode,
        Password: this.state.password
      })
      .then(function(response) {
        self.setState({ isLoading: false });
        // console.warn(response.data.Message);
        response.data.Status
          ? self.setState({
              isRetryPasswordVisible: false
            })
          : alert(response.data.Message);
      })
      .catch(function(error) {
        self.setState({ isLoading: false });
        console.log(error);
        alert(error, "**no connection to server!**");
      });
  }

  render() {
    const spin = this.airplaneSpinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "-180deg"]
    });
    const margin =
      this.state.animationStart == 1
        ? this.textureAnimatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-deviceHeight, -deviceHeight / 6]
          })
        : this.textureAnimatedValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-deviceHeight / 6, -deviceHeight / 1.5]
          });
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "space-between",
          flexDirection: "column"
        }}
      >
        {/* <StatusBar hidden /> */}
        <KeyboardAwareScrollView
          resetScrollToCoords={{ x: 0, y: 0 }}
          // contentContainerStyle={styles.container}
          scrollEnabled={true}
          enableOnAndroid={true}
        >
          <StatusBar backgroundColor="#164499" />

          <Animated.View style={{ zIndex: 1, marginTop: margin }}>
            <Image
              source={texture}
              style={{ width: deviceWidth, height: deviceHeight }}
              resizeMode="stretch"
            />
            <Animated.Image
              source={LoginAirPlane}
              style={{
                position: "absolute",
                bottom: 20,
                zIndex: 1,
                alignSelf: "center",
                transform: [{ rotate: spin }]
              }}
            />
          </Animated.View>
          {this.state.textInputLoginView == 1 ? this.loginForm() : null}
          {this.state.textInputRegisterView == 1 ? this.signupForm() : null}
        </KeyboardAwareScrollView>

        {this.footerOptions()}

        <Overlay
          isVisible={this.state.isVerifyRegisterVisible}
          windowBackgroundColor="rgba(0, 0, 0, .7)"
          overlayBackgroundColor="white"
          width="80%"
          // height="30%"
          height="auto"
          borderRadius={6}
          // onBackdropPress={() =>
          //   this.setState({ isVerifyRegisterVisible: false, pinCode: "" })
          // }
        >
          <View
            style={{
              flexDirection: "column",
              justifyContent: "space-between",
              margin: 2
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%"
              }}
            >
              <Icon
                style={{ color: "#B5C9E8", fontSize: RFPercentage(2.5), padding: 8 }}
                onPress={() => {
                  this.setState({
                    isVerifyRegisterVisible: false
                  });
                }}
                name="cancel"
              />
            </View>

            <Input
              placeholder="کد تائید دریافتی را وارد نمائید"
              inputStyle={[
                styles.textInputContainer,
                {
                  fontSize: RFPercentage(2.8),
                  textAlign: "center"
                }
              ]}
              containerStyle={styles.InputContainerSign}
              onChangeText={text =>
                this.setState({ pinCode: text.replace(" ", "") })
              }
              value={this.state.pinCode}
            />
            <TouchableOpacity
              style={[
                styles.nextContainer,
                { marginTop: 24, width: "40%", backgroundColor: "#164499" }
              ]}
              // onPress={() => this.setState({ isVerifyRegisterVisible: false })}
              onPress={() =>
                this.state.pinCode.length == 6
                  ? this.verifyUser()
                  : console.warn("Enter Pin Code")
              }
            >
              <View>
                <Text
                  style={[
                    styles.loginText,
                    {
                      color: "#ffffff"
                    }
                  ]}
                >
                  ارسال
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Overlay>

        <Overlay
          isVisible={this.state.isVerifyLoginVisible}
          windowBackgroundColor="rgba(0, 0, 0, .7)"
          overlayBackgroundColor="white"
          width="80%"
          // height="30%"
          height="auto"
          borderRadius={6}
          // onBackdropPress={() => this.setState({ isVerifyLoginVisible: false })}
        >
          <View
            style={{
              flexDirection: "column",
              justifyContent: "space-between",
              margin: 2
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%"
              }}
            >
              <Icon
                style={{ color: "#B5C9E8", fontSize: RFPercentage(2.5), padding: 8 }}
                onPress={() => {
                  this.setState({
                    isVerifyLoginVisible: false
                  });
                }}
                name="cancel"
              />
            </View>
            <Input
              placeholder="کد تائید دریافتی را وارد نمائید"
              inputStyle={[
                styles.textInputContainer,
                {
                  fontSize: RFPercentage(2.8),
                  textAlign: "center"
                }
              ]}
              containerStyle={styles.InputContainerSign}
              onChangeText={text =>
                this.setState({ pinCode: text.replace(" ", "") })
              }
              value={this.state.pinCode}
            />
            <TouchableOpacity
              style={[
                styles.nextContainer,
                { marginTop: 24, width: "40%", backgroundColor: "#164499" }
              ]}
              // onPress={() => this.setState({ isVerifyLoginVisible: false })}
              onPress={() =>
                this.state.pinCode.length == 6
                  ? this.verifyUser()
                  : console.warn("Enter Pin Code")
              }
            >
              <View>
                <Text
                  style={[
                    styles.loginText,
                    {
                      color: "#ffffff"
                    }
                  ]}
                >
                  ارسال
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Overlay>

        <Overlay
          isVisible={this.state.isLoading}
          windowBackgroundColor="rgba(0, 0, 0, .7)"
          overlayBackgroundColor="white"
          width="auto"
          height="auto"
          borderRadius={6}
        >
          <ActivityIndicator />
        </Overlay>

        <Overlay
          isVisible={this.state.isVerifyMobileVisible}
          windowBackgroundColor="rgba(0, 0, 0, .7)"
          overlayBackgroundColor="white"
          width="80%"
          // height="30%"
          height="auto"
          borderRadius={6}
          onBackdropPress={() =>
            // this.setState({ isVerifyMobileVisible: false, forgetPassStage: 1 })
            null
          }
        >
          <View
            style={{
              flexDirection: "column",
              justifyContent: "space-between",
              margin: 2
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%"
              }}
            >
              <Icon
                style={{ color: "#B5C9E8", fontSize: RFPercentage(2.5), padding: 8 }}
                onPress={() => {
                  this.setState({
                    isVerifyMobileVisible: false
                  });
                }}
                name="cancel"
              />
            </View>

            <Input
              // placeholder="شماره موبایل خود را وارد کنید"
              placeholder={this.state.phEnterMobile}
              inputStyle={[
                styles.textInputContainer,
                {
                  fontSize: RFPercentage(2.8),
                  textAlign: "center"
                }
              ]}
              containerStyle={styles.InputContainerSign}
              onChangeText={text =>
                this.state.phEnterMobile == "شماره موبایل خود را وارد کنید"
                  ? this.setState({ loginMobile: text.replace(" ", "") })
                  : this.setState({ pinCode: text.replace(" ", "") })
              }
              value={
                this.state.phEnterMobile == "شماره موبایل خود را وارد کنید"
                  ? this.state.loginMobile
                  : this.state.pinCode
              }
            />

            <TouchableOpacity
              style={[
                styles.nextContainer,
                { marginTop: 24, width: "40%", backgroundColor: "#164499" }
              ]}
              // onPress={() => this.setState({ isVerifyLoginVisible: false })}
              onPress={() =>
                this.state.forgetPassStage == 1
                  ? this.verifyMobileForForgetPassword()
                  : this.verifyPinCodeForForgetPassword()
              }
            >
              <View>
                <Text
                  style={[
                    styles.loginText,
                    {
                      color: "#ffffff"
                    }
                  ]}
                >
                  ارسال
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Overlay>

        <Overlay
          isVisible={this.state.isRetryPasswordVisible}
          windowBackgroundColor="rgba(0, 0, 0, .7)"
          overlayBackgroundColor="white"
          width="80%"
          // height="30%"
          height="auto"
          borderRadius={6}
          onBackdropPress={() =>
            // this.setState({ isRetryPasswordVisible: false })
            null
          }
        >
          <View
            style={{
              flexDirection: "column",
              justifyContent: "space-between",
              margin: 2
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%"
              }}
            >
              <Icon
                style={{ color: "#B5C9E8", fontSize: RFPercentage(2.5), padding: 8 }}
                onPress={() => {
                  this.setState({
                    isRetryPasswordVisible: false
                  });
                }}
                name="cancel"
              />
            </View>
            <Input
              placeholder="رمز عبور"
              textContentType="password"
              secureTextEntry={true}
              inputStyle={[
                styles.textInputContainer,
                {
                  fontSize: RFPercentage(2.8),
                  textAlign: "center"
                }
              ]}
              containerStyle={styles.InputContainerSign}
              onChangeText={text =>
                this.setState({ password: text.replace(" ", "") })
              }
              value={this.state.password}
            />
            <Input
              placeholder="تکرار رمز عبور"
              textContentType="password"
              secureTextEntry={true}
              inputStyle={[
                styles.textInputContainer,
                {
                  fontSize: RFPercentage(2.8),
                  textAlign: "center"
                }
              ]}
              containerStyle={styles.InputContainerSign}
              onChangeText={text =>
                this.setState({ rePassword: text.replace(" ", "") })
              }
              value={this.state.rePassword}
            />
            <TouchableOpacity
              style={[
                styles.nextContainer,
                { marginTop: 24, width: "40%", backgroundColor: "#164499" }
              ]}
              // onPress={() => this.setState({ isVerifyLoginVisible: false })}
              onPress={() =>
                this.state.forgetPassStage == 3 ? this.sendNewPassword() : null
              }
            >
              <View>
                <Text
                  style={[
                    styles.loginText,
                    {
                      color: "#ffffff"
                    }
                  ]}
                >
                  ارسال
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Overlay>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    // marginTop: "1%",
    padding: "10%"
    // backgroundColor: "yellow"
  },
  BtnWrapper: {
    flexDirection: "row-reverse",
    width: "80%",
    alignSelf: "center",
    justifyContent: "space-between",
    alignItems: "center"
  },
  loginContainer: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#164499",
    width: "45%",
    padding: 6
  },
  loginText: {
    color: "#164499",
    textAlign: "center",
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.3),
    includeFontPadding: false
  },
  registerContainer: {
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#164499",
    width: "45%",
    padding: 6
  },
  registerText: {
    color: "#164499",
    textAlign: "center",
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.3),
    includeFontPadding: false
  },
  skipContainer: {
    alignItems: "center",
    borderBottomColor: "#29D2E6",
    width: "8%",
    margin: 8,
    justifyContent: "center",
    alignSelf: "center"
  },
  skipText: {
    color: "#29D2E6",
    fontSize: RFPercentage(2.1)
  },
  textInput: {
    borderColor: "darkgrey",
    // borderBottomWidth: 1,
    height: deviceHeight / 14
    // width: "100%",
    // zIndex: 1و
    // backgroundColor: "red"
  },
  InputContainerSign: {
    borderColor: "grey",
    // borderBottomWidth: 1,
    height: deviceHeight / 14,
    // width: "100%",
    // backgroundColor: "red",
    marginBottom: 6
  },
  inputContainerText: {
    // backgroundColor: "yellow",
    // paddingBottom: -4
    height: "60%"
  },
  labelContainer: {
    color: "#808080",
    fontSize: RFPercentage(1.9),
    writingDirection: "rtl"

    // backgroundColor:"blue"
  },
  textInputContainer: {
    color: "black",
    fontSize: RFPercentage(2.2)
    // paddingVertical: -4
  },
  nextContainer: {
    alignSelf: "center",
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    // borderWidth: 1,
    borderColor: "#164499",
    // backgroundColor: "#164499",
    width: "80%",
    marginBottom: 10,
    padding: 6
  }
});
