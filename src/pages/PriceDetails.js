import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  AsyncStorage,
  Platform
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import LinearGradient from "react-native-linear-gradient";
import PropTypes from "prop-types";
import WaterMark from "../components/WatermarkIcon";
import AppbarTitle from "../components/AppbarTitle";
import { USER_ID } from "../constants/UserParams";
const height = Dimensions.get("window").height;

export default class PriceDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prices: null,
      alarm: null,
      total_Title: null,
      total: null
    };
  }
  componentWillMount() {
    const { navigation } = this.props;
    const prices = navigation.getParam("prices", "prices");
    const alarm = navigation.getParam("alarm", "alarm");
    const total_Title = navigation.getParam("total_Title", "total_Title");
    const total = navigation.getParam("total", "total");
    this.setState(
      {
        prices: prices,
        alarm: alarm,
        total_Title: total_Title,
        total: total
      },
      console.log(
        "prices",
        prices,
        "alarm",
        alarm,
        "total_Title",
        total_Title,
        "total",
        total
      )
    );
  }

  navigateToBack() {
    this.props.navigation.navigate("OneWayTicketDetails");
  }

  async checkLoginStatus() {
    try {
      const value = await AsyncStorage.getItem(USER_ID);
      if (value !== null) {
        this.props.navigation.navigate("PassengersInformation", {
          // prices: this.state.prices,
          // alarm: this.state.alarm,
          // total_Title: this.state.total_Title,
          // total: this.state.total
        });
      } else {
        this.props.navigation.navigate("Login", {
          fromAvailable: "true"
          // prices: this.state.prices,
          // alarm: this.state.alarm,
          // total_Title: this.state.total_Title,
          // total: this.state.total
        });
      }
    } catch (error) {
      // Error retrieving data
      console.warn(error);
    }
  }

  render() {
    const state = this.state;
    return (
      <LinearGradient
        colors={["#1B275A", "#1B275A", "#1B275A"]}
        style={{ flex: 1 }}
      >
        <AppbarTitle
          pageTitle="جزئیات قیمت"
          backIconColor="white"
          titleTextColor="white"
          navigateTo={this.navigateToBack.bind(this)}
        />

        <ScrollView
          style={{ backgroundColor: "transparent", marginTop: 14, zIndex: 10 }}
        >
          <View style={{ justifyContent: "space-around" }}>
            <View style={{}}>
              <View style={styles.tableContainer}>
                <View style={styles.PriceDetails}>
                  <View style={styles.priceDetailsContainer}>
                    <Text style={styles.priceTitle}>
                      {state.prices[0].Base_Title}
                    </Text>
                    <Text style={styles.price}>{state.prices[0].Base}</Text>
                  </View>

                  <View style={styles.divider} />

                  <View style={styles.taxContainer}>
                    <Text style={styles.priceTitle}>
                      {state.prices[0].Tax_Title}
                    </Text>
                    <Text style={styles.price}>{state.prices[0].Tax}</Text>
                  </View>
                  <View style={styles.divider} />
                  <View style={styles.otherCostsContainer}>
                    <Text style={styles.priceTitle}>
                      {state.prices[0].Other_Title}
                    </Text>
                    <Text style={styles.price}>{state.prices[0].Other}</Text>
                  </View>
                </View>
                <View style={styles.unitPriceContainer}>
                  <Text style={styles.unitPriceTitle}>
                    {state.prices[0].Amount_Title}
                  </Text>
                  <View style={styles.dashContainer}>
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                    <View style={styles.dash} />
                  </View>
                  <Text style={styles.unitPrice}>{state.prices[0].Amount}</Text>
                </View>
                <View style={styles.totalItemPriceContainer}>
                  <Icon name="adult" color="#15479F" style={styles.adultIcon} />
                  <Text style={styles.numberText}>{state.prices[0].Age}</Text>
                  <Text style={styles.totalItemPrice}>
                    {state.prices[0].Total}
                  </Text>
                </View>
              </View>

              {state.prices[1] != null ? (
                <View style={[styles.tableContainer, { marginTop: 15 }]}>
                  <View style={styles.PriceDetails}>
                    <View style={styles.priceDetailsContainer}>
                      <Text style={styles.priceTitle}>
                        {state.prices[1].Base_Title}
                      </Text>
                      <Text style={styles.price}>{state.prices[1].Base}</Text>
                    </View>

                    <View style={styles.divider} />

                    <View style={styles.taxContainer}>
                      <Text style={styles.priceTitle}>
                        {state.prices[1].Tax_Title}
                      </Text>
                      <Text style={styles.price}>{state.prices[1].Tax}</Text>
                    </View>
                    <View style={styles.divider} />
                    <View style={styles.otherCostsContainer}>
                      <Text style={styles.priceTitle}>
                        {state.prices[0].Other_Title}
                      </Text>
                      <Text style={styles.price}>{state.prices[0].Other}</Text>
                    </View>
                  </View>
                  <View style={styles.unitPriceContainer}>
                    <Text style={styles.unitPriceTitle}>
                      {state.prices[1].Amount_Title}
                    </Text>
                    <View style={styles.dashContainer}>
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                    </View>
                    <Text style={styles.unitPrice}>
                      {state.prices[1].Amount}
                    </Text>
                  </View>
                  <View style={styles.totalItemPriceContainer}>
                    <Icon
                      name="child"
                      color="#15479F"
                      style={styles.adultIcon}
                    />
                    <Text style={styles.numberText}>{state.prices[1].Age}</Text>
                    <Text style={styles.totalItemPrice}>
                      {state.prices[1].Total}
                    </Text>
                  </View>
                </View>
              ) : null}
              {state.prices[2] != null ? (
                <View style={[styles.tableContainer, { marginTop: 15 }]}>
                  <View style={styles.PriceDetails}>
                    <View style={styles.priceDetailsContainer}>
                      <Text style={styles.priceTitle}>
                        {state.prices[2].Base_Title}
                      </Text>
                      <Text style={styles.price}>{state.prices[2].Base}</Text>
                    </View>

                    <View style={styles.divider} />

                    <View style={styles.taxContainer}>
                      <Text style={styles.priceTitle}>
                        {state.prices[2].Tax_Title}
                      </Text>
                      <Text style={styles.price}>{state.prices[2].Tax}</Text>
                    </View>
                    <View style={styles.divider} />
                    <View style={styles.otherCostsContainer}>
                      <Text style={styles.priceTitle}>
                        {state.prices[2].Other_Title}
                      </Text>
                      <Text style={styles.price}>{state.prices[2].Other}</Text>
                    </View>
                  </View>
                  <View style={styles.unitPriceContainer}>
                    <Text style={styles.unitPriceTitle}>
                      {state.prices[2].Amount_Title}
                    </Text>
                    <View style={styles.dashContainer}>
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                      <View style={styles.dash} />
                    </View>
                    <Text style={styles.unitPrice}>
                      {state.prices[2].Amount}
                    </Text>
                  </View>
                  <View style={styles.totalItemPriceContainer}>
                    <Icon
                      name="newborn"
                      color="#15479F"
                      style={styles.adultIcon}
                    />
                    <Text style={styles.numberText}>{state.prices[2].Age}</Text>
                    <Text style={styles.totalItemPrice}>
                      {state.prices[2].Amount}
                    </Text>
                  </View>
                </View>
              ) : null}
              <Text style={styles.rialText}>{this.state.alarm}</Text>
            </View>

            <View
              style={{
                // justifyContent: "flex-end",
                alignSelf: "center",
                width: "100%",
                marginTop: 20
                // alignSelf : 'center',
                // marginBottom: -100,
              }}
            >
              <View
                style={{
                  flexDirection: "row-reverse",
                  width: "90%",
                  justifyContent: "space-around",
                  alignSelf: "center"
                  // width : '100%'
                }}
              >
                <Text
                  style={{
                    color: "#FFFFFF",
                    fontSize: RFPercentage(2.5),
                    fontFamily: "Sahel-Bold"
                  }}
                >
                  {this.state.total_Title}
                </Text>
                <Text
                  style={{
                    color: "#FFBA00",
                    fontSize: RFPercentage(2.8),
                    fontFamily: "Sahel-Bold"
                  }}
                >
                  {this.state.total}
                </Text>
              </View>

              <TouchableOpacity
                style={styles.Btn}
                // onPress={() =>
                //   this.props.navigation.navigate("PassengersInformation", {
                //     // prices: this.state.prices,
                //     // alarm: this.state.alarm,
                //     // total_Title: this.state.total_Title,
                //     // total: this.state.total
                //   })
                // }
                onPress={() => this.checkLoginStatus()}
              >
                <View
                  style={{ justifyContent: "center", alignItems: "center" }}
                >
                  <Text
                    style={{
                      color: "#FFFFFF",
                      fontFamily: "Sahel-Bold",
                      fontSize: RFPercentage(2.6),
                      textAlign: "center",
                      zIndex: 1
                      // includeFontPadding: false
                    }}
                  >
                    تایید و ادامه
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <WaterMark zIndexWatermark={Platform.OS == 'ios' ? -1 : 0} />
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({
  backIcon: {
    margin: 16,
    marginBottom: 0,
    fontSize: RFPercentage(2.2),
    color: "#FFFFFF",
    width: 40,
    height: 30
  },
  mainLinerGradient: {
    flex: 1
  },
  PriceDetailsTextContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  PriceDetailsText: {
    color: "#FFFFFF",
    fontSize: RFPercentage(2.2),
    fontFamily: "Sahel-Bold",
    textAlign: "center"
  },
  headerElement: {
    marginBottom: 10
  },
  tableContainer: {
    borderRadius: 6,
    justifyContent: "center",
    flexDirection: "column",
    marginRight: "5%",
    marginLeft: "5%",
    borderWidth: 0.5,
    borderColor: "#F9C476",
    alignItems: "flex-end",
    flex: 1
  },
  PriceDetails: {
    borderRadius: 6,
    justifyContent: "center",
    flexDirection: "row-reverse",
    flex: 1
  },
  priceTitle: {
    color: "#FFFFFF",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(1.7)
  },
  price: {
    color: "#FFFFFF",
    fontSize: RFPercentage(1.7)
  },
  priceDetailsContainer: {
    alignItems: "flex-end",
    paddingTop: "5%",
    paddingRight: "5%",
    width: "34%",
    justifyContent: "space-around"
  },
  divider: {
    width: 1,
    height: "90%",
    borderWidth: 0.25,
    borderColor: "#F8C376",
    marginTop: "5%"
  },
  taxContainer: {
    alignItems: "center",
    paddingTop: "5%",
    width: "33.33%",
    justifyContent: "space-around"
  },
  otherCostsContainer: {
    alignItems: "flex-start",
    paddingTop: "5%",
    paddingLeft: "5%",
    width: "33.33%",
    justifyContent: "space-around"
  },
  unitPriceTitle: {
    color: "#FFFFFF",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(1.7),
    width: "35%",
    textAlign: "right",
    paddingRight: "5%",
    alignSelf: "center"
  },
  unitPriceContainer: {
    paddingRight: 20,
    flexDirection: "row-reverse",
    width: "100%",
    marginTop: "10%",
    flex: 1
  },
  dash: {
    width: "6%",
    height: 1,
    backgroundColor: "#F9C476"
  },
  dashContainer: {
    width: "35%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "center",
    paddingRight: "2%"
  },
  unitPrice: {
    width: "35%",
    alignSelf: "center",
    color: "#FFFFFF",
    fontSize: RFPercentage(1.7),
    paddingLeft: "3%"
  },
  totalItemPriceContainer: {
    backgroundColor: "#BDD1F5",
    width: "100%",
    borderRadius: 6,
    flexDirection: "row-reverse",
    paddingLeft: "4%",
    alignItems: "center",
    marginTop: 25,
    flex: 1
  },
  adultIcon: {
    fontSize: RFPercentage(3),
    paddingTop: 3,
    paddingBottom: 3
  },
  numberText: {
    fontSize: RFPercentage(1.7),
    height: "70%",
    color: "#1B275A",
    fontFamily: "Sahel-Bold",
    paddingRight: "12%"
  },
  totalItemPrice: {
    textAlign: "left",
    flex: 1,
    paddingLeft: "4%",
    color: "#1B275A",
    fontSize: RFPercentage(1.8)
  },
  rialText: {
    fontSize: RFPercentage(1.7),
    color: "#FFFFFF",
    fontFamily: "Sahel-Bold",
    textAlign: "right",
    // marginTop: 5,
    paddingRight: "4%",
    marginTop: 10
  },
  Btn: {
    width: "80%",
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    borderRadius: 30,
    backgroundColor: "#F63323",
    padding: 7,
    marginTop: 6,
    marginBottom: 24,
    zIndex: 1000
    // height: "3%",
    // marginBottom: 30,
  }
});
