import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  ScrollView,
  StatusBar,
  FlatList,
  AsyncStorage,
  Platform
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import PropTypes from "prop-types";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { PIXEL_RATIO } from "../constants/DeviceParameters";
import WaterMark from "../components/WatermarkIcon";
import Btn from "../components/BtnGradientCheckmark";
import traveler from "../Images/traveler.png";
import bag from "../Images/bag.png";
import Rectangle from "../Images/Rectangle.png";
import AppbarTitle from "../components/AppbarTitle";
let circleRadius = PIXEL_RATIO > 2 ? 20 : 15;
import axios from "axios";
import { baseUrl } from "../constants/BaseURL";
import { REQUEST_ID } from "../constants/UserParams.js";

export default class OneWayTicketDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      stopModalVisible: false,
      data: null,
      prices: null,
      alarm: null,
      total_Title: null,
      total: null,
      airlineName: null,
      airlineLogo: null
    };
  }

  async componentWillMount() {
    const { navigation } = this.props;
    const Index = navigation.getParam("Index", "Index");
    const Req_ID = navigation.getParam("Req_ID", "Req_ID");
    const airlineName = navigation.getParam("airlineName", "airlineName");
    const airlineLogo = navigation.getParam("airlineLogo", "airlineLogo");
    console.log(Index , Req_ID);
    var self = this;
    self.setState({
      airlineName: airlineName,
      airlineLogo: airlineLogo
    });
    await axios
      .post(baseUrl + "/Flight/Detail", {
        Index: Index,
        Req_ID: Req_ID
      })
      .then(function(response) {
        console.log(response.data , 'data');
        let arrayLength = response.data.Result.LeaveStops.length - 1;
        for (let i =0 ; i < arrayLength ; i++){
          let diff = Math.abs(
            new Date(response.data.Result.LeaveStops[i].Arrival_DateTime) - new Date(response.data.Result.LeaveStops[i+1].Departure_DateTime)
          );
          let minutes = Math.floor(diff / 1000 / 60);
          let hours = Math.floor(minutes / 60);
          let finalMinutes = Math.floor(minutes % 60);
      
          console.log(hours, "h", finalMinutes, "m");
          response.data.Result.LeaveStops[i].Arrival_DateTime = `${hours}h ${finalMinutes}m`;
          
        }


        self.setState({
          data: response.data.Result.LeaveStops,
          prices: response.data.Result.Prices,
          alarm: response.data.Result.Alarm,
          total_Title: response.data.Result.Total_Title,
          total: response.data.Result.Total
        });
      })
      .catch(function(error) {
        console.log(error);
        console.warn(
          "error handling require here: **no connection to server!**"
        );
      });
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 20
        }}
      />
    );
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setStopModalVisible(visible) {
    this.setState({ stopModalVisible: visible });
  }

  item = ({ item }) => {
    let arrayLength = this.state.data.length - 1;
    console.log(arrayLength, item.Index);
    console.log(item)
    // let diff = Math.abs(
    //   new Date(item[0].Departure_DateTime) - new Date(item[1].Arrival_DateTime)
    // );
    // let minutes = Math.floor(diff / 1000 / 60);
    // let hours = Math.floor(minutes / 60);
    // let finalMinutes = Math.floor(minutes % 60);

    // console.log(hours, "hreal", finalMinutes, "m");

    return (
      <View Style={styles.itemDecoration}>
        <View style={styles.flight}>
          <View style={styles.departureTag}>
            <View style={styles.departureTime}>
              <Icon
                color="#173E90"
                name="departure"
                size={14}
                style={{
                  transform: [{ rotate: "0deg" }],
                  marginEnd: 10
                }}
              />
              <Text style={styles.flightTime}>{item.Departure_Time}</Text>
            </View>
            <Text
              style={styles.flightDate}
              numberOfLines={2}
              ellipsizeMode={"middle"}
            >
              {item.Departure_Date_En}
            </Text>
            <Text
              style={styles.flightDate}
              numberOfLines={2}
              ellipsizeMode={"middle"}
            >
              {item.Departure_Date_Pr}
            </Text>
            <Text
              style={styles.flightCity}
              numberOfLines={2}
              ellipsizeMode={"middle"}
            >
              {/* ({item.departureCode})-{item.departureCity} */}
              {item.Origin_City}
            </Text>
            <Text
              style={styles.flightAirport}
              numberOfLines={2}
              ellipsizeMode={"middle"}
            >
              {item.Origin_Airport}
            </Text>
          </View>

          <View style={styles.flightDivider}>
            <View>
              <Text style={styles.textDuration}>{item.Duration}</Text>
            </View>

            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                marginTop: -3,
                marginBottom: 3
              }}
            >
              <Icon
                color="#C7CFD8"
                name="substraction"
                size={12}
                style={{ transform: [{ rotate: "0deg" }] }}
              />

              <View
                style={{
                  height: 1,
                  borderWidth: 0.5,
                  borderColor: "#C7CFD8",
                  marginStart: 3,
                  marginEnd: 3,
                  width: "80%"
                }}
              />

              <Icon
                color="#C7CFD8"
                name="substraction"
                size={12}
                style={{ transform: [{ rotate: "0deg" }] }}
              />
            </View>

            <View style={styles.airlineImage}>
              <Image
                source={{ uri: item.AirlineLogo }}
                style={styles.airlineImage}
              />
            </View>

            <View>
              <Text style={styles.textDuration}>{item.Airplane}</Text>
            </View>

            <View>
              <Text style={styles.textDuration}>{item.Flight_Number}</Text>
            </View>
          </View>

          <View style={styles.destinationTag}>
            <View style={styles.departureTime}>
              <Text style={styles.flightTime}>{item.Arrival_Time}</Text>
              <Icon
                color="#173E90"
                name="destination"
                size={14}
                style={{
                  transform: [{ rotate: "0deg" }],
                  marginStart: 10
                }}
              />
            </View>
            <Text
              style={styles.flightDate}
              numberOfLines={1}
              ellipsizeMode={"middle"}
            >
              {item.Arrival_Date_En}
            </Text>
            <Text
              style={styles.flightDate}
              numberOfLines={2}
              ellipsizeMode={"middle"}
            >
              {item.Arrival_Date_Pr}
            </Text>
            <Text
              style={styles.flightCity}
              numberOfLines={1}
              ellipsizeMode={"middle"}
            >
              {/* ({item.destinationCode})-{item.destinationCity} */}
              {item.Destination_City}
            </Text>
            <Text
              style={styles.flightAirport}
              numberOfLines={2}
              ellipsizeMode={"middle"}
            >
              {item.Destination_Airport}
            </Text>
          </View>
        </View>
        {item.Index != arrayLength ? (
          <View style={styles.stopDecoration}>
            <View style={styles.stopCityView}>
              <Text numberOfLines={2} style={styles.stopCityText}>{item.StopCity}</Text>
            </View>
            <Icon
              color="black"
              name="stop--icon"
              size={12}
              style={{ transform: [{ rotate: "0deg" }] }}
            />
            <View style={styles.stopCityView}>
              <Text style={styles.stopDurationText}>{item.Arrival_DateTime}</Text>
              {/* <Text style={styles.stopDurationText} /> */}
            </View>
          </View>
        ) : null}
      </View>
    );
  };

 

  dash() {
    return (
      <View style={styles.dashContainer}>
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
      </View>
    );
  }

  navigateToBack() {
    this.props.navigation.navigate("AirLineSearchResult");
  }
  _keyExtractor = (item, index) => console.log(item.Index , 'index');

  render() {
    // let diff = Math.abs(
    //   new Date("2019-07-17T03:55:00Z") - new Date("2019-07-17T02:00:00Z")
    // );
    // let minutes = Math.floor(diff / 1000 / 60);
    // let hours = Math.floor(minutes / 60);
    // let finalMinutes = Math.floor(minutes % 60);

    // console.log(hours, "h", finalMinutes, "m");
    return (
      <LinearGradient
        colors={["#1B275A", "#1B275A", "#1B275A"]}
        style={styles.mainLinerGradient}
      >
        <StatusBar backgroundColor="#1B275A" />

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        >
          <LinearGradient
            colors={["#15479F", "#7E3E65", "#F63323"]}
            style={styles.modalLinerGradient}
          >
            <StatusBar backgroundColor="#15479F" />
            <View style={styles.modalContainer}>
              <View style={styles.fairRuleContainer}>
                <View style={styles.modalHeaderContainer}>
                  <View style={styles.cabinCheckedContainer}>
                    <Image
                      source={traveler}
                      style={styles.travelerImageStyle}
                    />
                    <View>
                      <Text style={styles.cabinChecked}>
                        {this.props.cabin}
                      </Text>
                      <Text style={styles.cabinCheckedDetails}>
                        {this.props.cabinInfo}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.cabinCheckedContainer}>
                    <Image source={bag} style={styles.bagImageStyle} />
                    <View>
                      <Text style={styles.cabinChecked}>
                        {this.props.checked}
                      </Text>
                      <Text style={styles.cabinCheckedDetails}>
                        {this.props.checkedInfo}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.ruleDetailsContainer} />
                <ScrollView style={styles.ruleScrollView}>
                  <Text style={styles.fareRuleText}>
                    Full Rules and Details
                  </Text>
                  <Text style={styles.fareRuleDetails}>
                    {this.props.fareRule}
                  </Text>
                </ScrollView>
                <TouchableOpacity
                  style={styles.modalBtnContainer}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Btn type="tik" />
                </TouchableOpacity>
                <Image source={Rectangle} style={styles.rectangleImage} />
              </View>
            </View>
          </LinearGradient>
        </Modal>

        <AppbarTitle
          pageTitle="جزئیات پرواز"
          backIconColor="white"
          titleTextColor="white"
          navigateTo={this.navigateToBack.bind(this)}
        />

        {/* <View>
          <Icon
            name="back"
            style={styles.back}
            onPress={() =>
              this.props.navigation.navigate("AirLineSearchResult")
            }
          />
        </View> */}

        <View>
          <View
            style={{
              backgroundColor: "#ffffff",
              // marginLeft: "5%",
              // marginRight: "5%",
              marginHorizontal: "5%",
              marginTop: "0.5%",
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40,
              height: "75%"
            }}
          >
            <View style={styles.hearderStyle}>
              <View style={styles.airlineImage}>
                <Image
                  source={{ uri: this.state.airlineLogo }}
                  style={styles.airlineImage}
                />
              </View>

              <View style={styles.airplaneContainer}>
                <Text style={styles.airlineName}>{this.state.airlineName}</Text>
              </View>
            </View>

            <View style={styles.dividerContainer}>
              <View style={styles.topCircle} />

              {/* 38 */}
              {this.dash()}
              <View style={styles.bottomCircle} />
            </View>

            <View style={{ flex: 1, marginBottom: 16 }}>
              <FlatList
                style={{
                  padding: 0
                }}
                data={this.state.data}
                contentContainerStyle={{ paddingTop: 0, paddingBottom: 0 }}
                ItemSeparatorComponent={this.renderSeparator}
                extraData={this.state}
                renderItem={this.item}
                keyExtractor={this._keyExtractor}              
                />
            </View>

            <View style={styles.blueCircleContainer}>
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
            </View>
          </View>

          <View
            style={{
              alignItems: "center",
              zIndex: 5,
              marginTop: 0,
              height: "5%",
              justifyContent: "center"
            }}
          >
            <TouchableOpacity
              style={{
                flexDirection: "row",
                alignItems: "center",
                padding: 2,
                marginTop: 9
              }}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            >
              <Text style={styles.flightDetails}>
                Flight and baggage details
              </Text>

              <Icon color="#ffffff" name="right" />
            </TouchableOpacity>
          </View>

          <View
            style={{
              alignItems: "center",
              height: "9%",
              justifyContent: "center",
              marginTop: "5%"
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("PriceDetails", {
                  prices: this.state.prices,
                  alarm: this.state.alarm,
                  total_Title: this.state.total_Title,
                  total: this.state.total
                })
              }
              style={styles.BtnContainer}
            >
              <Text style={styles.BtnText}>{this.props.acceptText}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <WaterMark zIndexWatermark={Platform.OS == 'ios' ? -1 : 0} />
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  flight: {
    flexDirection: "row"
  },
  itemDecoration: {
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  flightAirport: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.7),
    color: "#7E7C85",
    textAlign: "center",
    paddingTop: 4,
    includeFontPadding: false
  },
  flightCity: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(2.1),
    color: "#7E7C85",
    textAlign: "center",
    paddingTop: 4,
    includeFontPadding: false
  },
  flightTime: {
    fontFamily: "Lato-Black",
    fontSize: RFPercentage(2.7),
    color: "#173E90",
    textAlign: "center",
    includeFontPadding: false
  },
  flightDate: {
    fontSize: RFPercentage(1.65),
    fontFamily: "Montserrat-Regular",
    color: "#19A2F2",
    textAlign: "center",
    paddingTop: 4,
    includeFontPadding: false
  },
  back: {
    color: "#ffffff",
    fontSize: RFPercentage(2.2),
    width: 40,
    height: 30,
    margin: 16
  },
  mainLinerGradient: {
    flex: 1
  },
  modalLinerGradient: {
    position: "absolute",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    backgroundColor: "red",
    zIndex: -100
  },
  modalContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  fairRuleContainer: {
    height: "80%",
    backgroundColor: "#ffffff",
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40
  },
  modalHeaderContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10
  },
  cabinCheckedContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  travelerImageStyle: {
    marginRight: 5
  },
  bagImageStyle: {
    marginRight: 5
  },
  ruleDetailsContainer: {
    borderWidth: 0.5,
    borderColor: "#CDDCF3",
    marginTop: 10
  },
  fareRuleText: {
    fontSize: RFPercentage(2.3),
    fontFamily: "Montserrat-Regular",
    color: "#15479F"
  },
  ruleScrollView: {
    flex: 1,
    padding: 30
  },
  fareRuleDetails: {
    fontSize: RFPercentage(2),
    fontFamily: "Montserrat-Regular",
    color: "#7E7C85",
    paddingTop: 10
  },
  modalBtnContainer: {
    width: 150,
    height: "8%",
    alignSelf: "center",
    position: "absolute",
    bottom: 20,
    zIndex: 2
  },
  rectangleImage: {
    position: "absolute",
    zIndex: 1,
    bottom: 0
  },
  departureTag: {
    width: "36%",
    padding: 9,
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center"
  },
  flightDivider: {
    width: "28%",
    flexDirection: "column",
    alignContent: "flex-start",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: 14
  },
  destinationTag: {
    width: "36%",
    padding: 9,
    flexDirection: "column",
    alignContent: "center",
    alignItems: "center"
  },
  departureTime: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-around"
  },
  airlineImage: {
    width: 32,
    height: 32,
    marginBottom: 5,
    top: 0
  },
  hearderStyle: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10
  },
  airlineName: {
    fontSize: RFPercentage(2),
    fontFamily: "Montserrat-Regular",
    color: "#000000",
    marginBottom: -4,
    textAlign: "center"
  },
  airPlaneName: {
    fontFamily: "Lato-Regular",
    color: "#7A7F85",
    fontSize: RFPercentage(2.2),
    textAlign: "center"
  },
  airplaneContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  dividerContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%"
  },
  dash: {
    width: "1%",
    height: 1,
    backgroundColor: "#B2BAC5"
  },
  dashContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1
  },
  topCircle: {
    width: 20,
    height: 20,
    borderRadius: 100,
    backgroundColor: "#1A2A61",
    marginLeft: -10
  },
  bottomCircle: {
    width: 20,
    height: 20,
    borderRadius: 100,
    backgroundColor: "#1A2A61",
    marginRight: -10
  },
  blueCircle: {
    height: circleRadius / 1.8,
    width: circleRadius / 1.5,
    borderTopRightRadius: circleRadius / 3,
    borderTopLeftRadius: circleRadius / 3,
    backgroundColor: "#1B275A"
  },
  blueCircleContainer: {
    flexDirection: "row",
    paddingHorizontal: 2,
    marginTop: 0,
    justifyContent: "space-between",
    position: "absolute",
    left: 0,
    right: 0,
    bottom: -1
  },
  flightDetails: {
    fontSize: RFPercentage(1.9),
    fontFamily: "Montserrat-Regular",
    color: "#ffffff",
    marginRight: 10
  },
  BtnText: {
    fontSize: RFPercentage(2.3),
    fontFamily: "Sahel-Bold",
    color: "#ffffff",
    textAlignVertical: "center",
    includeFontPadding: false
  },
  cabinChecked: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(2.1),
    color: "#7E7C85"
  },
  cabinCheckedDetails: {
    fontFamily: "Montserrat-Bold",
    fontSize: RFPercentage(2.3),
    color: "#313841"
  },
  BtnContainer: {
    padding: 10,
    backgroundColor: "#E7342A",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 100,
    width: "70%",
    height: "100%",
    zIndex: 1
  },
  textDuration: {
    color: "#8A8795",
    fontSize: RFPercentage(1.8),
    includeFontPadding: false,
    textAlign: "center"
  },
  stopDecoration: {
    marginHorizontal: "15%",
    backgroundColor: "#F9C476",
    borderRadius: 6,
    padding: 4,
    marginTop: 4,
    flexDirection: "row",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  stopCityView: {
    width: "46%"
  },
  stopCityText: {
    color: "black",
    fontSize: RFPercentage(1.65),
    includeFontPadding: false,
    textAlign: "center"
  },
  stopDurationText: {
    color: "black",
    fontSize: RFPercentage(1.8),
    includeFontPadding: false,
    textAlign: "center"
  }
});

OneWayTicketDetails.propTypes = {
  airlineName: PropTypes.string.isRequired,
  airPlaneName: PropTypes.string.isRequired,
  startDate: PropTypes.string.isRequired,
  outbound: PropTypes.string.isRequired,
  flightnum: PropTypes.string.isRequired,
  departureAirport: PropTypes.string.isRequired,
  departureCity: PropTypes.string.isRequired,
  destinationAirport: PropTypes.string.isRequired,
  destinationCity: PropTypes.string.isRequired,
  stopDetails: PropTypes.string.isRequired,
  departureTime: PropTypes.string.isRequired,
  destinationTime: PropTypes.string.isRequired,
  duration: PropTypes.string.isRequired,
  Refferance: PropTypes.string.isRequired,
  pnr: PropTypes.string.isRequired,
  ticket: PropTypes.string.isRequired,
  ticketNum: PropTypes.string.isRequired,
  destinationAirPlane: PropTypes.string.isRequired,
  cost: PropTypes.string.isRequired,
  toman: PropTypes.string.isRequired,
  acceptText: PropTypes.string.isRequired,
  cabin: PropTypes.string.isRequired,
  cabinInfo: PropTypes.string.isRequired,
  checked: PropTypes.string.isRequired,
  checkedInfo: PropTypes.string.isRequired,
  fareRule: PropTypes.string.isRequired,
  stopDetaildepartureAirport: PropTypes.string.isRequired,
  stopDetaildepartureCity: PropTypes.string.isRequired,
  stopDetaildepartureDate: PropTypes.string.isRequired,
  stopDetaildepartureTime: PropTypes.string.isRequired,
  stopDetaildestinationAirport: PropTypes.string.isRequired,
  stopDetaildestinationCity: PropTypes.string.isRequired,
  stopDetaildestinationDate: PropTypes.string.isRequired,
  stopDetaildestinationTime: PropTypes.string.isRequired
};
OneWayTicketDetails.defaultProps = {
  airlineName: "American Airlines",
  airPlaneName: "Airbus: A125 -510",
  startDate: "06 Jun, Mon, 2018",
  outbound: "Outbound",
  flightnum: "Flight: AN 78",
  departureAirport: "IKA",
  departureCity: "Tehran",
  destinationAirport: "DCC",
  destinationCity: "Paris",
  stopDetails: "1 Stop for 1H & 20MDubai (DXB)",
  departureTime: "00:10",
  destinationTime: "11:11",
  duration: "6h 10m",
  Refferance: "Refferance",
  pnr: "BF5457G",
  ticket: "e-Ticket",
  ticketNum: "05 214532",
  destinationAirPlane: "Airbus: A125 -510",
  cost: "7164700",
  toman: "تومان",
  acceptText: "تایید و ادامه",
  cabin: "Cabin",
  cabinInfo: "7KG",
  checked: "Checked",
  checkedInfo: "75KG",
  fareRule:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo mauris, tincidunt et justo sed, rhoncus ultricies risus. Integer ac sem risus. Sed mollis venenatis sagittis. Aliquam in leo sollicitudin nunc iaculis bibendum. Etiam vel justo congue, auctor tortor vitae, sollicitudin massa. Praesent finibus, diam quis tempus dapibus, neque erat porta velit, et tempus mi libero sed tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque vulputate ut nisi vel semper. Phasellus convallis sem egestas purus mattis finibus. Integer pulvinar nisl lectus, et sollicitudin massa aliquet sed. Proin turpis magna, pretium eget ullamcorper id, accumsan et orci. Donec tincidunt augue a nulla blandit, sed eleifend lorem fringilla. Morbi sodales ex eget libero placerat, vitae maximus dolor gravidamAliquam a leo et diam euismod sagittis sit amet at sem. Donec placerat sodales cursus. Maecenas pharetra urna est, eget elementum turpis aliquam ut. Fusce feugiat malesuada purus, a consequat dui dapibus sit amet. Donec felis erat, scelerisque a nulla a, viverra ornare risus. Pellentesque congue purus non mi finibus, at aliquam risus lacinia. Vivamus ultrices, mauris quis fermentum sagittis, odio odio ultricies velit, non vestibulum dolor diam blandit nunc. Suspendisse aliquam laoreet ante eu vestibulum. Cras eu tincidunt dui, et finibus magna. Vestibulum efficitur fermentum diam, ac fringilla mi facilisis vel. Nam suscipit ullamcorper nibh, vitae consequat risus semper id. Nam quis risus et nisl convallis pharetra. Nulla euismod ipsum non nibh cursus tempus. Quisque id lorem tincidunt massa blandit convallis eget eget lorem. Donec bibendum dolor id felis condimentum porttitor. Suspendisse tristique lectus id interdum ultricies.Nullam varius nulla non sapien sagittis vulputate. Mauris ac pulvinar justo, eu laoreet sem. Ut non auctor leo. Donec sollicitudin ex quis lorem pharetra tincidunt. Pellentesque tellus massa, dapibus nec placerat nec, hendrerit eget augue. Mauris rhoncus scelerisque sem, vel lacinia orci malesuada ultricies. Duis placerat tellus sit amet interdum laoreet. Sed nulla nulla, fermentum vel suscipit eget, laoreet non lectus. In nunc lectus, feugiat sed cursus pretium, interdum ut arcu. Aliquam rutrum sollicitudin malesuada. Etiam hendrerit lobortis ex nec mollis. Etiam aliquet enim non magna faucibus gravida. Donec maximus lobortis maximus. Praesent arcu nibh, placerat eu dapibus ac, pellentesque at elit. Proin elementum aliquet dapibus. Nunc dignissim volutpat porta. Pellentesque gravida dui sed dolor ultricies, eu tempor tortor hendrerit. Phasellus nec blandit nibh, sit amet iaculis ante. Cras sit amet magna luctus, feugiat nisi ut, commodo purus.Etiam a nisi magna. Maecenas luctus sodales luctus. Vestibulum ultrices neque nec dictum feugiat. Nam sed lorem ut nibh vulputate maximus. Aenean urna nibh, tristique id mauris vitae, congue faucibus tortor. In in nisl facilisis, dapibus libero sed, vulputate libero. Duis ligula lacus, efficitur in ex id, egestas euismod turpis. Donec ullamcorper dolor ac aliquet fringilla. In id odio in metus scelerisque iaculis eget quis ex. Curabitur at elit fringilla, tempor nisl ut, facilisis magna. Praesent id luctus erat. Sed tincidunt tortor ac nisi egestas vehicula. In eu interdum risus. Nullam vestibulum felis vel vehicula consequat. Nullam et ex odio. ",
  stopDetaildepartureAirport: "IKA",
  stopDetaildepartureCity: "Tehran",
  stopDetaildepartureDate: "شنبه 12 فروردین",
  stopDetaildepartureTime: "ساعت 00:00",
  stopDetaildestinationAirport: "IST",
  stopDetaildestinationCity: "Istanbul",
  stopDetaildestinationDate: "شنبه 12 فروردین",
  stopDetaildestinationTime: "ساعت 00:00"
};
