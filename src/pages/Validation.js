import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableNativeFeedback,
  TouchableOpacity,
  KeyboardAvoidingView
} from "react-native";
import CodeInput from "react-native-confirmation-code-input";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import MainSearchNavigation from "../utilities/navigations/MainSearchNavigation";
import WatermarkIconBlack from "../components/WatermarkIconBlack.js";
import BtnGradient from "../components/BtnGradient.js";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
export default class Validation extends Component {
  constructor(props) {
    super(props);
  }
  _onFinishCheckingCode1(isValid) {
    if (isValid) this.props.navigation.navigate("MainSearchNavigation");
  }
  static navigationOptions = {
    header: null
  };
  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="height"
        enabled={false}
      >
        <View style={styles.logoContainer}>
          <Icon name="logo" color="#194A99" size={80} />
          <Icon name="typography" color="#194A99" size={50} />
        </View>

        <View style={styles.textContainer}>
          {/* <Text style={styles.text}>کد تاییدیه برای شما ارسال شد</Text> */}
          <Text style={styles.text}>لطفاً کد دریافتی را وارد نمایید</Text>
          <View style={styles.resendCode}>
            <Text>ارسال مجدد پیامک تاییدیه</Text>
          </View>
        </View>

        <View style={styles.sendCode}>
          <CodeInput
            codeLength={4}
            ref="codeInputRef5"
            className="border-b"
            space={30}
            size={60}
            inputPosition="center"
            activeColor="#685F86"
            inactiveColor="#685F86"
            autoFocus={false}
            ignoreCase={true}
            compareWithCode="1234"
            keyboardType="numeric"
            height="100%"
            codeInputStyle={styles.codeInputStyle}
            containerStyle={styles.inputStyle}
            onFulfill={isValid => this._onFinishCheckingCode1(isValid)}
          />
        </View>

        <View style={styles.btnSearch}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("MainSearchNavigation")
            }
          >
            <BtnGradient type="ورود" />
          </TouchableOpacity>
        </View>

        <WatermarkIconBlack />
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column",
    alignItems: "center"
  },
  logoContainer: {
    alignItems: "center",
    marginTop: "10%"
  },
  textContainer: {
    alignItems: "center",
    marginTop: "7%"
  },
  text: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.9),
    textAlign: "center",
    alignItems: "center",
  },
  sendCode: {
    alignItems: "center",
    height: "5%",
    width: "100%"
  },
  codeInputStyle: {
    fontSize: RFPercentage(3.4),
    fontFamily: "Sahel"
  },
  inputStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 0,
    marginTop: "9%"
  },
  resendCode: {
    marginTop: "8%",
    fontSize: RFPercentage(2.1),
    fontFamily: "Sahel",
    alignItems: "center",
    textAlign: "center"
  },
  btnSearch: {
    height: "14.6%",
    width: "85%",
    marginTop: "19%",
    justifyContent: "center"
  }
});
