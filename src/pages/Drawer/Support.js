import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  ScrollView,
  AsyncStorage,
  Text,
  TouchableOpacity,
  Linking,
  Platform
} from "react-native";
import React, { Component } from "react";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import Appbar from "../../components/AppbarTitle";
import ImageInformation from "../../Images/information.png";
import { RFPercentage, RFValue }  from "react-native-responsive-fontsize";
import WaterMark from "../../components/WatermarkIconBlack";
const Icon = createIconSetFromFontello(fontelloConfig);
import PropTypes from "prop-types";

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phoneNumber: 2324
    };
  }
  // static propTypes = { url: 'https://samseir.com' };
  openWebsite = () => {
    Linking.canOpenURL("https://samseir.com").then(supported => {
      if (supported) {
        Linking.openURL("https://samseir.com");
      } else {
        console.log("Don't know how to open URI: " + "https://samseir.com");
      }
    });
  };
  openMap = () => {
    const scheme = Platform.select({
      ios: "maps:0,0?q=",
      android: "geo:0,0?q="
    });
    const latLng = `${35.787567},${51.373288}`;
    const label = "آژانس هواپیمایی سام سیر";
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });

    Linking.openURL(url);
  };
  openCall = () => {
    let phone = "0212324";
    // Linking.openURL(`tel:${phoneNumber}`);

    console.log("callNumber ----> ", phone);
    let phoneNumber = phone;
    if (Platform.OS !== "android") {
      phoneNumber = `tel://${phone}`;
    } else {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          console.log(supported);
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  // componentDidMount(){
  //     _retrieveData = async () => {
  //         try {
  //           const value = await AsyncStorage.getItem('isWalkThrough');
  //           if (value !== null) {
  //             this.setState({userId : 1})
  //             console.log(value);
  //             loginGlobal =1
  //           }
  //         } catch (error) {
  //           // Error retrieving data
  //         }
  //       };
  // }
  // static navigationOptions = {
  //     drawerLabel : loginGlobal ?  true : null,
  //     drawerIcon : loginGlobal ? null : () => <Icon name="aboutUs" color="#FFBA00" size={20} />

  //   };
  navigateToBack() {
    this.props.navigation.navigate("FlightOptions");
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Appbar
          pageTitle="ارتباط با سام سیر"
          backIconColor="#11397F"
          titleTextColor="#11397F"
          navigateTo={this.navigateToBack.bind(this)}
        />
        <View style={styles.informationContainer}>
          <Image source={ImageInformation} />
          <View style={{ width: "100%", marginTop: "20%" }}>
            <TouchableOpacity
              style={styles.locationContainer}
              onPress={this.openCall}
            >
              <Icon name="call" style={styles.earthIcon} size={20} />
              <Text style={styles.siteText}>021-2324</Text>
            </TouchableOpacity>
            <View style={styles.locationContainer}>
              <Icon name="earth" style={styles.earthIcon} size={20} />
              <TouchableOpacity onPress={this.openWebsite}>
                <Text style={styles.siteText}>www.samseir.com</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.locationContainer}
              onPress={this.openMap}
            >
              <Icon name="location" style={styles.earthIcon} size={23} />
              <View>
                <Text style={styles.addressText}>
                  No.191, Opposite of Modarres
                </Text>
                <Text style={styles.addressText}>Hospital, Saadat Abad</Text>
                <Text style={styles.addressText}>1998633516, Tehran-IRAN</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <WaterMark />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  informationContainer: {
    // justifyContent: "center",
    alignItems: "center",
    marginStart: "19%",
    marginEnd: "19%",
    marginTop: "10%",
    // backgroundColor: "yellow",
    flex: 1
  },
  siteContainer: {
    flexDirection: "row",
    alignItems: "center",
    width: "100%",
    marginTop: "10%"
  },
  earthIcon: {
    color: "#FFBA00",
    width: "20%"
  },
  siteText: {
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2),
    color: "#11397F",
    width: "100%",
    includeFontPadding  : false

    // textAlign: "center"
  },
  addressText: {
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2),
    color: "#11397F",
    width: "100%",
    includeFontPadding  : false

    // textAlign: "center"
  },
  locationContainer: {
    flexDirection: "row",
    // backgroundColor: "red",
    // justifyContent: "space-between",
    width: "100%",
    marginTop: 25
  },
  phoneNumberContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "orange",
    // width : '50%',
    height: "27%",
    justifyContent: "space-evenly",
    borderRadius: 30,
    padding: 10
  },
  calBtn: {
    flex: 1
  },
  phoneNumber: {
    color: "#15479F",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2.5),
    // width :'60%',
    // height : '20%',
    textAlign: "center"
    // backgroundColor : 'green'
  }
});
