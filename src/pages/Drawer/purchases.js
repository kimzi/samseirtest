import {
  StyleSheet,
  View,
  FlatList,
  Image,
  ScrollView,
  AsyncStorage,
  Text,
  TouchableOpacity
} from "react-native";
import React, { Component } from "react";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import Appbar from "../../components/AppbarTitle";
import { RFPercentage, RFValue }  from "react-native-responsive-fontsize";
import WatermarkIcon from '../../components/WatermarkIcon'
let loginGlobal = null;
export default class Purchases extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          Id: 0,
          PNRText: "کد رزرو",
          PNR: "BF5457G",
          DateText: "تاریخ خرید",
          Date: "2019/05/02",
          PassengerNumber: "5نفر",
          Time: "07:35",
          Departure: "تهران ",
          Destination: "ونکوور",
          GregorianFlightDate: "06 Jun, 2018 ",
          JalaliFlightDate: "3 اردیبهشت 1398",
          InformationText: "بازگشت مبلغ به سام پی انجام شده است",
          InformationType: 2,
          Status: false
        },
        {
          Id: 1,
          PNRText: "کد رزرو",
          PNR: "BF5457G",
          DateText: "تاریخ خرید",
          Date: "2019/05/02",
          PassengerNumber: "5نفر",
          Time: "07:35",
          Departure: "تهران",
          Destination: "ونکوور",
          GregorianFlightDate: "06 Jun, 2018 ",
          JalaliFlightDate: "3 اردیبهشت 1398",
          InformationText: "3 روز تا این پرواز زمان دارید ",
          InformationType: 1,
          Status: true
        }
      ]
    };
  }
  navigateToBack() {
    this.props.navigation.navigate("FlightOptions");
  }
  _renderItem = ({ item }) => (
    <TouchableOpacity
      style={[
        styles.BtnContainer,
        { borderColor: item.Status ? "#4CAF50" : "#F63323" }
      ]}
    >
      <View>
        <View style={styles.DateItemContainer}>
          <Text style={styles.DateText}>
            {item.PNRText}: {item.PNR}
          </Text>
          <Text style={styles.DateText}>
            {item.DateText}: {item.Date}
          </Text>
        </View>
        <View style={[styles.DateItemContainer, { marginTop: 5 }]}>
          <View style={styles.passengerNumberContainer}>
            <Icon
              name="passengers"
              color="#15479F"
              size={20}
              style={{ paddingLeft: 5 }}
            />
            <Text style={styles.timeText}>{item.PassengerNumber}</Text>
          </View>
          <View style={styles.passengerNumberContainer}>
            <Text style={styles.timeText}>{item.Time}</Text>
            <Icon
              name="time"
              color="#15479F"
              size={20}
              style={{ paddingRight: 5 }}
            />
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            marginTop: 10,
            width : '100%'
          }}
        >
          <View style={[styles.passengerNumberContainer , { width : '25%' , justifyContent : 'center' , alignItems : 'center' , paddingLeft : 5}]}>
            <Icon
              name="departure"
              color="#15479F"
              size={15}
              style={{ paddingLeft: 5 }}
            />
            <Text numberOfLines={2} style={[styles.flightText ,  {flexShrink: 1 , }]}>{item.Departure}</Text>
          </View>
          <View style={{ alignItems: "center", justifyContent: "center" , width : '50%'}}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              {/* <Icon
                name="calander"
                color="#15479F"
                size={15}
                style={{ paddingRight: 5 }}
              /> */}
              <Text
                style={[
                  styles.flightText,
                  { fontFamily: "Montserrat-Regular",justifyContent : 'center' , textAlign : 'center' , alignItems : 'center'}
                ]}
              >
                {item.GregorianFlightDate}
              </Text>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Icon color="#91A9CD" name="substraction" size={12} />
              <View
                style={{
                  height: 1,
                  borderWidth: 0.5,
                  borderColor: "#91A9CD",
                  marginLeft: 3,
                  marginRight: 3,
                  width: "70%"
                }}
              />
              <Icon color="#91A9CD" name="substraction" size={12} />
            </View>
            <View>
              <Text  numberOfLines={2} style={[styles.flightText , {justifyContent : 'center' , textAlign : 'center' , alignItems : 'center'}]}>{item.JalaliFlightDate}</Text>
            </View>
          </View>
          <View style={[styles.passengerNumberContainer , { width : '25%' ,justifyContent : 'center' , alignItems : 'center' , paddingRight : 10 }]}>
            <Icon
              name="destination"
              color="#15479F"
              size={15}
              style={{ paddingLeft: 5}}
            />
            <Text numberOfLines={2} style={[styles.flightText , {flexShrink: 1}]}>{item.Destination}</Text>
          </View>
        </View>
        <View style={styles.dividerContainer}>
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
          <View style={styles.dash} />
        </View>
        <View style={{justifyContent : 'center'}}>
          <Text style={{color : item.InformationType == 1 ? '#F09819' : '#8A8795' , textAlign : 'right' , marginTop : 5}}>{item.InformationText}</Text>
        </View>
        <View
              style={[
                styles.ticketTypeContainer,
                { borderColor: item.Status ?  "#4CAF50" : "#F63323"}
              ]}
            >
              <Text
                style={[
                  styles.ticketTypeText,
                  { color: item.Status ?  "#4CAF50" : "#F63323"}
                ]}
              >
                {!item.Status  ? "ناموفق" : "موفق"}
              </Text>
            </View>

      </View>
    </TouchableOpacity>
  );

  render() {
    return (
      <View style={styles.container}>
        <Appbar
          pageTitle="خریدهای قبلی"
          backIconColor="#FFFFFF"
          titleTextColor="#FFFFFF"
          navigateTo={this.navigateToBack.bind(this)}
        />
        <FlatList
          style={{ flex: 1, height: 100 }}
          data={this.state.data}
          renderItem={this._renderItem}
        />
       <WatermarkIcon zIndexWatermark= {-1} />

      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#1B275A",
    flex: 1
  },
  BtnContainer: {
    backgroundColor: "#FFFFFF",
    marginTop: 5,
    marginStart: "5%",
    marginEnd: "5%",
    paddingTop: 20,
    paddingRight : 20,
    paddingBottom : 10,
    paddingLeft : 30,
    borderRadius: 6,
    borderWidth: 2
  },
  DateItemContainer: {
    justifyContent: "space-between",
    flexDirection: "row-reverse"
  },
  passengerNumberContainer: {
    flexDirection: "row-reverse",
    alignItems: "center"
  },
  DateText: {
    color: "#8A8795",
    fontSize: RFPercentage(2.1),
    fontFamily: "Sahel"
  },
  timeText: {
    color: "#15479F",
    fontSize: RFPercentage(2),
    fontFamily: "Sahel"
  },
  flightText: {
    color: "#15479F",
    fontSize: RFPercentage(2.1),
    fontFamily: "Sahel"
  },
  dividerContainer: {
    flexDirection: "row",
    height: 1,
    alignItems: "center",
    justifyContent: "space-between",
    marginTop: 10
  },
  dash: {
    width: "2%",
    height: 1,
    backgroundColor: "#B4BECC"
  },
  ticketTypeContainer: {
    width: 20,
    height: 60,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    backgroundColor: "#FFFFFF",
    position: "absolute",
    left: -30,
    zIndex: 1,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    top: "31%",
    borderWidth : 1,
    borderLeftWidth : 0
  },
  ticketTypeText: {
    transform: [{ rotate: "90deg" }],
    justifyContent: "center",
    alignItems: "center",
    width: 46,
    fontSize: RFPercentage(1.6),
    paddingTop: 0,
    // paddingRight: 4,
    color: "#3B2889",
    textAlign: "center"
  },


});
