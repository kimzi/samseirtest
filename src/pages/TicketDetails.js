import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  ScrollView,
  StatusBar,
  Alert
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import PropTypes from "prop-types";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Airline from "../Images/TS.png";
import { PIXEL_RATIO } from "../constants/DeviceParameters";
import WaterMark from "../components/WatermarkIcon";
import Btn from "../components/BtnGradientCheckmark";
import traveler from "../Images/traveler.png";
import bag from "../Images/bag.png";
import AirLineSearchResult from "./MainSearch/AirLineSearchResult";
import PassengersInformation from "./PassengersInformation";
import LoginTest from "./LoginTest";
import Rectangle from "../Images/Rectangle.png";
let circleRadius = PIXEL_RATIO > 2 ? 20 : 15;

export default class TicketDetails extends Component {
  state = {
    modalVisible: false,
    stopModalVisible: false
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setStopModalVisible(visible) {
    this.setState({ stopModalVisible: visible });
  }

  dash() {
    return (
      <View style={styles.dashContainer}>
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
        <View style={styles.dash} />
      </View>
    );
  }

  render() {
    return (
      <LinearGradient
        colors={["#1B275A", "#183579", "#15479F"]}
        style={styles.mainLinerGradient}
      >
        <StatusBar backgroundColor="#1B275A" />

        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.stopModalVisible}
          onRequestClose={() => {
            this.setStopModalVisible(!this.state.stopModalVisible);
          }}
        >
          <TouchableOpacity
            style={{ flex: 1 }}
            activeOpacity={1}
            onPressOut={() => {
              this.setStopModalVisible(!this.state.stopModalVisible);
            }}
          >
            <LinearGradient
              colors={["#15479F", "#7E3E65", "#F63323"]}
              style={styles.modalLinerGradient}
            >
              <StatusBar backgroundColor="#15479F" />
              <TouchableWithoutFeedback>
                <View style={styles.stopModalContainer}>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row-reverse",
                      paddingTop: 15,
                      paddingLeft: 15
                    }}
                    onPress={() => {
                      this.setStopModalVisible(!this.state.stopModalVisible);
                    }}
                  >
                    <Icon name="cancel" />
                  </TouchableOpacity>
                  <View style={styles.titleContainer}>
                    <Text style={styles.titleText}>
                      جزئیات توقف‌های این سفر
                    </Text>
                  </View>
                  <View style={styles.stopDetailsContainer}>
                    <View style={styles.squareContainer}>
                      <View style={styles.departureDestinationAirportContainer}>
                        <Text style={[styles.stopAirportText]}>
                          {this.props.stopDetaildepartureAirport}
                        </Text>
                        <Text style={styles.stopCityText}>
                          {this.props.stopDetaildepartureCity}
                        </Text>
                        <Text
                          style={[
                            styles.stopDateText,
                            { textAlign: "left", marginTop: 5 }
                          ]}
                        >
                          {this.props.stopDetaildepartureDate}
                        </Text>
                        <Text
                          style={[styles.stopDateText, { textAlign: "left" }]}
                        >
                          {this.props.stopDetaildepartureTime}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.flightDetailsContainer}>
                      <View
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          flex: 1
                        }}
                      >
                        <View
                          style={{
                            flexDirection: "row-reverse",
                            alignItems: "center"
                          }}
                        >
                          <View>
                            <Text style={styles.flightNumbers}>
                              Flight Number: 8694
                            </Text>
                            <Text style={styles.flightNumbers}>
                              Airplane: Airbus 321
                            </Text>
                          </View>
                          <Image
                            source={Airline}
                            style={{ width: 20, height: 20 }}
                          />
                        </View>
                        {/* <View
                      style={{
                        width: "80%",
                        height: 10,
                        backgroundColor: "#000"
                      }}
                    /> */}
                        <Icon name="stopicon" />
                        <View>
                          <Text style={styles.stopTime}>Stop for 1H & 20M</Text>
                        </View>
                      </View>
                    </View>
                    <View style={styles.squareContainer}>
                      <View
                        style={[
                          styles.departureDestinationAirportContainer,
                          { marginRight: 10 }
                        ]}
                      >
                        <Text
                          style={[
                            styles.stopAirportText,
                            { textAlign: "right" }
                          ]}
                        >
                          {this.props.stopDetaildestinationAirport}
                        </Text>
                        <Text
                          style={[styles.stopCityText, { textAlign: "right" }]}
                        >
                          {this.props.stopDetaildestinationCity}
                        </Text>
                        <Text
                          style={[
                            styles.stopDateText,
                            { textAlign: "right", marginTop: 5 }
                          ]}
                        >
                          {this.props.stopDetaildestinationDate}
                        </Text>
                        <Text
                          style={[styles.stopDateText, { textAlign: "right" }]}
                        >
                          {this.props.stopDetaildestinationTime}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <TouchableOpacity
                    style={styles.modalBtnContainer}
                    onPress={() => {
                      this.setStopModalVisible(!this.state.stopModalVisible);
                    }}
                  >
                    <Btn type="tik" />
                  </TouchableOpacity>
                </View>
              </TouchableWithoutFeedback>
            </LinearGradient>
          </TouchableOpacity>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        >
          <LinearGradient
            colors={["#15479F", "#7E3E65", "#F63323"]}
            style={styles.modalLinerGradient}
          >
            <StatusBar backgroundColor="#15479F" />
            <View style={styles.modalContainer}>
              <View style={styles.fairRuleContainer}>
                <View style={styles.modalHeaderContainer}>
                  <View style={styles.cabinCheckedContainer}>
                    <Image
                      source={traveler}
                      style={styles.travelerImageStyle}
                    />
                    <View>
                      <Text style={styles.cabinChecked}>
                        {this.props.cabin}
                      </Text>
                      <Text style={styles.cabinCheckedDetails}>
                        {this.props.cabinInfo}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.cabinCheckedContainer}>
                    <Image source={bag} style={styles.bagImageStyle} />
                    <View>
                      <Text style={styles.cabinChecked}>
                        {this.props.checked}
                      </Text>
                      <Text style={styles.cabinCheckedDetails}>
                        {this.props.checkedInfo}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.ruleDetailsContainer} />
                <ScrollView style={styles.ruleScrollView}>
                  <Text style={styles.fareRuleText}>
                    Full Rules and Details
                  </Text>
                  <Text style={styles.fareRuleDetails}>
                    {this.props.fareRule}
                  </Text>
                </ScrollView>
                <TouchableOpacity
                  style={styles.modalBtnContainer}
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Btn type="tik" />
                </TouchableOpacity>
                <Image source={Rectangle} style={styles.rectangleImage} />
              </View>
            </View>
          </LinearGradient>
        </Modal>

        <View>
          <Icon
            name="back"
            style={styles.back}
            onPress={() =>
              this.props.navigation.navigate("AirLineSearchResult")
            }
          />

          <View
            style={{
              backgroundColor: "#ffffff",
              marginLeft: "7%",
              marginRight: "7%",
              marginTop: "0.5%",
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40,
              height: "75%"
            }}
          >
            <View style={styles.hearderStyle}>
              <View style={styles.airlineImage}>
                <Image source={Airline} style={styles.airlineImage} />
              </View>

              <View style={styles.airplaneContainer}>
                <Text style={styles.airlineName}>{this.props.airlineName}</Text>
                <Text>{this.props.airPlaneName}</Text>
              </View>
            </View>

            <View style={styles.dividerContainer}>
              <View style={styles.topCircle} />

              {/* 38 */}
              {this.dash()}
              <View style={styles.bottomCircle} />
            </View>

            <ScrollView style={styles.ticketContainer}>
              <View style={styles.startDateDetails}>
                <View>
                  <Text style={styles.startDate}>{this.props.startDate}</Text>
                </View>

                <View style={styles.boundflightContainer}>
                  <View>
                    <Text style={styles.redText}>{this.props.outbound}</Text>
                  </View>

                  <View>
                    <Text style={styles.redText}>{this.props.flightnum}</Text>
                  </View>
                </View>

                <View style={styles.destinationDepartureDetails}>
                  <View>
                    <View style={styles.columnDirection}>
                      <Text style={styles.airportText}>
                        {this.props.departureAirport}
                      </Text>
                    </View>
                    <View>
                      <Text style={styles.cityText}>
                        {this.props.departureCity}
                      </Text>
                    </View>
                  </View>

                  <TouchableOpacity
                    style={styles.stopDetailsTextContainer}
                    onPress={() => {
                      this.setStopModalVisible(!this.state.stopModalVisible);
                    }}
                  >
                    <Text numberOfLines={2} style={styles.stopDetailsText}>
                      {this.props.stopDetails}
                    </Text>
                    <Icon color="#F09819" name="right" />
                  </TouchableOpacity>

                  <View>
                    <View>
                      <Text style={styles.airportText}>
                        {this.props.destinationAirport}
                      </Text>
                    </View>
                    <View>
                      <Text style={[styles.cityText, { textAlign: "right" }]}>
                        {this.props.destinationCity}
                      </Text>
                    </View>
                  </View>
                </View>

                <View style={styles.timeContainer}>
                  <View>
                    <Text style={styles.time}>{this.props.departureTime}</Text>
                  </View>

                  <View style={styles.durationContainer}>
                    <Text style={styles.duration}>{this.props.duration}</Text>
                  </View>

                  <View>
                    <Text style={styles.time}>
                      {this.props.destinationTime}
                    </Text>
                  </View>
                </View>

                <View style={styles.dashWrapper}>{this.dash()}</View>

                <View
                  style={[
                    styles.destinationDepartureDetails,
                    { marginTop: 2.5 }
                  ]}
                >
                  <View>
                    <View style={styles.columnDirection}>
                      <Text style={styles.Refferance}>
                        {this.props.Refferance}
                      </Text>
                    </View>

                    <View>
                      <Text style={styles.pnr}>{this.props.pnr}</Text>
                    </View>
                  </View>

                  <View>
                    <View>
                      <Text style={styles.Refferance}>{this.props.ticket}</Text>
                    </View>

                    <View>
                      <Text style={styles.pnr}>{this.props.ticketNum}</Text>
                    </View>
                  </View>
                </View>
              </View>

              <View style={styles.grayViewContainer}>
                <View style={styles.grayCircleContainer}>
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />

                  <View style={styles.grayCircle} />
                  <View style={styles.grayCircle} />
                </View>

                <View style={styles.startDateDetails}>
                  <View>
                    <Text style={styles.startDate}>{this.props.startDate}</Text>
                  </View>

                  <View style={styles.boundflightContainer}>
                    <View>
                      <Text style={styles.redText}>{this.props.outbound}</Text>
                    </View>

                    <View>
                      <Text style={styles.redText}>{this.props.flightnum}</Text>
                    </View>
                  </View>

                  <View style={styles.destinationDepartureDetails}>
                    <View>
                      <View style={styles.columnDirection}>
                        <Text style={styles.airportText}>
                          {this.props.departureAirport}
                        </Text>
                      </View>

                      <View>
                        <Text style={styles.cityText}>
                          {this.props.departureCity}
                        </Text>
                      </View>
                    </View>

                    <View style={styles.stopDetailsTextContainer}>
                      <Text numberOfLines={2} style={styles.stopDetailsText}>
                        {this.props.stopDetails}
                      </Text>

                      <Icon color="#F09819" name="right" />
                    </View>

                    <View>
                      <View>
                        <Text style={styles.airportText}>
                          {this.props.destinationAirport}
                        </Text>
                      </View>

                      <View>
                        <Text style={[styles.cityText, { textAlign: "right" }]}>
                          {this.props.destinationCity}
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View style={styles.timeContainer}>
                    <View>
                      <Text style={styles.time}>
                        {this.props.departureTime}
                      </Text>
                    </View>

                    <View style={styles.durationContainer}>
                      <Text style={styles.duration}>{this.props.duration}</Text>
                    </View>

                    <View>
                      <Text style={styles.time}>
                        {this.props.destinationTime}
                      </Text>
                    </View>
                  </View>

                  <View style={styles.destinationAirPlaneContainer}>
                    <Text style={styles.destinationAirPlane}>
                      {this.props.destinationAirPlane}
                    </Text>
                  </View>
                  {this.dash()}
                  <View
                    style={[
                      styles.destinationDepartureDetails,
                      { marginTop: 2.5, marginBottom: "20%" }
                    ]}
                  >
                    <View>
                      <View style={styles.columnDirection}>
                        <Text style={styles.Refferance}>
                          {this.props.Refferance}
                        </Text>
                      </View>

                      <View>
                        <Text style={styles.pnr}>{this.props.pnr}</Text>
                      </View>
                    </View>

                    <View>
                      <View>
                        <Text style={styles.Refferance}>
                          {this.props.ticket}
                        </Text>
                      </View>

                      <View>
                        <Text style={styles.pnr}>{this.props.ticketNum}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </ScrollView>

            <View style={styles.blueCircleContainer}>
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
              <View style={styles.blueCircle} />
            </View>
          </View>

          <View
            style={{
              alignItems: "center",
              zIndex: 5,
              marginTop: 0,
              height: "4%",
              justifyContent: "center"
            }}
          >
            <TouchableOpacity
              style={{ flexDirection: "row", alignItems: "center" }}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}
            >
              <Text style={styles.flightDetails}>
                Flight and baggage details
              </Text>

              <Icon color="#ffffff" name="right" />
            </TouchableOpacity>
          </View>

          <View
            style={{
              alignItems: "center",
              height: "8%",
              justifyContent: "center",
              marginTop: "5%"
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("LoginTest")}
              style={styles.BtnContainer}
            >
              <View style={{ flexDirection: "row-reverse" }}>
                <Text style={[styles.BtnText, { marginLeft: 10 }]}>
                  {this.props.cost}
                </Text>

                <Text style={styles.BtnText}>{this.props.toman}</Text>
              </View>

              <Text style={styles.BtnText}>{this.props.acceptText}</Text>
            </TouchableOpacity>
          </View>
        </View>

        <WaterMark />
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  stopAirportText: {
    fontFamily: "Montserrat-Bold",
    fontSize: RFPercentage(1.8),
    color: "#313841"
  },
  flightDetailsContainer: {
    height: "25%"
  },
  flightNumbers: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(1.3),
    color: "#7A7F85",
    textAlign: "center"
  },
  stopTime: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.3),
    color: "#CD1F16"
  },
  stopCityText: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(1.5),
    color: "#7A7F85"
  },
  stopDateText: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.5),
    color: "#15479F"
  },
  titleContainer: {
    alignItems: "center",
    marginTop: 10,
    height: "10%",
    justifyContent: "center"
  },
  stopDetailsContainer: {
    justifyContent: "space-around",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 10
  },
  departureDestinationAirportContainer: {
    marginTop: 10,
    marginLeft: 10
  },
  squareContainer: {
    width: 95,
    height: 95,
    backgroundColor: "#E9EEF6",
    borderRadius: 10
  },
  titleText: {
    fontFamily: "Sahel-Bold",
    color: "#15479F",
    fontSize: RFPercentage(2.3)
  },
  back: {
    color: "#ffffff",
    fontSize: RFPercentage(2.2),
    padding: 16,
    backgroundColor: "red"
  },
  mainLinerGradient: {
    flex: 1
  },
  modalLinerGradient: {
    position: "absolute",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    // backgroundColor: "rgba(21,71,159, 100)"
    backgroundColor: "red",
    zIndex: -100
  },
  modalContainer: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  fairRuleContainer: {
    height: "80%",
    backgroundColor: "#ffffff",
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40
  },
  modalHeaderContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10
  },
  cabinCheckedContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  travelerImageStyle: {
    marginRight: 5
  },
  bagImageStyle: {
    marginRight: 5
  },
  ruleDetailsContainer: {
    borderWidth: 0.5,
    borderColor: "#CDDCF3",
    marginTop: 10
  },
  fareRuleText: {
    fontSize: RFPercentage(2.3),
    fontFamily: "Montserrat-Regular",
    color: "#15479F"
  },
  ruleScrollView: { flex: 1, padding: 30 },
  fareRuleDetails: {
    fontSize: RFPercentage(2),
    fontFamily: "Montserrat-Regular",
    color: "#7E7C85",
    paddingTop: 10
  },
  modalBtnContainer: {
    width: 150,
    height: "8%",
    alignSelf: "center",
    position: "absolute",
    bottom: 20,
    zIndex: 2
  },
  rectangleImage: {
    position: "absolute",
    zIndex: 1,
    bottom: 0
  },
  ticketContainer: {
    backgroundColor: "#ffffff"
  },
  airlineImage: {
    position: "absolute",
    left: 10,
    top: 0
  },
  hearderStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  airlineName: {
    fontSize: RFPercentage(2),
    fontFamily: "Montserrat-Bold",
    color: "#313841",
    textAlign: "center"
  },
  airPlaneName: {
    fontFamily: "Lato-Regular",
    color: "#7A7F85",
    fontSize: RFPercentage(2.2),
    textAlign: "center"
  },
  airplaneContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  dividerContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%"
  },
  dash: {
    width: "1%",
    height: 1,
    backgroundColor: "#B2BAC5"
  },
  dashContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    flex: 1
  },
  topCircle: {
    width: 20,
    height: 20,
    borderRadius: 100,
    backgroundColor: "#1A2A61",
    marginLeft: -10
  },
  bottomCircle: {
    width: 20,
    height: 20,
    borderRadius: 100,

    backgroundColor: "#1A2A61",
    marginRight: -10
  },
  grayCircle: {
    // width: circleRadius,
    // height: circleRadius,
    // borderRadius: 100,
    height: circleRadius / 1.8,
    width: circleRadius / 1.5,
    borderTopRightRadius: circleRadius / 3,
    borderTopLeftRadius: circleRadius / 3,
    backgroundColor: "#eeeeee"
  },
  blueCircle: {
    // width: circleRadius,
    // height: circleRadius,
    // borderTopLeftRadius: 100,
    // borderTopRightRadius: 100,

    // backgroundColor: "#173F8D"
    height: circleRadius / 1.8,
    width: circleRadius / 1.5,
    borderTopRightRadius: circleRadius / 3,
    borderTopLeftRadius: circleRadius / 3,
    backgroundColor: "#173F8D"
  },
  grayViewContainer: {
    backgroundColor: "#eeeeee",
    marginTop: 12.5,
    justifyContent: "space-between",
    width: "100%"
  },
  grayCircleContainer: {
    flexDirection: "row",
    position: "absolute",
    left: 0,
    right: 0,
    top: -7,
    justifyContent: "space-between"
  },
  blueCircleContainer: {
    flexDirection: "row",
    paddingHorizontal: 2,
    marginTop: 0,
    justifyContent: "space-between",
    position: "absolute",
    left: 0,
    right: 0,
    bottom: -1
  },
  startDate: {
    fontSize: RFPercentage(2.8),
    fontFamily: "Montserrat-Regular",
    color: "#19A2F2"
  },
  startDateDetails: {
    paddingLeft: "5%",
    paddingRight: "5%",
    paddingTop: 0
  },
  boundflightContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "2%",
    paddingBottom: "2%"
  },
  redText: {
    color: "#E7342A",
    fontSize: RFPercentage(2),
    fontFamily: "Montserrat-Regular"
  },
  destinationDepartureDetails: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  airportText: {
    fontSize: RFPercentage(2.6),
    fontFamily: "Montserrat-Bold",
    color: "#313841"
  },
  cityText: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(2.1)
  },
  stopDetailsText: {
    fontSize: RFPercentage(1.6),
    fontFamily: "Montserrat-Regular",
    color: "#F09819",
    textAlign: "center"
  },
  timeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 5
  },
  time: {
    fontSize: RFPercentage(2.6),
    fontFamily: "Montserrat-Bold",
    color: "#313841"
  },
  duration: {
    fontSize: RFPercentage(1.9),
    fontFamily: "Montserrat-Regular",
    color: "#7E7C85"
  },
  durationContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  Refferance: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.9),
    color: "#7E7C85"
  },
  pnr: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(2.1),
    color: "#172434"
  },
  destinationAirPlaneContainer: {
    alignItems: "center",
    marginTop: 5,
    marginBottom: 10
  },
  destinationAirPlane: {
    fontFamily: "Lato-Regular",
    fontSize: RFPercentage(2.1),
    color: "#7A7F85"
  },
  flightDetails: {
    fontSize: RFPercentage(1.9),
    fontFamily: "Montserrat-Regular",
    color: "#ffffff",
    marginRight: 10
  },
  BtnText: {
    fontSize: RFPercentage(2.3),
    fontFamily: "Sahel-Bold",
    color: "#ffffff",
    textAlignVertical: "center"
  },
  cabinChecked: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(2.1),
    color: "#7E7C85"
  },
  cabinCheckedDetails: {
    fontFamily: "Montserrat-Bold",
    fontSize: RFPercentage(2.3),
    color: "#313841"
  },
  columnDirection: {
    flexDirection: "column"
  },
  stopDetailsTextContainer: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    width: "40%"
  },
  BtnContainer: {
    paddingRight: 10,
    paddingLeft: 10,
    backgroundColor: "#E7342A",
    justifyContent: "space-between",
    flexDirection: "row-reverse",
    alignItems: "center",
    borderRadius: 100,
    width: "70%",
    height: "100%"
  },
  stopModalContainer: {
    width: "90%",
    height: "60%",
    alignSelf: "center",
    borderRadius: 10,
    zIndex: 20,
    backgroundColor: "#ffffff"
  }
});

TicketDetails.propTypes = {
  airlineName: PropTypes.string.isRequired,
  airPlaneName: PropTypes.string.isRequired,
  startDate: PropTypes.string.isRequired,
  outbound: PropTypes.string.isRequired,
  flightnum: PropTypes.string.isRequired,
  departureAirport: PropTypes.string.isRequired,
  departureCity: PropTypes.string.isRequired,
  destinationAirport: PropTypes.string.isRequired,
  destinationCity: PropTypes.string.isRequired,
  stopDetails: PropTypes.string.isRequired,
  departureTime: PropTypes.string.isRequired,
  destinationTime: PropTypes.string.isRequired,
  duration: PropTypes.string.isRequired,
  Refferance: PropTypes.string.isRequired,
  pnr: PropTypes.string.isRequired,
  ticket: PropTypes.string.isRequired,
  ticketNum: PropTypes.string.isRequired,
  destinationAirPlane: PropTypes.string.isRequired,
  cost: PropTypes.string.isRequired,
  toman: PropTypes.string.isRequired,
  acceptText: PropTypes.string.isRequired,
  cabin: PropTypes.string.isRequired,
  cabinInfo: PropTypes.string.isRequired,
  checked: PropTypes.string.isRequired,
  checkedInfo: PropTypes.string.isRequired,
  fareRule: PropTypes.string.isRequired,
  stopDetaildepartureAirport: PropTypes.string.isRequired,
  stopDetaildepartureCity: PropTypes.string.isRequired,
  stopDetaildepartureDate: PropTypes.string.isRequired,
  stopDetaildepartureTime: PropTypes.string.isRequired,
  stopDetaildestinationAirport: PropTypes.string.isRequired,
  stopDetaildestinationCity: PropTypes.string.isRequired,
  stopDetaildestinationDate: PropTypes.string.isRequired,
  stopDetaildestinationTime: PropTypes.string.isRequired
};
TicketDetails.defaultProps = {
  airlineName: "American Airlines",
  airPlaneName: "Airbus: A125 -510",
  startDate: "06 Jun, Mon, 2018",
  outbound: "Outbound",
  flightnum: "Flight: AN 78",
  departureAirport: "IKA",
  departureCity: "Tehran",
  destinationAirport: "DCC",
  destinationCity: "Paris",
  stopDetails: "1 Stop for 1H & 20MDubai (DXB)",
  departureTime: "00:10",
  destinationTime: "11:11",
  duration: "6h 10m",
  Refferance: "Refferance",
  pnr: "BF5457G",
  ticket: "e-Ticket",
  ticketNum: "05 214532",
  destinationAirPlane: "Airbus: A125 -510",
  cost: "7164700",
  toman: "تومان",
  acceptText: "تایید و ادامه",
  cabin: "Cabin",
  cabinInfo: "7KG",
  checked: "Checked",
  checkedInfo: "75KG",
  fareRule:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo mauris, tincidunt et justo sed, rhoncus ultricies risus. Integer ac sem risus. Sed mollis venenatis sagittis. Aliquam in leo sollicitudin nunc iaculis bibendum. Etiam vel justo congue, auctor tortor vitae, sollicitudin massa. Praesent finibus, diam quis tempus dapibus, neque erat porta velit, et tempus mi libero sed tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque vulputate ut nisi vel semper. Phasellus convallis sem egestas purus mattis finibus. Integer pulvinar nisl lectus, et sollicitudin massa aliquet sed. Proin turpis magna, pretium eget ullamcorper id, accumsan et orci. Donec tincidunt augue a nulla blandit, sed eleifend lorem fringilla. Morbi sodales ex eget libero placerat, vitae maximus dolor gravidamAliquam a leo et diam euismod sagittis sit amet at sem. Donec placerat sodales cursus. Maecenas pharetra urna est, eget elementum turpis aliquam ut. Fusce feugiat malesuada purus, a consequat dui dapibus sit amet. Donec felis erat, scelerisque a nulla a, viverra ornare risus. Pellentesque congue purus non mi finibus, at aliquam risus lacinia. Vivamus ultrices, mauris quis fermentum sagittis, odio odio ultricies velit, non vestibulum dolor diam blandit nunc. Suspendisse aliquam laoreet ante eu vestibulum. Cras eu tincidunt dui, et finibus magna. Vestibulum efficitur fermentum diam, ac fringilla mi facilisis vel. Nam suscipit ullamcorper nibh, vitae consequat risus semper id. Nam quis risus et nisl convallis pharetra. Nulla euismod ipsum non nibh cursus tempus. Quisque id lorem tincidunt massa blandit convallis eget eget lorem. Donec bibendum dolor id felis condimentum porttitor. Suspendisse tristique lectus id interdum ultricies.Nullam varius nulla non sapien sagittis vulputate. Mauris ac pulvinar justo, eu laoreet sem. Ut non auctor leo. Donec sollicitudin ex quis lorem pharetra tincidunt. Pellentesque tellus massa, dapibus nec placerat nec, hendrerit eget augue. Mauris rhoncus scelerisque sem, vel lacinia orci malesuada ultricies. Duis placerat tellus sit amet interdum laoreet. Sed nulla nulla, fermentum vel suscipit eget, laoreet non lectus. In nunc lectus, feugiat sed cursus pretium, interdum ut arcu. Aliquam rutrum sollicitudin malesuada. Etiam hendrerit lobortis ex nec mollis. Etiam aliquet enim non magna faucibus gravida. Donec maximus lobortis maximus. Praesent arcu nibh, placerat eu dapibus ac, pellentesque at elit. Proin elementum aliquet dapibus. Nunc dignissim volutpat porta. Pellentesque gravida dui sed dolor ultricies, eu tempor tortor hendrerit. Phasellus nec blandit nibh, sit amet iaculis ante. Cras sit amet magna luctus, feugiat nisi ut, commodo purus.Etiam a nisi magna. Maecenas luctus sodales luctus. Vestibulum ultrices neque nec dictum feugiat. Nam sed lorem ut nibh vulputate maximus. Aenean urna nibh, tristique id mauris vitae, congue faucibus tortor. In in nisl facilisis, dapibus libero sed, vulputate libero. Duis ligula lacus, efficitur in ex id, egestas euismod turpis. Donec ullamcorper dolor ac aliquet fringilla. In id odio in metus scelerisque iaculis eget quis ex. Curabitur at elit fringilla, tempor nisl ut, facilisis magna. Praesent id luctus erat. Sed tincidunt tortor ac nisi egestas vehicula. In eu interdum risus. Nullam vestibulum felis vel vehicula consequat. Nullam et ex odio. ",
  stopDetaildepartureAirport: "IKA",
  stopDetaildepartureCity: "Tehran",
  stopDetaildepartureDate: "شنبه 12 فروردین",
  stopDetaildepartureTime: "ساعت 00:00",
  stopDetaildestinationAirport: "IST",
  stopDetaildestinationCity: "Istanbul",
  stopDetaildestinationDate: "شنبه 12 فروردین",
  stopDetaildestinationTime: "ساعت 00:00"
};
