import {
    StyleSheet,
    View,
    FlatList,
    Image,
    ScrollView,
    AsyncStorage,
    Text
  } from "react-native";
  import React, { Component } from "react";
  import { createIconSetFromFontello } from "react-native-vector-icons";
  import fontelloConfig from "../config.json";
  const Icon = createIconSetFromFontello(fontelloConfig);
  import Appbar from "../components/AppbarTitle";
  import { RFPercentage, RFValue }  from "react-native-responsive-fontsize";
  import WatermarkIcon from '../components/WatermarkIcon'
import { TouchableOpacity } from "react-native-gesture-handler";
export default class PurchaseDetails extends Component {
    constructor(props) {
      super(props);
      this.state = {
          selectedItem : 0, // 0= flight , 1 = price , 2 = passengers , 3 = rule

      }
    }
    navigateToBack() {
        this.props.navigation.navigate("FlightOptions");
      }
      renderComponents(){
          if(this.state.selectedItem == 0){
              return(
                  <View>
                      <Text>afd</Text>
                  </View>
              )
          }
      }
    render(){
        return(
            <View style={styles.container}>
            <Appbar
              pageTitle="جزییات خرید"
              backIconColor="#FFFFFF"
              titleTextColor="#FFFFFF"
              navigateTo={this.navigateToBack.bind(this)}
            />
            <View style={styles.titleBar}>
                <TouchableOpacity onPress={()=>this.setState({selectedItem : 0})} activeOpacity={0.8}>
                    <Text style={[styles.titleText , {color : this.state.selectedItem == 0 ? '#FFFFFF' : '#8A8795'}]}>
                    جزییات پرواز
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.setState({selectedItem : 1})} activeOpacity={0.8}>
                    <Text style={[styles.titleText , {color : this.state.selectedItem == 1 ? '#FFFFFF' : '#8A8795'}]}>
                    جزییات قیمت
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.setState({selectedItem : 2})} activeOpacity={0.8}>
                    <Text style={[styles.titleText , {color : this.state.selectedItem == 2? '#FFFFFF' : '#8A8795'}]}>
                    همراهان من
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.setState({selectedItem : 3})} activeOpacity={0.8}>
                    <Text style={[styles.titleText , {color : this.state.selectedItem == 3 ? '#FFFFFF' : '#8A8795'}]}>
                    قوانین
                    </Text>
                </TouchableOpacity>
            </View>
            {this.renderComponents()}
            {/* <FlatList
              style={{ flex: 1, height: 100 }}
              data={this.state.data}
              renderItem={this._renderItem}
            /> */}
           <WatermarkIcon zIndexWatermark= {-1} />
    
          </View>
    

        )
    }
}
const styles = StyleSheet.create({
    container: {
      backgroundColor: "#1B275A",
      flex: 1
    },
    titleBar:{
        flexDirection:'row-reverse',
        justifyContent : 'space-around',
        borderTopWidth : 1,
        borderBottomWidth:1,
        borderColor : '#8A8795',
        padding : 5
    },
    titleText:{
        color : '#8A8795',
        fontFamily:'Sahel-Bold',
        fontSize:RFPercentage(1.9)
    }
});  
