import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  StatusBar,
  FlatList,
  Dimensions
} from "react-native";

import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import BtnGradient from "../components/BtnGradient";
import WatermarkIcon from "../components/WatermarkIconBlack";
import Logo from "../Images/pngLogo.png";
import {
  IS_WALKTHROUGH,
  IS_LOGIN_POSTPONED,
  USER_ID
} from "../constants/UserParams";
import activeSamPay from "../Images/activeSamPay.png";
import inactiveSamPay from "../Images/inactiveSamPay.png";
import activeCIP from "../Images/activeCIP.png";
import inactiveCIP from "../Images/inactiveCIP.png";
import activeGift from "../Images/activeGift.png";
import inactiveGift from "../Images/inactiveGift.png";
let deviceWidth = Dimensions.get("window").width;

export default class WalkThrough extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      number: 0,
      firstLaunch: null,
      data: [
        {
          activeIcon: activeCIP,
          inactiveIcon: inactiveCIP,
          id: "cip"
        },
        {
          activeIcon: activeSamPay,
          inactiveIcon: inactiveSamPay,
          id: "sampay"
        },
        {
          activeIcon: activeGift,
          inactiveIcon: inactiveGift,
          id: "gift"
        }
      ]
    };
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem(IS_WALKTHROUGH, "true");
      const isLoginPostponed = await AsyncStorage.getItem(IS_LOGIN_POSTPONED);
      const userId = await AsyncStorage.getItem(USER_ID);
      if (isLoginPostponed == null && userId == null) {
        this.props.navigation.navigate("Login", {
          fromAvailable: "false"
          // prices: this.state.prices,
          // alarm: this.state.alarm,
          // total_Title: this.state.total_Title,
          // total: this.state.total
        });
      } else {
        this.props.navigation.navigate("FlightOptions");
        //   this.props.navigation.dispatch({
        //     type: 'Reset',
        //     index: 0,
        //    actions: [{ type: 'Navigate', routeName: 'Drawer' }]
        //  })
      }
    } catch (error) {
      // Error saving data
    }
  };

  render() {
    const array = [
      "در سام‌سیر، با هر خرید، می‌توانید هدیه ویژه سام‌سیر را برای خود انتخاب کنید",
      " در سام سیر، با هر خرید، بخشی از مبلغ، به حساب سام سیر شما باز خواهد گشت و در خریدهای بعدی، می توانید از آن مبلغ استفاده کنید.",
      "در سام‌سیر، هر تعداد خرید بیشتر داشته باشید، تخفیفات و جوایز بیشتری دریافت خواهید نمود",
      "پشتیبانی ۲۴ ساعت در ۷ روز هفته در سام سیر، ضامن آرامش شما پیش از یک سفر دلچسب خواهد بود. "
    ];
    let { number } = this.state;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#fff" />
        <View style={styles.logoContainer}>
          <Image source={Logo} style={styles.logo} />
          <Icon
            name="typography"
            style={{ fontSize: RFPercentage(5), marginTop: 10 }}
            color="#194A99"
          />
        </View>
        <View style={{ flex: 1 }}>
          <Text style={styles.infoText} numberOfLines={4}>
            {array[number]}
          </Text>
          {number == 0 ? (
            // <View style={styles.boxContainer}>
            //   <View style={styles.boxes}>
            //     <Text style={[styles.textBox, { fontSize: RFPercentage(2) }]}>FREE</Text>
            //     <Text style={[styles.textBox, { fontSize: RFPercentage(3.5) }]}>CIP</Text>
            //     <Text style={[styles.textBox, { fontSize: RFPercentage(2) }]}>
            //       SERVICES
            //     </Text>
            //   </View>
            //   <View style={styles.boxes}>
            //     <Text style={[styles.textBox, { fontSize: RFPercentage(2.2) }]}>
            //       MONEY
            //     </Text>
            //     <Text style={[styles.textBox, { fontSize: RFPercentage(2.2) }]}>
            //       REFUND
            //     </Text>
            //   </View>
            //   <View style={styles.boxes}>
            //     <Text style={[styles.textBox, { fontSize: RFPercentage(2) }]}>
            //       SPECIAL
            //     </Text>
            //     <Text style={[styles.textBox, { fontSize: RFPercentage(2) }]}>
            //       DISCOUNT
            //     </Text>
            //   </View>
            // </View>
            <FlatList
              horizontal
              contentContainerStyle={{
                justifyContent: "space-around",
                paddingHorizontal: "8%",
                flex: 1
              }}
              data={this.state.data}
              renderItem={({ item }) => (
                <View style={{ padding: 15 }}>
                  <TouchableOpacity
                    style={styles.optionWrapper}
                    // onPress={() => this.sampayTypeSelected(item.id)}
                    activeOpacity={0.9}
                  >
                    <Image
                      source={
                        this.state.itemSampayPressed === item.id
                          ? item.activeIcon
                          : item.activeIcon
                      }
                    />
                  </TouchableOpacity>
                </View>
              )}
            />
          ) : null}
        </View>
        <View style={styles.infoContainer}>
          <TouchableOpacity
            style={{}}
            onPress={
              number != 3
                ? () => this.setState({ number: (number += 1) })
                : () => this._storeData()
            }
          >
            <View style={styles.linearGradient}>
              <BtnGradient type="باشه" />
            </View>
          </TouchableOpacity>
        </View>

        <View style={{ marginBottom: "8%" }}>
          <WatermarkIcon />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between"
  },
  logoContainer: {
    marginTop: 50,
    alignItems: "center",
    flex: 1
  },
  linearGradient: {
    width: 120,
    height: "50%",
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  boxContainer: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  boxes: {
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#91A9CD",
    width: 90,
    height: 90,
    justifyContent: "center"
  },
  textBox: {
    fontFamily: "Montserrat-Bold",
    color: "#B5C9E8"
  },
  logo: {
    // width: "20%",
    // height: "23%"
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  okBotton: {
    color: "#fff",
    textAlign: "center",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2.3)
  },
  infoText: {
    color: "#15479F",
    fontSize: RFPercentage(2.6),
    textAlign: "center",
    fontFamily: "Sahel-Bold",
    marginRight: 20,
    marginLeft: 20
  },
  infoContainer: {
    alignItems: "center",
    flex: 1
  },
  group: {
    opacity: 0.35,
    marginBottom: 40
  },
  optionWrapper: {
    backgroundColor: "#FFFFFF",
    borderColor: "#15479F",
    borderWidth: 1,
    // margin: 10,
    width: 0.22 * deviceWidth,
    height: 0.22 * deviceWidth,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 6
  }
});
