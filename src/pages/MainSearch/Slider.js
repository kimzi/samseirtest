import React from "react";
import { StyleSheet, View, Text, Platform } from "react-native";
import CustomMarker from "./CustomMarker";
// import MultiSlider from "@ptomasroos/react-native-multi-slider";
import LinearGradient from "react-native-linear-gradient";
// import Image from "../../Images/activeHotel.png";
export default class Slider extends React.Component {
  state = {
    multiSliderValue: [3, 7]
  };

  multiSliderValuesChange = values => {
    this.setState({
      multiSliderValue: values
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.sliders}>
          <View style={styles.sliderOne} />
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={["#F63323", "#15479F"]}
            style={styles.linearGradient}
          >
            <MultiSlider
              values={[
                this.state.multiSliderValue[0],
                this.state.multiSliderValue[1]
              ]}
              sliderLength={280}
              style={{ width: 10 }}
              onValuesChange={this.multiSliderValuesChange}
              min={0}
              max={100}
              allowOverlap
              snapped
              customMarker={CustomMarker}
              trackStyle={{
                height: 5,
                backgroundColor: "#DAE4F4",
                justifyContent: "center",
                alignItems: "center"
              }}
              containerStyle={{
                backgroundColor: "#fff"
              }}
              touchDimensions={{
                height: 1,
                width: 5,
                borderRadius: 50
              }}
            />
          </LinearGradient>
        </View>
      </View>
    );
  }
}
var styles = StyleSheet.create({
  sliderOne: {}
});
