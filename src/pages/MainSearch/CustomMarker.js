import React from "react";
import { StyleSheet, Image, View } from "react-native";

class CustomMarker extends React.Component {
  render() {
    return (
      <View style={styles.firstContainer}>
        <View style={styles.secondContainer}>
          <View style={styles.thirdContainer} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  firstContainer: {
    height: 30,
    width: 30,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    elevation: 2,
    shadowColor : '#000',
    shadowOpacity : 0.3, 
    shadowRadius : 1,
    shadowOffset :  {width: 0,height: 0},

  },
  secondContainer: {
    height: 25,
    width: 25,
    backgroundColor: "#B5C9E8",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8
  },
  thirdContainer: {
    height: 15,
    width: 15,
    backgroundColor: "#15479F",
    borderRadius: 20
  }
});

export default CustomMarker;
