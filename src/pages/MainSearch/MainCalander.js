// import React, { Component } from "react";
// import { Text, View, StyleSheet, TouchableOpacity } from "react-native";
// import { createIconSetFromFontello } from "react-native-vector-icons";
// import fontelloConfig from "../../config.json";
// const Icon = createIconSetFromFontello(fontelloConfig);
// import { Calendar } from "react-native-calendars";
// // import { CalendarPersian } from "react-native-calendars-persian";

// import _ from "lodash";
// import { LocaleConfig } from "react-native-calendars";
// import RF from "react-native-responsive-fontsize";
// import WaterMark from "../../components/WatermarkIconBlack";
// import BtnGradientCheckmark from "../../components/BtnGradientCheckmark";
// import Cregorian from "./Cregorian";
// import FlightOptions from "../../utilities/navigations/FlightOptions";
// var moment = require("moment-jalaali");
// moment().format("jYYYY-jM-jD");

// // LocaleConfig.locales["fr"] = {
// //   monthNames: [
// //     "January",
// //     "February ",
// //     "March ",
// //     "April ",
// //     "May ",
// //     "June ",
// //     "July ",
// //     "August ",
// //     "September ",
// //     "October ",
// //     "November ",
// //     "December "
// //   ],
// //   monthNamesShort: [
// //     "Janv.",
// //     "Févr.",
// //     "Mars",
// //     "Avril",
// //     "Mai",
// //     "Juin",
// //     "Juil.",
// //     "Août",
// //     "Sept.",
// //     "Oct.",
// //     "Nov.",
// //     "Déc."
// //   ],
// //   dayNames: [
// //     "Sunday",
// //     "Monday	",
// //     "Tuesday",
// //     "Wednesday	",
// //     "Thursday",
// //     "Friday",
// //     "Saturday"
// //   ],
// //   dayNamesShort: ["S", "M", "T", "W", "T", "F", "S"]
// // };

// // LocaleConfig.defaultLocale = "fr";
// // let weekDay = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
// // let jWeekDay = [
// //   "یک‌شنبه",
// //   "دوشنبه",
// //   "سه‌شنبه",
// //   "چهارشنبه",
// //   "پنج‌شنبه",
// //   "جمعه",
// //   "شنبه"
// // ];
// // let monthName = [
// //   "Jan",
// //   "Feb",
// //   "Mar",
// //   "Apr",
// //   "May",
// //   "Jun",
// //   "Jul",
// //   "Aug",
// //   "Sept",
// //   "Oct",
// //   "Nov",
// //   "Des"
// // ];
// // let jMonthName = [
// //   "فروردین",
// //   "اردیبهشت",
// //   "خرداد",
// //   "تیر",
// //   "مرداد",
// //   "شهریور",
// //   "مهر",
// //   "آبان",
// //   "آذر",
// //   "دی",
// //   "بهمن",
// //   "اسفند"
// // ];

// export default class CalendarComponent extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       startDate: null,
//       finishDate: null,
//       render: false,
//       marks: {},
//       markDay: {},
//       calendarSelection: false,
//       gregorianStart: null,
//       jamaliStart: null,
//       gregorianFinish: null,
//       jamaliFinish: null,
//       monthRender: 0,
//       JamaliOneDaySelected: null,
//       gregorianOneDaySelected: null,
//       oneDateSelected: null,
//       calanderCode : 1
//     };
//   }

//   async oneDaySelected(date) {
//     const Splited = date.split("-");
//     year = Splited[0];
//     month = Splited[1];
//     Day = Splited[2];

//     let d = new Date(date);
//     m = moment(date, "YYYY-M-D");
//     // this.setState({
//     //   JamaliOneDaySelected: `(${jWeekDay[d.getDay()]}, ${
//     //     m.format("jYYYY-jM-jD").split("-")[2]
//     //   } ${jMonthName[m.format("jYYYY-jM-jD").split("-")[1] - 1]})`,
//     //   gregorianOneDaySelected: `${weekDay[d.getDay()]}, ${+Day} ${
//     //     monthName[+month - 1]
//     //   }`,
//     //   oneDateSelected: date
//     // });
//     let mark = {};
//     mark[date] = {
//       selected: true,
//       marked: true,
//       color: "#15479F",
//       startingDay: true,
//       endingDay: true
//     };
//     this.setState({ markDay: mark });
//     await this.setState({ render: 0 });
//   }

//   onPress(sourceCode, receivedValue) {
//     if (sourceCode == 0) {
//       this.state.startDate && this.state.finishDate != null
//         ? // this.props.navigation.navigate(
//           //     "FlightOptions",
//           //     {
//           //       startDay: this.state.startDate,
//           //       finishDate: this.state.finishDate,
//           //       jStartDate: this.state.jamaliStart,
//           //       gStartDate: this.state.gregorianStart,
//           //       jFinishDate: this.state.jamaliFinish,
//           //       gFinishDate: this.state.gregorianFinish
//           //     },
//           //     console.log(this.startDate)
//           //   )
//           (receivedValue(this.state.jamaliStart, this.state.jamaliFinish),
//           this.props.navigation.goBack())
//         : alert("select your start and finish date");
//     } else if (sourceCode == 1) {
//       this.state.oneDateSelected != null
//         ? //  this.props.navigation.navigate(
//           //     "FlightOptions",
//           //     {
//           //       JamaliOneDaySelected: this.state.JamaliOneDaySelected,
//           //       gregorianOneDaySelected: this.state.gregorianOneDaySelected,
//           //       oneDateSelected: this.state.oneDateSelected,
//           //       dateWasSelected: 1
//           //     },
//           //     console.log(this.state.JamaliOneDaySelected)
//           //   )
//           (receivedValue(this.state.JamaliOneDaySelected),
//           this.props.navigation.goBack())
//         : alert("select your date");
//     }
//   }
//   render(){
//     return(
//       <View style={styles.container}>
//       <View>
//         <Icon
//           name="back"
//           color="#000000"
//           style={styles.backIcon}
//           onPress={() => this.props.navigation.navigate("FlightOptions")}
//         />
//       </View>
//       <View style={{ justifyContent: "flex-start" }}>
//         <View>
//           <View style={styles.headerTextContainer}>
//             <Text style={styles.headerText}>
//               روزهای رفت و برگشت خود را انتخاب کنید
//             </Text>
//           </View>
//           <View style={{ justifyContent: "center", alignItems: "center" }}>
//             <TouchableOpacity
//               onPress={() =>this.setState({calanderCode : 0})
//               }
//               style={{
//                 marginTop: 10,
//                 justifyContent: "center",
//                 width: "25%",
//                 borderWidth: 1,
//                 borderColor: "#F09819",
//                 borderRadius: 20
//               }}
//             >
//               <View style={{}}>
//                 <Text
//                   style={{
//                     textAlign: "center",
//                     alignSelf: "center",
//                     color: "#F09819",
//                     fontSize: RF(1.9),
//                     fontFamily: "Sahel"
//                   }}
//                 >
//                   {/* تقویم شمسی */}
//                   {/* {this.state.calanderValue} */}
//                   {this.state.calanderCode ? 'تقویم شمسی' : 'تقویم میلادی'}
//                 </Text>
//               </View>
//             </TouchableOpacity>
//           </View>
//           <View style={styles.calanderContainer}>
//           {this.state.calanderCode ? 
//           (                <Calendar
//             monthFormat={" MMMM yyyy"}
//             onMonthChange={month => {
//               console.log("month changed", month);
//             }}
//             hideArrows={false}
//             hideExtraDays={false}
//             disableMonthChange={true}
//             startDate={1}
//             hideDayNames={false}
//             hideArrows={false}
//             renderArrow={direction => <Icon name={direction} size={20} />}
//             showWeekNumbers={false}
//             onPressArrowLeft={substractMonth => substractMonth()}
//             onPressArrowRight={addMonth => addMonth()}
//             onDayPress={day => this.oneDaySelected(day.dateString)}
//             markedDates={this.state.markDay}
//             markingType={"period"}
//             theme={{
//               textSectionTitleColor: "#7A7F85",
//               selectedDayBackgroundColor: "#00adf5",
//               selectedDayTextColor: "#ffffff",
//               todayTextColor: "#B6BEC6",
//               dayTextColor: "#B6BEC6",
//               textDisabledColor: "#B6BEC6",
//               dotColor: "red",
//               selectedDotColor: "#ffffff",
//               arrowColor: "orange",
//               monthTextColor: "#15479F",
//               textDayFontFamily: "Montserrat-Regular",
//               textMonthFontFamily: "Montserrat-Regular",
//               textDayHeaderFontFamily: "Montserrat-Regular",
//               textDayFontSize: RF(2.2),
//               textMonthFontSize: RF(2.9),
//               textDayHeaderFontSize: RF(2.2)
//             }}
//           />)
//           : null
//           // (            
//           // <CalendarPersian
//           //   jalali={true}
//           //   monthFormat={" MMMM yyyy"}
//           //   onMonthChange={month => {
//           //     console.log("month changed", month);
//           //   }}
//           //   hideArrows={false}
//           //   hideExtraDays={false}
//           //   disableMonthChange={true}
//           //   startDate={1}
//           //   hideDayNames={false}
//           //   hideArrows={false}
//           //   renderArrow={direction => <Icon name={direction} size={20} />}
//           //   showWeekNumbers={false}
//           //   onPressArrowLeft={substractMonth => substractMonth()}
//           //   onPressArrowRight={addMonth => addMonth()}
//           //   onDayPress={day => this.oneDaySelected(day.dateString)}
//           //   markedDates={this.state.markDay}
//           //   markingType={"period"}
//           //   theme={{
//           //     "stylesheet.calendar.header": {
//           //       week: {
//           //         flexDirection: "row-reverse",
//           //         justifyContent: "space-around"
//           //       }
//           //     },
//           //     "stylesheet.calendar.main": {
//           //       week: {
//           //         flexDirection: "row-reverse",
//           //         justifyContent: "space-between",
//           //         marginTop: 5
//           //       }
//           //     },
//           //     textSectionTitleColor: "#7A7F85",
//           //     selectedDayBackgroundColor: "#00adf5",
//           //     selectedDayTextColor: "#ffffff",
//           //     todayTextColor: "#B6BEC6",
//           //     dayTextColor: "#B6BEC6",
//           //     textDisabledColor: "#B6BEC6",
//           //     dotColor: "red",
//           //     selectedDotColor: "#ffffff",
//           //     arrowColor: "orange",
//           //     monthTextColor: "#15479F",
//           //     textDayFontFamily: "Montserrat-Regular",
//           //     textMonthFontFamily: "Montserrat-Regular",
//           //     textDayHeaderFontFamily: "Montserrat-Regular",
//           //     textDayFontSize: RF(3),
//           //     textMonthFontSize: RF(2.9),
//           //     textDayHeaderFontSize: RF(2.2)
//           //   }}
//           // />
// }

// </View>
// <View
//     style={{
//       flexDirection: "row",
//       alignItems: "center",
//       justifyContent: "center"
//     }}
//   >
//     <View
//       style={{
//         flexDirection: "column",
//         justifyContent: "center",
//         alignItems: "center"
//       }}
//     >
//       <Text style={[styles.dateText]}>
//         {this.state.gregorianOneDaySelected ? "تاریخ" : null}{" "}
//       </Text>
//       <Text style={styles.EnDetails}>
//         {this.state.gregorianOneDaySelected}
//       </Text>
//       <Text style={styles.Pdetails}>
//         {this.state.JamaliOneDaySelected}
//       </Text>
//     </View>
//   </View>
// </View>
// <WaterMark />
// <TouchableOpacity
// style={{
//   width: 150,
//   height: "6%",
//   position: "absolute",
//   bottom: 20,
//   alignSelf: "center"
// }}
// onPress={() => this.onPress(flightOptionCode, receivedValue)}
// >
// <BtnGradientCheckmark />
// </TouchableOpacity>
// </View>
// </View>

//     );
//   };
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1
//   },
//   backIcon: {
//     padding: 16,
//     fontSize: RF(2.2)
//   },
//   headerText: {
//     color: "#15479F",
//     fontFamily: "Sahel",
//     fontSize: RF(2.2)
//   },
//   headerTextContainer: {
//     alignItems: "center"
//   },
//   calanderContainer: {
//     marginLeft: 20,
//     marginRight: 20,
//     height: "65%"
//   },
//   dateText: {
//     fontFamily: "Sahel",
//     fontSize: 15,
//     color: "#15479F"
//   },
//   EnDetails: {
//     fontFamily: "Montserrat-Medium",
//     fontSize: RF(2.1),
//     color: "#2C3039"
//   },
//   Pdetails: {
//     fontFamily: "Sahel",
//     fontSize: RF(2.2),
//     color: "#2C3039"
//   }
// });
