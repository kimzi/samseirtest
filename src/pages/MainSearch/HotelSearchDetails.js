import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Button,
  Image,
  Dimensions,
  FlatList
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import OneWay from "./OneWay";
const Icon = createIconSetFromFontello(fontelloConfig);
import hotelPic from "../../Images/HotelPic.jpg";
import AirlinePic from "../../Images/TS.png";
import SearchSetting from "./SearchSetting";
let deviceWidth = Dimensions.get("window").width;

export default class SearchResult extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      selectedMenu: 1,
      star: []
    };
  }
  componentWillMount() {
    let i = 0;
    console.log("enter star function");
    let array = [];
    for (i; i < 5; i++) {
      array.push(<Icon name="star" color="#F09819" size={20} />);
    }
    this.setState({ star: array });
  }
  render() {
    return (
      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0.5 }}
        colors={["#173C89", "#163F8F"]}
        style={styles.linearGradient}
      >
        <View style={styles.buttonContainer}>
          <View style={{ backgroundColor: "#fff", flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: "#fff" }}>
              <Image
                style={{ width: deviceWidth, height: deviceWidth * 0.58 }}
                source={hotelPic}
              />
              <View
                style={{
                  position: "absolute",
                  top: 10,
                  right: 30,
                  flexDirection: "row-reverse"
                }}
              >
                <Icon
                  name="share"
                  color="#fff"
                  size={25}
                  style={{ marginLeft: 10 }}
                />
                <Icon name="bookmark" color="#fff" size={25} />
              </View>
              <View
                style={{
                  width: deviceWidth,
                  height: 60,
                  backgroundColor: "#fff",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    color: "#1C285A",
                    fontFamily: "Montserrat-Bold",
                    fontSize: 15
                  }}
                >
                  Maison Albar Hotel Opera Diamond
                </Text>
                <View style={{ flex: 1 }}>
                  <FlatList
                    contentContainerStyle={{ margin: 4 }}
                    horizontal={true}
                    data={this.state.star}
                    renderItem={({ item }) => (
                      <Icon name="star" color="#F09819" size={20} />
                    )}
                  />
                </View>
              </View>
              <View
                style={{
                  width: deviceWidth,
                  height: 30,
                  backgroundColor: "#fff",
                  flexDirection: "row-reverse"
                }}
              >
                <Text
                  style={{
                    fontFamily: "Montserrat-Medium",
                    fontSize: 14,
                    color: "#1C285A",
                    marginLeft: 10
                  }}
                >
                  +8173306
                </Text>
                <Icon name="phone" color="#15479F" size={20} />
              </View>
            </View>
          </View>
        </View>
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({
  linearGradient: {
    flex: 1
  },
  backIcon: {
    marginTop: 15,
    marginLeft: 15
  },
  searchSettingStyle: {
    borderWidth: 1,
    borderColor: "#FFBA00",
    width: 85,
    height: 30,
    justifyContent: "center",
    borderRadius: 20
  },
  settingText: {
    color: "#FFBA00",
    textAlign: "center",
    marginBottom: 2,
    fontFamily: "Sahel",
    fontSize: 11
  },
  resultCounterText: {
    fontFamily: "Sahel",
    color: "#fff",
    fontSize: 11
  },
  topOptionContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: "flex-end"
  },
  menuOptionText: {
    color: "#fff",
    fontFamily: "Sahel",
    textAlign: "center",
    marginBottom: 5
  },
  topMenuContainer: {
    marginTop: 10,
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    backgroundColor: "#0D3477",
    alignItems: "center"
  },
  topCircle: {
    width: 30,
    height: 30,
    borderRadius: 100,
    backgroundColor: "#163F8F",
    marginTop: -15,
    marginLeft: -15
  },
  bottomCircle: {
    width: 30,
    height: 30,
    borderRadius: 100,
    backgroundColor: "#163F8F",
    marginTop: 0,
    marginLeft: -15
  },
  invertedOpacity: {
    flexDirection: "column",
    justifyContent: "space-between"
  },
  ticket: {
    backgroundColor: "#fff",
    height: 110,
    width: 380,
    marginTop: 20,
    paddingRight: 10,
    flexDirection: "row",
    justifyContent: "flex-end",
    borderRadius: 10
  },
  ticketContainer: {
    alignItems: "center"
  },
  price: {
    fontSize: 17,
    fontFamily: "Sahel-Bold",
    color: "#F09819"
  },
  numberOfPeople: {
    fontSize: 13,
    fontFamily: "Sahel",
    color: "#F09819",
    marginTop: -10
  },
  destinationCountry: {
    fontSize: 13,
    fontFamily: "Montserrat-Bold",
    color: "#707070",
    marginTop: 8
  },
  time: {
    color: "#313841",
    fontFamily: "Lato-Black",
    fontSize: 25
  },
  city: {
    fontFamily: "Montserrat-Medium"
  },
  HotelName: {
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    color: "#1C285A"
  }
});
