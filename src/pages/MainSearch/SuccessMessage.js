import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Text,
  Animated,
  Easing,
  Dimensions,
  StatusBar,
  ToastAndroid,
  KeyboardAvoidingView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  PixelRatio,
  ScrollView
  
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import LinearGradient from "react-native-linear-gradient";
import AppbarTitle from "../components/AppbarTitle";
import pngLogo from '../../Images/pngLogo.png';
import success from '../../Images/success.png'

export default class LoginTest extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
    };
  }
navigateToBack() {
  this.props.navigation.navigate("FlightOptions");
}

  render() {
    return (
      <LinearGradient
        colors={["#15479F", "#18377D", "#1B275A"]}
        style={styles.container}
      >
        {/* <View style={styles.headerContainer}>
          <TouchableOpacity>
            <Icon name="back" style={styles.backIcon} />
          </TouchableOpacity>
        </View> */}
                 <AppbarTitle
          pageTitle="پرداخت"
          backIconColor="white"
          titleTextColor="white"
          navigateTo={this.navigateToBack.bind(this)}
        />
        </LinearGradient>
    )}
    }