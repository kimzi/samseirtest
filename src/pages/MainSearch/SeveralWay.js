import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  PixelRatio,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
  FlatList
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import { Dimensions } from "react-native";
import AirLineSearchResult from "./AirLineSearchResult";
// importing stayles from Hotel.js file:
import Hotel, { styles } from "./Hotel";
import BtnSearch from "../../components/BtnSearch";
import TravelFamilyContainer from "../../components/TravelFamilyContainer";
import WatermarkIcon from "../../components/WatermarkIcon";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import FlightOptionsContainer from "../../components/FlightOptionsContainer";
import LightCalanderContainer from "../../components/LightCalanderContainer";
import Indicator from "../Indicator";
const axios = require("axios");
const Icon = createIconSetFromFontello(fontelloConfig);

export default class TwoWay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      array: [],
      number: 9,
      itemFontColor: "#333",
      itemPressed: 0,
      airports: ""
    };
  }


  render() {
    return (
      <View style={prestyles.container}>
        <Text style={{color : '#fff' , fontFamily:'Sahel' , fontSize : RFPercentage(3)}}>Under Construction !!!</Text>
      </View>
    );
  }
}

export const prestyles = StyleSheet.create({
  container:{
    justifyContent : 'center',
    alignItems : 'center',
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#1B275A"

  },
  destinationDepartureContainer: {
    height: PixelRatio.get() >= 3 ? "23%" : "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10
  },
  destination: {
    backgroundColor: "#E9EEF6",
    borderRadius: 10,
    flexDirection: "column",
    width: "41%",
    height: "100%"
  },
  textContainer: {
    alignItems: "center",
    flexDirection: "column"
  },
  airportName: {
    color: "#15479F",
    fontFamily: "Montserrat-Bold",
    fontSize: RFPercentage(5.5),
    justifyContent: "center",
    textAlign: "center"
  },
  cityAirport: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.8),
    color: "#9EADC5"
  },
  cityDetails: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.6),
    color: "#9EADC5"
  },
  BtnSearch: {
    width: "85%",
    height: PixelRatio.get() >= 3 ? "7%" : "9%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    marginTop: 20
  },
  countryText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(2.4),
    marginVertical: 5,
    color: "black"
  }
});
