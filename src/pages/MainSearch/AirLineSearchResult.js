import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  StatusBar,
  Button,
  Image,
  Modal,
  ActivityIndicator,
  Alert,
  AsyncStorage
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import OneWay from "./OneWay";
import axios from "axios";
import { baseUrl } from "../../constants/BaseURL";
const Icon = createIconSetFromFontello(fontelloConfig);
import FilterSearchResultOptionsContainer from "../../components/FilterSearchResultOptionsContainer";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import ListItemTicketContainer from "../../components/ListItemTicketContainer.js";
import TwoWayListItemTicketContainer from "../../components/TwoWayListItemTicketContainer";
import HandleBack from "../../utilities/HandleBack";
import AppbarTitle from "../../components/AppbarTitle";
import TicketDetails from "../TicketDetails";
import rectangle from "../../Images/blueRectangle.png";
import Indicator from "../Indicator";
import { REQUEST_ID } from "../../constants/UserParams";
export default class AirLineSearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      reqId: "",
      sourceCode: null
    };
  }

  async componentWillMount() {
    const { navigation } = this.props;
    const data = navigation.getParam("data", "data");
    const reqId = navigation.getParam("reqId", "reqId");
    const sourceCode = navigation.getParam("sourceCode", "sourceCode");
    this.setState({
      data: data,
      reqId: reqId,
      sourceCode: sourceCode
    });
    await AsyncStorage.setItem(REQUEST_ID, reqId);
  }

  onBack = () => {
    this.props.navigation.navigate("FlightOptions");
    return true;
  };

  navigateToBack() {
    this.props.navigation.navigate("FlightOptions");
  }

  render() {
    const { navigation } = this.props;
    const sourceCode = navigation.getParam("sourceCode", "sourceCode");
    return (
      <HandleBack onBack={this.onBack}>
        <View style={styles.linearGradient}>
          <StatusBar backgroundColor="#1B275A" />

          <AppbarTitle
            pageTitle="نتایج پرواز"
            backIconColor="white"
            titleTextColor="white"
            // menuIcon="edits"
            navigateTo={this.navigateToBack.bind(this)}
            // showPassengerModal={this.showPassModal.bind(this)}
          />

          <View style={{ flex: 1 }}>
            {/* <View>
              <Icon
                name="back"
                style={styles.backIcon}
                onPress={() => this.props.navigation.navigate("FlightOptions")}
              />
            </View> */}

            <View style={styles.topOptionContainer}>
              {/* <TouchableOpacity
              onPress={() => this.props.navigation.navigate("SearchSetting")}
              style={styles.searchSettingStyle}
            >
              <Text style={styles.settingText}>{this.props.searchSetting}</Text>
            </TouchableOpacity> */}
              <Text style={styles.resultCounterText}>
                {this.state.data.length} نتیجه
              </Text>
            </View>

            {/* <FilterSearchResultOptionsContainer /> */}

            {/*To make a distance between tab and items  */}
            <View style={{ height: 0 }}>
              <Text />
            </View>

            {sourceCode == 1 ? (
              <ListItemTicketContainer
                type={this.state.data}
                reqId={this.state.reqId}
              />
            ) : (
              <TwoWayListItemTicketContainer
                type={this.state.data}
                reqId={this.state.reqId}
              />
            )}
          </View>
          {/* <Image
          source={rectangle}
          style={{
            position: "absolute",
            bottom: 0,
            zIndex: 1,
            // resizeMode: "cover"
            width: "100%"
          }}
        /> */}
        </View>
      </HandleBack>
    );
  }
}

AirLineSearchResult.propTypes = {
  searchSetting: PropTypes.string.isRequired,
  searchCounter: PropTypes.string.isRequired
};

AirLineSearchResult.defaultProps = {
  searchSetting: "تنظیمات جستجو",
  searchCounter: "25 نتیجه"
};

const styles = StyleSheet.create({
  linearGradient: {
    backgroundColor: "#1B275A",
    flex: 1
  },
  backIcon: {
    margin: 16,
    fontSize: RFPercentage(2.2),
    color: "#ffffff",
    width: 40,
    height: 30
  },
  searchSettingStyle: {
    borderWidth: 1,
    borderColor: "#FFBA00",
    width: 100,
    height: 30,
    padding: 4,
    justifyContent: "center",
    borderRadius: 20
  },
  settingText: {
    color: "#FFBA00",
    textAlign: "center",
    marginBottom: 2,
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.7)
  },
  resultCounterText: {
    fontFamily: "Sahel",
    color: "#fff",
    fontSize: RFPercentage(1.7)
  },
  topOptionContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    paddingLeft: "5%",
    marginRight: 15,
    alignItems: "flex-end"
  }
});
