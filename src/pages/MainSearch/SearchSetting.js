import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Button,
  Image,
  Platform
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import Hotel from "./Hotel";
import AirLineSearchResult from "./AirLineSearchResult";
const Icon = createIconSetFromFontello(fontelloConfig);
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import BtnGradient from '../../components/BtnGradient';
import Slider from "./Slider";
import { ScrollView } from "react-native-gesture-handler";
export default class SearchSetting extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      value: 20,
      selectedStops: 1,
      selectedClass: 1
    };
  }

  render() {
    return (
      <View style={{flex:1}}>
      <ScrollView style={{flex: 1}}>
        <Icon
          name="back"
          color="#000000"
          style={{margin : 16, fontSize:RFPercentage(2.2),    width: 40,
            height: 30,
        }}
          onPress={() => this.props.navigation.navigate("AirLineSearchResult")}
        />
        <Text style={styles.headerText}>تنظیمات جست و جو</Text>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0.5 }}
          colors={["#fff", "#E9EEF6"]}
          style={styles.linearGradient}
        >
          <View style={{ justifyContent: "center" }}>
            <Text style={styles.text}>توقف ها</Text>
          </View>
        </LinearGradient>
        <View style={styles.optionContainer}>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedStops == 4 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedStops: 4 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedStops == 4 ? "#fff" : "#15479F" }
              ]}
            >
              چند توقف
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedStops == 3 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedStops: 3 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedStops == 3 ? "#fff" : "#15479F" }
              ]}
            >
              یک توقف{" "}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedStops == 2 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedStops: 2 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedStops == 2 ? "#fff" : "#15479F" }
              ]}
            >
              بدون توقف{" "}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedStops == 1 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedStops: 1 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedStops == 1 ? "#fff" : "#15479F" }
              ]}
            >
              هر تعداد{" "}
            </Text>
          </TouchableOpacity>
        </View>
        {/* <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0.5 }}
          colors={["#fff", "#E9EEF6"]}
          style={styles.linearGradient}
        >
          <View style={{ justifyContent: "center" }}>
            <Text style={styles.text}>کلاس پرواز </Text>
          </View>
        </LinearGradient>
        <View style={styles.optionContainer}>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedClass == 4 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedClass: 4 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedClass == 4 ? "#fff" : "#15479F" }
              ]}
            >
              {" "}
              First Class
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedClass == 3 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedClass: 3 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedClass == 3 ? "#fff" : "#15479F" }
              ]}
            >
              Bussines
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedClass == 2 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedClass: 2 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedClass == 2 ? "#fff" : "#15479F" }
              ]}
            >
              Premium
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={20}
            style={[
              styles.options,
              {
                backgroundColor:
                  this.state.selectedClass == 1 ? "#15479F" : "#fff"
              }
            ]}
            onPress={() => this.setState({ selectedClass: 1 })}
          >
            <Text
              style={[
                styles.optionsText,
                { color: this.state.selectedClass == 1 ? "#fff" : "#15479F" }
              ]}
            >
              Economic
            </Text>
          </TouchableOpacity>
        </View> */}
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0.5 }}
          colors={["#fff", "#E9EEF6"]}
          style={styles.linearGradient}
        >
          <View style={{ justifyContent: "center" }}>
            <Text style={styles.text}>زمانبندی</Text>
          </View>
        </LinearGradient>
        <View style={styles.timerContainer}>
          <Icon
            name="departure"
            size={20}
            color="#B5C9E8"
            style={styles.timer}
          />
          <Text style={styles.departureText}>Tehran</Text>
          <Slider />
        </View>
        <View style={styles.timerContainer}>
          <Icon
            name="destination"
            size={20}
            color="#B5C9E8"
            style={styles.timer}
          />
          <Text style={styles.departureText}>Paris</Text>
          <Slider />
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0.5 }}
          colors={["#fff", "#E9EEF6"]}
          style={styles.linearGradient}
        >
          <View style={{ justifyContent: "center" }}>
            <Text style={styles.text}>قیمت</Text>
          </View>
        </LinearGradient>
        <View style={styles.timerContainer}>
          <Slider />
        </View>
        <LinearGradient
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0.5 }}
          colors={["#fff", "#E9EEF6"]}
          style={styles.linearGradient}
        >
          <View style={{ justifyContent: "center" }}>
            <Text style={styles.text}>ایرلاین</Text>
          </View>
        </LinearGradient>

        <View>
          <Icon name="group" size={150} color="#15479F" style={styles.group} />
        </View>
      </ScrollView>
      {/* <View style={{position : 'absolute' , bottom : '10%' , left : '35%' , width : '30%' , height : '7%' }}>
        <BtnGradient type="ورود" />
        </View> */}

      </View>
    );
  }
}
const styles = StyleSheet.create({
  linearGradient: {
    height: '5%'
  },
  text: {
    color: "#8195B7",
    fontSize: 17,
    fontFamily: "Sahel-Bold",
    writingDirection : 'rtl'
  },
  backIcon: {
    marginTop: 10,
    marginLeft: 10
  },
  headerText: {
    textAlign: "center",
    fontSize: 17,
    fontFamily: "Sahel-Bold",
    color: "#15479F",
    marginBottom: 10
  },
  options: {
    borderRadius: 20,
    width: '24%',
    height: '80%',
    marginTop: 10,
    borderWidth: 2,
    borderColor: "#15479F",
    justifyContent : 'flex-start'
  },
  optionsText: {
    color: "#15479F",
    textAlign: "center",
    fontFamily: "Sahel-Bold",
    marginBottom: 5
  },
  optionContainer: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 10,
    marginBottom : 15
  },
  timerContainer: {
    flexDirection: "row",
    marginTop: 20,
    justifyContent: "space-around"
  },
  timer: {
    marginTop: 5
  },
  departureText: {
    marginTop: 10
  },
  group: {
    opacity: Platform.OS === 'ios' ? 0.5 : 0.25,
    marginBottom: 40
  },
  backIcon: {
    marginTop: 15,
    marginLeft: 15,
    fontSize: RFPercentage(2.15),
    color:"#fff"
  },

});
