import React, { Component } from "react";
import { Text, View, StyleSheet, TouchableOpacity, StatusBar } from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import Calander from "./calander";
import Farsi from "./calanderfarsi";
import _ from "lodash";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import WaterMark from "../../components/WatermarkIconBlack";
import BtnGradientCheckmark from "../../components/BtnGradientCheckmark";
import Cregorian from "./Cregorian";
import FlightOptions from "../../utilities/navigations/FlightOptions";
var moment = require("moment-jalaali");
moment().format("jYYYY-jM-jD");
import AppbarTitle from "../../components/AppbarTitle";

export default class CalendarComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      finishDate: null,
      render: false,
      marks: {},
      markDay: {},
      calendarSelection: false,
      gregorianStart: null,
      jamaliStart: null,
      gregorianFinish: null,
      jamaliFinish: null,
      monthRender: 0,
      JamaliOneDaySelected: null,
      gregorianOneDaySelected: null,
      oneDateSelected: null,
      calanderCode: true,
      sourceCode: null,
      date: ""
    };
  }

  updateState(oneDateSelected, gregorianOneDaySelected, JamaliOneDaySelected) {
    this.setState({
      oneDateSelected: oneDateSelected,
      gregorianOneDaySelected: gregorianOneDaySelected,
      JamaliOneDaySelected: JamaliOneDaySelected
    });
  }

  onBackPress(receivedValue) {
    this.state.oneDateSelected != null
      ? (receivedValue(
          this.state.oneDateSelected,
          this.state.gregorianOneDaySelected,
          this.state.JamaliOneDaySelected
        ),
        this.props.navigation.goBack())
      : alert("select your date");
  }
  navigateToBack() {
    this.props.navigation.navigate("FlightOptions");
  }

  render() {
    const receivedValue = this.props.navigation.getParam(
      "receivedValue",
      () => {}
    );
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#FFFFFF" />

        {/* <View>
          <Icon
            name="back"
            color="#000000"
            style={styles.backIcon}
            onPress={() => this.props.navigation.navigate("FlightOptions")}
          />
        </View> */}
        <AppbarTitle
          pageTitle="تقویم"
          backIconColor="black"
          titleTextColor="black"
          navigateTo={this.navigateToBack.bind(this)}
        />

        <View style={{ justifyContent: "flex-start", flex: 1 }}>
          <View>
            <View style={styles.headerTextContainer}>
              <Text style={styles.headerText}>
                تاریخ سفر خود را انتخاب کنید
              </Text>
            </View>

            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ calanderCode: !this.state.calanderCode })
                }
                style={{
                  marginTop: 10,
                  justifyContent: "center",
                  width: "25%",
                  borderWidth: 1,
                  borderColor: "#F09819",
                  borderRadius: 20
                }}
              >
                <View style={{}}>
                  <Text
                    style={{
                      textAlign: "center",
                      alignSelf: "center",
                      color: "#F09819",
                      fontSize: RFPercentage(1.9),
                      fontFamily: "Sahel",
                      includeFontPadding : false ,
                    }}
                  >
                    {this.state.calanderCode ? "تقویم شمسی" : "تقویم میلادی"}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={styles.calanderContainer}>
              {this.state.calanderCode ? (
                <Calander updateParentState={this.updateState.bind(this)} />
              ) : (
                <Farsi updateParentState={this.updateState.bind(this)} />
              )}
            </View>
          </View>

          <TouchableOpacity
            style={{
              width: 150,
              height: "6%",
              position: "absolute",
              bottom: 20,
              alignSelf: "center"
            }}
            onPress={() => this.onBackPress(receivedValue)}
          >
            <BtnGradientCheckmark />
          </TouchableOpacity>

          <WaterMark />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backIcon: {
    padding: 16,
    fontSize: RFPercentage(2.2)
  },
  headerText: {
    color: "#15479F",
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2)
  },
  headerTextContainer: {
    alignItems: "center"
  },
  calanderContainer: {
    marginLeft: 20,
    marginRight: 20,
    height: "90%"
  },
  dateText: {
    fontFamily: "Sahel",
    fontSize: 15,
    color: "#15479F"
  },
  EnDetails: {
    fontFamily: "Montserrat-Medium",
    fontSize: RFPercentage(2.1),
    color: "#2C3039"
  },
  Pdetails: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2.2),
    color: "#2C3039"
  }
});
