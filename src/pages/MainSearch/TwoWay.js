import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Button,
  TouchableHighlight,
  Image,
  PixelRatio,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
  FlatList,
  ActivityIndicator
} from "react-native";
import { baseUrl } from "../../constants/BaseURL";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import { Dimensions } from "react-native";
import AirLineSearchResult from "./AirLineSearchResult";
// importing styles from Hotel.js file:
import Hotel, { styles } from "./Hotel";
import BtnSearch from "../../components/BtnSearch";
import TravelFamilyContainer from "../../components/TravelFamilyContainer";
import WatermarkIcon from "../../components/WatermarkIcon";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import FlightOptionsContainer from "../../components/FlightOptionsContainer";
import LightCalanderContainer from "../../components/LightCalanderContainer";
import Indicator from "../Indicator";
import { connect } from "react-redux";
import PropTypes from "prop-types";
const axios = require("axios");
const Icon = createIconSetFromFontello(fontelloConfig);
import * as locActions from "../../actions/MyLocationAction";

class TwoWay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      airports: "",
      searchAirportPlaceHolder: "",
      modalType: "",
      departureCode: "مبدأ",
      departureAirport: "",
      departureCity: "",
      destinationCode: "مقصد",
      destinationAirport: "",
      destinationCity: "",
      startDate: "",
      finishDate: "",
      gregorianStart: null,
      jamaliStart: null,
      gregorianFinish: null,
      jamaliFinish: null,
      latitude: null,
      longitute: null,
      timestamp: null,
      fromModal: false,
      isDestinationReplaced: false,
      flightClass: "Economic"

    };
  }

  receivedValue = (
    startDate,
    finishDate,
    gregorianStart,
    gregorianFinish,
    jamaliStart,
    jamaliFinish
  ) => {
    this.setState({
      startDate,
      finishDate,
      gregorianStart,
      gregorianFinish,
      jamaliStart,
      jamaliFinish
    });
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  UNSAFE_componentWillReceiveProps(props) {
    if (props.departureCode != this.props.departureCode) {
      this.setState({ departureCode: props.departureCode });
    }
    if (props.departureCity != this.props.departureCity) {
      this.setState({ departureCity: props.departureCity });
    }
    if (props.departureAirport != this.props.departureAirport) {
      this.setState({ departureAirport: props.departureAirport });
    }
  }
  checkRequirements() {

    // this.state.startDate && this.state.finishDate == null
    //             ? console.warn("لطفا تاریخ پرواز را مشخص کنید")
    //             : (
    //               // console.warn(this.state.oneDateSelected),
    //               this.state.departureCity == null
    //                 ? console.warn("لطفا مبدأ پرواز را مشخص کنید")
    //                 : (
    //                   // console.warn(this.state.departureCity),
    //                   this.state.destinationCity == null
    //                     ? console.warn("لطفا مقصد پرواز را مشخص کنید")
    //                     : this.props.navigation.navigate(
    //                         "AirLineSearchResult",
    //                         {
    //                           departure: this.state.departureCode,
    //                           destination: this.state.destinationCode,
    //                           departureDate: this.state.startDate,
    //                           returnDate: this.state.finishDate,
    //                           adults: this.props.adultsNumber,
    //                           child: this.props.child,
    //                           infant: this.props.newBorn,
    //                           flightClass: "Economic",
    //                           sourceCode: 2
    //                         }
    //                       ,console.log(this.state.departureCode,this.state.destinationCode,this.state.startDate,this.state.finishDate,this.props.child,this.props.newBorn))))
    //                       }


    console.log(this.state.flightClass,this.state.departureCode,this.state.destinationCode,this.state.destinationCode, this.state.startDate, this.props.adultsNumber)


    if (
      this.props.adultsNumber >= this.props.newBorn &&
      this.state.startDate!= null  && this.state.finishDate != null &&
      this.state.destinationCity != null &&
      this.state.departureCity != null
    ) {
      this.props.navigation.navigate("Indicator", {
        flightClass: this.state.flightClass,
        departure: this.state.departureCode,
        destination: this.state.destinationCode,
        departureDate: this.state.startDate,
        returnDate: this.state.finishDate,
        adults: this.props.adultsNumber,
        child: this.props.child,
        infant: this.props.newBorn,
        sourceCode: 2

      });
    } else if (this.props.adultsNumber < this.props.newBorn) {
      console.log("تعداد بزرگسال نمی تواند از تعداد نوزاد کمتر باشد");
    } else if (this.state.startDate && this.state.finishDate == null) {
      console.log("لطفا تاریخ پرواز را مشخص کنید");
    } else if (this.state.departureCity == null) {
      console.log("لطفا مبدأ پرواز را مشخص کنید");
    } else if (this.state.destinationCity == null) {
      console.log("لطفا مقصد پرواز را مشخص کنید");
    }
  }
  updateFlightClassState(flightClass) {
    this.setState({
      flightClass: flightClass
    });
  }



  render() {
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center"
              // backgroundColor: "yellow",
              // marginTop: 100
            }}
          >
            <View
              style={{
                backgroundColor: "white",
                marginHorizontal: 0,
                padding: 0
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: 0,
                  padding: 15,
                  // height: 50,
                  // backgroundColor: "red",
                  // borderColor: "#15479F",
                  borderWidth: 0
                }}
              >
                <TouchableOpacity
                  onPress={() => this.setModalVisible(!this.state.modalVisible)}
                  style={{ padding: 8 }}
                >
                  <Icon
                    name="cancel"
                    size={15}
                    color="#B5C9E8"
                    onPress={() =>
                      this.setModalVisible(!this.state.modalVisible)
                    }
                  />
                </TouchableOpacity>
                <TextInput
                  style={{
                    flex: 1,
                    fontSize: RFPercentage(2.8),
                    // backgroundColor: "yellow",
                    padding: 4,
                    // color: "#B5C9E8",
                    marginLeft: 20
                  }}
                  placeholder={this.state.searchAirportPlaceHolder}
                  keyboardType="default"
                  placeholderTextColor="#B5C9E8"
                  autoCapitalize="words"
                  onChangeText={
                    text => {
                      if (text.length > 2) {
                        axios
                          .post(baseUrl + "/Airport/Search", {
                            Airport: text
                          })
                          .then(response => {
                            this.setState({ airports: response.data.Result });
                          })
                          .catch(function(error) {
                            console.warn(error);
                          });
                      }
                    }
                    // text => this.updateValue(text, "PlaceOfBirth")
                  }
                />
              </View>

              <View
                style={{ height: 2, width: "100%", backgroundColor: "#B5C9E8" }}
              />
            </View>

            <View
              style={{
                backgroundColor: "white",
                flex: 1,
                paddingHorizontal: 20,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <FlatList
                data={this.state.airports}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View
                    style={{ justifyContent: "center", alignItems: "center" }}
                  >
                    <Text
                      style={[prestyles.countryText]}
                      numberOfLines={1}
                      ellipsizeMode={"middle"}
                      // onPress={() => this.selectedPassanger(item.index)}
                      onPress={() => {
                        if (this.state.modalType == "1") {
                          this.setState({ departureCode: item.ID });
                          this.setState({ departureCity: item.En_City });
                          this.setState({
                            departureAirport: item.En_Name + "-" + item.Country
                          });
                          this.setModalVisible(!this.state.modalVisible);
                          this.setState({ fromModal: true });
                          // this.props.saveDeparture()
                        } else if (this.state.modalType == "2") {
                          this.setState({ destinationCode: item.ID });
                          this.setState({ destinationCity: item.En_City });
                          this.setState({
                            destinationAirport:
                              item.En_Name + "-" + item.Country
                          });
                          this.setModalVisible(!this.state.modalVisible);
                        }
                      }}
                    >
                      {item.ID}, {item.En_City}, {item.Country}, {item.En_Name}
                    </Text>
                    <View
                      style={{
                        height: 1,
                        width: "90%",
                        justifyContent: "center",
                        alignItems: "center",
                        backgroundColor: "#B5C9E8"
                      }}
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </Modal>

        <View style={[prestyles.destinationDepartureContainer]}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.setModalVisible(true);
              this.setState({
                searchAirportPlaceHolder: "فرودگاه مقصد را مشخص کنید",
                modalType: "2"
              });
            }}
          >
            <View
              style={[
                prestyles.destination,
                { marginRight: 5 },
                { alignItems: "center" }
              ]}
            >
              <View style={[styles.containerHeader, { width: "80%" }]}>
                <Icon
                  name="destination"
                  style={styles.containerIcon}
                  color="red"
                />
                <Text style={styles.containerTitle}>مقصد</Text>
              </View>
              <View style={{ flex: 1, justifyContent: "center" }}>
                <View style={[prestyles.textContainer]}>
                  <Text style={prestyles.airportName}>
                    {this.state.destinationCode}
                  </Text>
                  <Text style={prestyles.cityAirport}>
                    {this.state.destinationCity}
                  </Text>
                  <Text style={prestyles.cityDetails}>
                    {this.state.destinationAirport}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback
            onPress={() => {
              if (
                this.state.destinationCode != "مقصد" &&
                this.state.departureCode != "مبدأ"
              ) {
                this.setState({
                  departureCode: this.state.destinationCode,
                  departureCity: this.state.destinationCity,
                  departureAirport: this.state.destinationAirport,
                  destinationCode: this.state.departureCode,
                  destinationCity: this.state.departureCity,
                  destinationAirport: this.state.departureAirport
                });
              }
            }}
          >
            <View
              style={{
                position: "absolute",
                borderWidth: 3,
                borderColor: "#1B275A",
                width: 35,
                height: 35,
                zIndex: 1,
                elevation: 3,
                shadowColor: "#000",
                shadowOpacity: 0.3,
                shadowRadius: 1,
                shadowOffset: { width: 0, height: 0 },
                backgroundColor: "#fff",
                borderRadius: 20,
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              }}
            >
              <Icon
                name="displacement"
                size={PixelRatio.get() < 2 ? 14 : 18}
                color="#F09819"
              />
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback
            onPress={() => {
              this.setModalVisible(true);
              this.setState({
                searchAirportPlaceHolder: "فرودگاه مبدأ را مشخص کنید",
                modalType: "1"
              });
            }}
          >
            <View
              style={[
                prestyles.destination,
                { marginLeft: 5 },
                { alignItems: "center"  }
              ]}
            >
              <View style={[styles.containerHeader, { width: "80%" }]}>
                <Icon name="departure" style={styles.containerIcon} />
                <Text
                  style={styles.containerTitle}
                  onPress={() => {
                    // this.setState({ departureCity: this.prop });
                    //to load GPS coordinate and get nearest airport
                    // this.props.loadLoc();
                  }}
                >
                  مبدا
                </Text>
              </View>
              <View style={{ flex: 1, justifyContent: "center" }}>
                <View style={[prestyles.textContainer]}>
                  {this.props.isLoading ? (
                    <ActivityIndicator />
                  ) : (
                    <View style={[prestyles.textContainer]}>
                      <Text style={prestyles.airportName}>
                        {/* {this.state.fromModal
                          ? this.state.departureCode
                          : this.props.departureCode} */}
                        {this.state.departureCode}
                      </Text>
                      <Text style={prestyles.cityAirport}>
                        {/* {this.state.fromModal
                          ? this.state.departureCity
                          : this.props.departureCity} */}
                        {this.state.departureCity}
                      </Text>
                      <Text style={prestyles.cityDetails}>
                        {/* {this.state.fromModal
                          ? this.state.departureAirport
                          : this.props.departureAirport} */}
                        {this.state.departureAirport}
                      </Text>
                    </View>
                  )}
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <View style={styles.lightContainer}>
          <TravelFamilyContainer />
        </View>

        <TouchableOpacity
          style={[
            prestyles.BtnSearch,
            { height: PixelRatio.get() >= 3 ? "10%" : "13%", marginTop: 5 }
          ]}
          onPress={() =>
            this.props.navigation.navigate("TWCalendar", {
              flightOptionCode: 2,
              receivedValue: this.receivedValue
            })
          }
        >
          <LightCalanderContainer
            // startDate={this.state.startDate + " --- " + this.state.finishDate}
            gregorianStart={this.state.gregorianStart}
            gregorianFinish={this.state.gregorianFinish}
            jamaliFinish={this.state.jamaliFinish}
            jamaliStart={this.state.jamaliStart}
          />
        </TouchableOpacity>

        <FlightOptionsContainer 
                    updateParentState={this.updateFlightClassState.bind(this)}
                    />

        <TouchableOpacity
          style={prestyles.BtnSearch}
          onPress={() => this.checkRequirements()}

          // onPress={
          //   () =>
          //     this.state.startDate && this.state.finishDate == null
          //       ? console.warn("لطفا تاریخ پرواز را مشخص کنید")
          //       : (
          //         // console.warn(this.state.oneDateSelected),
          //         this.state.departureCity == null
          //           ? console.warn("لطفا مبدأ پرواز را مشخص کنید")
          //           : (
          //             // console.warn(this.state.departureCity),
          //             this.state.destinationCity == null
          //               ? console.warn("لطفا مقصد پرواز را مشخص کنید")
          //               : this.props.navigation.navigate(
          //                   "AirLineSearchResult",
          //                   {
          //                     departure: this.state.departureCode,
          //                     destination: this.state.destinationCode,
          //                     departureDate: this.state.startDate,
          //                     returnDate: this.state.finishDate,
          //                     adults: this.props.adultsNumber,
          //                     child: this.props.child,
          //                     infant: this.props.newBorn,
          //                     flightClass: "Economic",
          //                     sourceCode: 2
          //                   }
          //                 ,console.log(this.state.departureCode,this.state.destinationCode,this.state.startDate,this.state.finishDate,this.props.child,this.props.newBorn))))
          //                 }
          >
                        
          <BtnSearch />
        </TouchableOpacity>

        <WatermarkIcon zIndexWatermark= {-1} />
      </View>
    );
  }
}

export const prestyles = StyleSheet.create({
  destinationDepartureContainer: {
    height: PixelRatio.get() >= 3 ? "23%" : "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10
  },
  destination: {
    backgroundColor: "#E9EEF6",
    borderRadius: 6,
    flexDirection: "column",
    width: "41%",
    height: "100%"
  },
  textContainer: {
    alignItems: "center",
    flexDirection: "column",
    marginHorizontal: 10,
    flex:1,
    justifyContent : 'flex-end',
  },
  airportName: {
    color: "#15479F",
    fontFamily: "Montserrat-Bold",
    fontSize: RFPercentage(5.5),
    justifyContent: "center",
    flex: 0.29,
    padding : 10,
    // backgroundColor:'red',
    includeFontPadding:false
  },
  cityAirport: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.8),
    color: "#9EADC5",
    flex:0.12,
    alignSelf : 'center',
  },
  cityDetails: {
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.6),
    color: "#9EADC5",
    textAlign: "center",
    flex:0.3
  },
  BtnSearch: {
    width: "85%",
    height: PixelRatio.get() >= 3 ? "7%" : "9%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    marginTop: 20
  },
  countryText: {
    fontFamily: "Sahel",
    textAlign: "center",
    fontSize: RFPercentage(2.4),
    marginVertical: 5,
    color: "black"
  }
});

// const SAVE = "save";
// const mapStateToProps = state => ({
//     departureCode: state.MyLocationReducer.departureCode,
//     // departureCity: state.MyLocationReducer.departureCity,
//     // departureAirport: state.MyLocationReducer.departureAirport

// })

// const mapDispatchToProps = dispatch => ({

//     saveDeparture: () => {
//       // console.warn(departureCode),
//         dispatch({ type: SAVE})
//         // dispatch({ type: SAVE })
//         // dispatch({ type:  SAVE})
//     }

// })

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(TwoWay);

function mapStateToProps(state) {
  return {
    departureCode: state.MyLocationReducer.departureCode,
    departureCity: state.MyLocationReducer.departureCity,
    departureAirport: state.MyLocationReducer.departureAirport,
    isLoading: state.MyLocationReducer.isLoading,
    error: state.MyLocationReducer.error,
    adultsNumber: state.counter.adultsNumber,
    child: state.counter.child,
    newBorn: state.counter.newBorn
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadLoc: () => dispatch(locActions.loadLoc())

    // saveDeparture: () =>
    //   dispatch({ type: "save", departureCode: this.state.departureCode })
  };
}

export default connect(
  mapStateToProps
  // mapDispatchToProps
)(TwoWay);
