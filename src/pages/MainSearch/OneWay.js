import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  PixelRatio,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
  FlatList,
  ActivityIndicator,
  Alert,
  BackHandler,
  AsyncStorage,
  StatusBar
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
// importing stayles from Hotel.js file:
import Hotel, { styles } from "./Hotel";
import TwoWay, { prestyles } from "./TwoWay";
import { baseUrl } from "../../constants/BaseURL";
import BtnSearch from "../../components/BtnSearch.js";
import TravelFamilyContainer from "../../components/TravelFamilyContainer";
import FlightOptionsContainer from "../../components/FlightOptionsContainer";
import LightCalanderContainer from "../../components/LightCalanderContainer";
import WatermarkIcon from "../../components/WatermarkIcon";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { connect } from "react-redux";
import axios from "axios";
import HandleBack from "../../utilities/HandleBack";
import Indicator from "../Indicator";
import AirlineSearchResult from "./AirLineSearchResult";
import Header from "../Drawer/Header";
import { NavigationActions } from "react-navigation";
import * as locActions from "../../actions/MyLocationAction";
import { USER_ID } from "../../constants/UserParams";
// import firebase from "react-native-firebase";

// firebase.crashlytics().crash();
class OneWay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      airports: "",
      searchAirportPlaceHolder: "",
      modalType: "",
      departureCode: "مبدأ",
      departureAirport: "",
      departureCity: null,
      destinationCode: "مقصد",
      destinationAirport: "",
      destinationCity: null,
      oneDateSelected: null,
      JamaliOneDaySelected: null,
      gregorianOneDaySelected: null,
      fromModal: false,
      isStatusTrue: null,
      flightClass: "Economic"
    };
  }

  onBack = () => {
    Alert.alert(
      "You're about to leave!",
      "Are you sure?",
      [
        { text: "Stay", onPress: () => {}, style: "cancel" },
        {
          text: "Exit",
          onPress: () => {
            BackHandler.exitApp();
          }
        }
      ],
      { cancelable: true }
    );
    return true;
  };

  updateFlightClassState(flightClass) {
    this.setState({
      flightClass: flightClass
    });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  UNSAFE_componentWillReceiveProps(props) {
    if (props.departureCode != this.props.departureCode) {
      this.setState({ departureCode: props.departureCode });
    }
    if (props.departureCity != this.props.departureCity) {
      this.setState({ departureCity: props.departureCity });
    }
    if (props.departureAirport != this.props.departureAirport) {
      this.setState({ departureAirport: props.departureAirport });
    }
  }

  // componentDidMount = async () => {
  UNSAFE_componentWillMount = async () => {
    // StatusBar.setHidden(false);
    //to load GPS coordinate and get nearest airport
    this.props.loadLoc();
    // try {
    //   const value = await AsyncStorage.getItem(USER_ID);
    //   if (value !== null) {
    //     // setTimeout(() => {this.props.navigation.navigate('FlightOptions')}, 1000)
    //     console.log(value);
    //   } else {
    //     setTimeout(() => {
    //       this.props.navigation.navigate("LoginTest");
    //     }, 1000);
    //   }
    // } catch (error) {
    //   // Error retrieving data
    // }
    // navigator.geolocation.getCurrentPosition(position => {
    //   console.log(position.coords.latitude.toString())
    // },
    // error => {
    //   console.log(error)
    // }
    // );
  };

  receivedValue = (
    oneDateSelected,
    gregorianOneDaySelected,
    JamaliOneDaySelected
  ) => {
    this.setState({
      oneDateSelected,
      gregorianOneDaySelected,
      JamaliOneDaySelected
    });
  };

  checkRequirements() {
    if (
      this.props.adultsNumber >= this.props.newBorn &&
      this.state.oneDateSelected != null &&
      this.state.destinationCity != null &&
      this.state.departureCity != null
    ) {
      this.props.navigation.navigate("Indicator", {
        departure: this.state.departureCode,
        // departure: "IKA",
        destination: this.state.destinationCode,
        // destination: "YYZ",
        departureDate: this.state.oneDateSelected,
        adults: this.props.adultsNumber,
        child: this.props.child,
        infant: this.props.newBorn,
        flightClass: this.state.flightClass,
        sourceCode: 1
      });
    } else if (this.props.adultsNumber < this.props.newBorn) {
      console.warn("تعداد بزرگسال نمی تواند از تعداد نوزاد کمتر باشد");
    } else if (this.state.oneDateSelected == null) {
      console.warn("لطفا تاریخ پرواز را مشخص کنید");
    } else if (this.state.departureCity == null) {
      console.warn("لطفا مبدأ پرواز را مشخص کنید");
    } else if (this.state.destinationCity == null) {
      console.warn("لطفا مقصد پرواز را مشخص کنید");
    }
  }

  render() {
    return (
      <HandleBack onBack={this.onBack}>
        <View style={styles.container}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              // this.setState({ departureCode: "" });
              this.setModalVisible(!this.state.modalVisible);
              // console.warn(this.state.departureCode);
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "column",
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  backgroundColor: "white",
                  marginHorizontal: 0,
                  padding: 0
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    borderRadius: 0,
                    padding: 15,
                    borderWidth: 0
                  }}
                >
                  <TouchableOpacity
                    onPress={() =>
                      this.setModalVisible(!this.state.modalVisible)
                    }
                    style={{ padding: 8 }}
                  >
                    <Icon
                      name="cancel"
                      size={15}
                      color="#B5C9E8"
                      onPress={() =>
                        this.setModalVisible(!this.state.modalVisible)
                      }
                    />
                  </TouchableOpacity>
                  <TextInput
                    style={{
                      flex: 1,
                      fontSize: RFPercentage(2.8),
                      padding: 4,
                      marginLeft: 20
                    }}
                    autoFocus={true}
                    placeholder={this.state.searchAirportPlaceHolder}
                    keyboardType="default"
                    placeholderTextColor="#B5C9E8"
                    autoCapitalize="words"
                    onChangeText={
                      text => {
                        if (text.length > 2) {
                          axios
                            .post(baseUrl + "/Airport/Search", {
                              Airport: text
                            })
                            .then(response => {
                              this.setState({ airports: response.data.Result });
                              console.log(response.data.Result);
                            })
                            .catch(function(error) {
                              console.log(error);
                              console.warn(
                                "error handling require here: **no connection to server!**"
                              );
                            });
                        }
                      }
                      // text => this.updateValue(text, "PlaceOfBirth")
                    }
                  />
                </View>

                <View
                  style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "#B5C9E8"
                  }}
                />
              </View>

              <View
                style={{
                  backgroundColor: "white",
                  flex: 1,
                  paddingHorizontal: 20,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <FlatList
                  data={this.state.airports}
                  extraData={this.state}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item }) => (
                    <View
                      style={{ justifyContent: "center", alignItems: "center" }}
                    >
                      <Text
                        style={[prestyles.countryText]}
                        numberOfLines={1}
                        ellipsizeMode={"middle"}
                        onPress={() => {
                          if (this.state.modalType == "1") {
                            this.setState({ departureCode: item.ID });
                            this.setState({ departureCity: item.En_City });
                            this.setState({
                              departureAirport:
                                item.En_Name + "-" + item.Country
                            });
                            this.setModalVisible(!this.state.modalVisible);
                            this.setState({ fromModal: true });
                          } else if (this.state.modalType == "2") {
                            this.setState({ destinationCode: item.ID });
                            this.setState({ destinationCity: item.En_City });
                            this.setState({
                              destinationAirport:
                                item.En_Name + "-" + item.Country
                            });
                            this.setModalVisible(!this.state.modalVisible);
                          }
                        }}
                      >
                        {item.ID}, {item.En_City}, {item.Country},{" "}
                        {item.En_Name}
                      </Text>
                      <View
                        style={{
                          height: 1,
                          width: "90%",
                          justifyContent: "center",
                          alignItems: "center",
                          backgroundColor: "#B5C9E8"
                        }}
                      />
                    </View>
                  )}
                />
              </View>
            </View>
          </Modal>

          <View style={[prestyles.destinationDepartureContainer]}>
            <TouchableWithoutFeedback
              onPress={() => {
                this.setModalVisible(true);
                this.setState({
                  searchAirportPlaceHolder: "فرودگاه مقصد را مشخص کنید",
                  modalType: "2"
                });
              }}
            >
              <View
                style={[
                  prestyles.destination,
                  { marginRight: 5 },
                  { alignItems: "center" }
                ]}
              >
                <View style={[styles.containerHeader, , { width: "80%" }]}>
                  <Icon name="destination" style={styles.containerIcon} />
                  <Text style={styles.containerTitle}>مقصد</Text>
                </View>
                <View style={{ flex: 1, justifyContent: "center" }}>
                  <View style={[prestyles.textContainer]}>
                    <Text style={prestyles.airportName}>
                      {this.state.destinationCode}
                    </Text>
                    <Text style={prestyles.cityAirport}>
                      {this.state.destinationCity}
                    </Text>
                    <Text style={prestyles.cityDetails}>
                      {this.state.destinationAirport}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                if (
                  this.state.destinationCode != "مقصد" &&
                  this.state.departureCode != "مبدأ"
                ) {
                  this.setState({
                    departureCode: this.state.destinationCode,
                    departureCity: this.state.destinationCity,
                    departureAirport: this.state.destinationAirport,
                    destinationCode: this.state.departureCode,
                    destinationCity: this.state.departureCity,
                    destinationAirport: this.state.departureAirport
                  });
                }
              }}
            >
              <View
                style={{
                  position: "absolute",
                  borderWidth: 3,
                  borderColor: "#1B275A",
                  width: 35,
                  height: 35,
                  zIndex: 1,
                  elevation: 3,
                  shadowColor: "#000",
                  shadowOpacity: 0.3,
                  shadowRadius: 1,
                  shadowOffset: { width: 0, height: 0 },
                  backgroundColor: "#fff",
                  borderRadius: 20,
                  justifyContent: "center",
                  alignItems: "center",
                  flex: 1
                }}
              >
                <Icon
                  name="displacement"
                  size={PixelRatio.get() < 2 ? 14 : 18}
                  color="#F09819"
                />
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                this.setModalVisible(true);
                this.setState({
                  searchAirportPlaceHolder: "فرودگاه مبدأ را مشخص کنید",
                  modalType: "1"
                });
              }}
            >
              <View
                style={[
                  prestyles.destination,
                  { marginLeft: 5 },
                  { alignItems: "center" }
                ]}
              >
                <View style={[styles.containerHeader, { width: "80%" }]}>
                  <Icon name="departure" style={styles.containerIcon} />
                  <Text
                    style={styles.containerTitle}
                    onPress={() => {
                      // this.setState({ fromModal: false });
                      //to load GPS coordinate and get nearest airport
                      // this.props.loadLoc();
                    }}
                  >
                    مبدا
                  </Text>
                </View>

                <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    alignSelf: "center"
                  }}
                >
                  <View
                    style={[
                      prestyles.textContainer,
                      { justifyContent: "center" }
                    ]}
                  >
                    {this.props.isLoading ? (
                      <ActivityIndicator />
                    ) : (
                      <View style={[prestyles.textContainer]}>
                        <Text style={prestyles.airportName}>
                          {/* {this.state.fromModal
                          ? this.state.departureCode
                          : this.props.departureCode} */}
                          {this.state.departureCode}
                        </Text>
                        <Text style={prestyles.cityAirport}>
                          {/* {this.state.fromModal
                          ? this.state.departureCity
                          : this.props.departureCity} */}
                          {this.state.departureCity}
                        </Text>
                        <Text style={prestyles.cityDetails}>
                          {/* {this.state.fromModal
                          ? this.state.departureAirport
                          : this.props.departureAirport} */}
                          {this.state.departureAirport}
                        </Text>
                      </View>
                    )}
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <View style={styles.lightContainer}>
            <TravelFamilyContainer />
          </View>

          <TouchableOpacity
            style={[
              prestyles.BtnSearch,
              { height: PixelRatio.get() >= 3 ? "10%" : "13%", marginTop: 5 }
            ]}
            onPress={() =>
              this.props.navigation.navigate("OneWayCalendar", {
                receivedValue: this.receivedValue
              })
            }
          >
            <LightCalanderContainer
              date={this.state.oneDateSelected}
              gregorianString={this.state.gregorianOneDaySelected}
              jalaaliString={this.state.JamaliOneDaySelected}
            />
          </TouchableOpacity>

          <FlightOptionsContainer
            updateParentState={this.updateFlightClassState.bind(this)}
          />

          <TouchableOpacity
            style={prestyles.BtnSearch}
            onPress={() => this.checkRequirements()}
          >
            <BtnSearch />
          </TouchableOpacity>

          <WatermarkIcon zIndexWatermark={-1} />
        </View>
      </HandleBack>
    );
  }
}

function mapStateToProps(state) {
  return {
    departureCode: state.MyLocationReducer.departureCode,
    departureCity: state.MyLocationReducer.departureCity,
    departureAirport: state.MyLocationReducer.departureAirport,
    isLoading: state.MyLocationReducer.isLoading,
    error: state.MyLocationReducer.error,
    adultsNumber: state.counter.adultsNumber,
    child: state.counter.child,
    newBorn: state.counter.newBorn
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loadLoc: () => dispatch(locActions.loadLoc())
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OneWay);
