import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Text,
  Animated,
  Easing,
  Dimensions,
  StatusBar,
  ToastAndroid,
  KeyboardAvoidingView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  PixelRatio,
  ScrollView
} from "react-native";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
import LinearGradient from "react-native-linear-gradient";
const Icon = createIconSetFromFontello(fontelloConfig);
import BtnGradient from "../components/BtnGradient";
import WatermarkIconBlack from "../components/WatermarkIconBlack";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import texture from "../Images/texture.png";
import LoginAirPlane from "../Images/loginAirplane.png";
import PassengersInformation from "./PassengersInformation";
import { Overlay } from "react-native-elements";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import axios from "axios";
import { IS_LOGIN_POSTPONED } from "../constants/UserParams";
import activeSamPay from "../Images/activeSamPay.png";
import inactiveSamPay from "../Images/inactiveSamPay.png";
import activeCIP from "../Images/activeCIP.png";
import inactiveCIP from "../Images/inactiveCIP.png";
import activeGift from "../Images/activeGift.png";
import inactiveGift from "../Images/inactiveGift.png";
import bank from "../Images/bank.png";
import BtnSearch from "../components/BtnRed";
import WatermarkIcon from "../components/WatermarkIcon.js";
import WaterMark from "../components/WatermarkIcon";
import { USER_ID, REQUEST_ID, REQUEST_INDEX } from "../constants/UserParams";
import AppbarTitle from "../components/AppbarTitle";
import { baseUrl } from "../constants/BaseURL";

let deviceWidth = Dimensions.get("window").width;

export default class Payment extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          activeIcon: activeCIP,
          inactiveIcon: inactiveCIP,
          id: "cip"
        },
        {
          activeIcon: activeSamPay,
          inactiveIcon: inactiveSamPay,
          id: "sampay"
        },
        {
          activeIcon: activeGift,
          inactiveIcon: inactiveGift,
          id: "gift"
        }
      ],
      gateWay: [
        {
          bankIcon: bank,
          id: 1
        },
        {
          bankIcon: bank,
          id: 2
        },
        {
          bankIcon: bank,
          id: 3
        },
        {
          bankIcon: bank,
          id: 4
        }
      ],
      sampaySelected: false,
      itemSampayPressed: false,
      bankSelected: false,
      walletCredit: "",
      Departure_Date_Pr: "",
      Departure_Date_En: "",
      Departure_Time: "",
      isLoading: false,
      //available params:
      reqId: "",
      reqIndex: "",
      userId: "",
      passengerList: "",
      paymentPrimaryInfo: {}
    };
  }

  async componentWillMount() {
    try {
      const userId = await AsyncStorage.getItem(USER_ID);
      const reqId = await AsyncStorage.getItem(REQUEST_ID);
      const reqIndex = await AsyncStorage.getItem(REQUEST_INDEX);
      await this.setState({ userId: userId, reqId: reqId, reqIndex: reqIndex });
      const { navigation } = this.props;
      const passengerList = navigation.getParam(
        "passengerList",
        "passengerList"
      );
      const paymentPrimaryInfo = navigation.getParam(
        "paymentPrimaryInfo",
        "paymentPrimaryInfo"
      );
      this.setState({
        passengerList: passengerList,
        paymentPrimaryInfo: paymentPrimaryInfo,
        walletCredit:
          "موجودی کیف پول" + " " + paymentPrimaryInfo.Credit + " " + "تومان",
        Departure_Date_En: paymentPrimaryInfo.Departure_Date_En,
        Departure_Date_Pr: paymentPrimaryInfo.Departure_Date_Pr,
        Departure_Time: paymentPrimaryInfo.Departure_Time
      });
    } catch (error) {
      console.warn("userId is not available");
    }
  }

  componentDidMount() {
    StatusBar.setHidden(false);
  }

  sampayTypeSelected(value) {
    this.setState({
      itemSampayPressed: value
    });
  }

  bankTypeSelected(value) {
    this.setState({
      bankSelected: value
    });
  }

  navigateToBack() {
    this.props.navigation.navigate("PassengersInformation");
  }

  render() {
    let payload = {
      Passengers: this.state.passengerList,
      UserID: this.state.userId.toString(),
      Req_ID: this.state.reqId,
      Index: this.state.reqIndex,
      PayType: "0"
    };
    return (
      <LinearGradient
        colors={["#1B275A", "#1B275A", "#1B275A"]}
        style={styles.container}
      >
        <AppbarTitle
          pageTitle="پرداخت"
          backIconColor="white"
          titleTextColor="white"
          navigateTo={this.navigateToBack.bind(this)}
        />

        <Overlay
          isVisible={this.state.isLoading}
          windowBackgroundColor="rgba(0, 0, 0, .7)"
          overlayBackgroundColor="white"
          width="auto"
          height="auto"
          borderRadius={6}
        >
          <ActivityIndicator />
        </Overlay>

        <ScrollView style={{ zIndex: 2 }}>
          <View style={{ paddingHorizontal: 20, justifyContent: "flex-start" }}>
            <View style={{ justifyContent: "flex-end" }}>
              <FlatList
                horizontal={true}
                contentContainerStyle={{
                  flex: 1,
                  justifyContent: "space-between"
                }}
                data={this.state.data}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View style={styles.itemContainer}>
                    <TouchableOpacity
                      style={styles.optionWrapper}
                      onPress={() => this.sampayTypeSelected(item.id)}
                      activeOpacity={0.9}
                    >
                      <Image
                        source={
                          this.state.itemSampayPressed === item.id
                            ? item.activeIcon
                            : item.inactiveIcon
                        }
                      />
                    </TouchableOpacity>
                  </View>
                )}
              />

              <Text style={styles.infoText}>
                {/* دریافت 5% تخفیف بر روی همین خرید شما */}
              </Text>
            </View>

            <View style={styles.detailContainer}>
              <View style={styles.detailWrapper}>
                <TouchableOpacity
                  style={[
                    styles.samPayContainer,
                    {
                      borderColor: this.state.sampaySelected
                        ? "#15479F"
                        : "#9EADC5"
                    }
                  ]}
                  onPress={() =>
                    this.setState({
                      sampaySelected: !this.state.sampaySelected
                    })
                  }
                >
                  <View
                    activeOpacity={0.9}
                    style={[
                      styles.selectCircle,
                      {
                        borderColor: this.state.sampaySelected
                          ? "#15479F"
                          : "#9EADC5"
                      }
                    ]}
                  >
                    {this.state.sampaySelected ? (
                      <View style={styles.bullet} />
                    ) : null}
                  </View>
                  <Image
                    source={
                      this.state.sampaySelected ? activeSamPay : inactiveSamPay
                    }
                  />
                  <Text
                    style={[
                      styles.credit,
                      {
                        color: this.state.sampaySelected ? "#15479F" : "#9EADC5"
                      }
                    ]}
                  >
                    {this.state.walletCredit}
                  </Text>
                </TouchableOpacity>
                <View>
                  <FlatList
                    style={{}}
                    horizontal
                    contentContainerStyle={{
                      flex: 1,
                      justifyContent: "space-between",
                      marginBottom: "3%"
                    }}
                    data={this.state.gateWay}
                    renderItem={({ item }) => (
                      <View style={[styles.itemContainer, { padding: 0 }]}>
                        <TouchableOpacity
                          style={[
                            styles.optionWrapper,
                            {
                              width: 0.18 * (deviceWidth - 20),
                              height: 0.18 * (deviceWidth - 20),
                              borderColor:
                                this.state.bankSelected === item.id
                                  ? "#15479F"
                                  : "#BDD1F5",
                              borderWidth: 1
                            }
                          ]}
                          onPress={() => this.bankTypeSelected(item.id)}
                          activeOpacity={0.9}
                        >
                          <Image source={item.bankIcon} />
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                </View>
                <View style={styles.travelSummery}>
                  <View style={styles.timeTravel}>
                    <View style={styles.date}>
                      <Text style={[styles.timeText]}>
                        {this.state.Departure_Date_En}
                      </Text>
                      <Text style={styles.timeText}>
                        {" "}
                        | {this.state.Departure_Date_Pr}
                      </Text>
                    </View>
                    <View style={styles.time}>
                      <Icon name="departure" style={styles.departureIcon} />
                      <Text style={[styles.timeText]}>
                        {this.state.Departure_Time}
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={[
                        styles.cityContainer,
                        {
                          paddingRight: 5,
                          width: " 30%",
                          textAlign: "center",
                          backgroundColor: "transparent"
                        }
                      ]}
                    >
                      ونکوور
                    </Text>
                    <View
                      style={{
                        flexDirection: "row",
                        justifyContent: "center",
                        alignItems: "center",
                        width: "35%",
                        backgroundColor: "transparent"
                      }}
                    >
                      <Icon name="substraction" style={styles.stopIcon} />
                      <View
                        style={{
                          height: 1,
                          width: "70%",
                          backgroundColor: "#91A9CD",
                          marginHorizontal: 3
                        }}
                      />
                      <Icon name="substraction" style={styles.stopIcon} />
                    </View>
                    <Text
                      style={[
                        styles.cityContainer,
                        {
                          paddingLeft: 5,
                          width: " 30%",
                          textAlign: "center",
                          backgroundColor: "transparent"
                        }
                      ]}
                    >
                      تهران
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    paddingHorizontal: 5,
                    height: PixelRatio.get() < 3 ? "22%" : "25%",
                    justifyContent: "space-between",
                    marginBottom: "3%"
                  }}
                >
                  <View
                    style={[
                      styles.priceDetails,
                      { paddingTop: 0, paddingBottom: 0 }
                    ]}
                  >
                    <Text style={styles.priceText}>مبلغ اولیه</Text>
                    <Text style={styles.priceText}>6،425،200</Text>
                  </View>
                  <View style={styles.priceDetails}>
                    <Text style={styles.priceText}>تخفیف</Text>
                    <Text style={styles.priceText}>1،230،000</Text>
                  </View>
                  <View style={styles.finalPriceDetails}>
                    <Text style={styles.finalPriceText}>مبلغ نهایی</Text>
                    <Text style={styles.finalPriceText}>5،725،200</Text>
                  </View>
                </View>
                <TouchableOpacity
                  style={{
                    height: PixelRatio.get() < 3 ? "10%" : "11%",
                    marginTop: 0,
                    zIndex: 4
                  }}
                  onPress={
                    () => {
                      this.setState({ isLoading: true });
                      var self = this;
                      axios({
                        url: baseUrl + "/flight/reserve",
                        method: "post",
                        data: payload
                      })
                        .then(function(response) {
                          self.setState({ isLoading: false });
                          // your action after success
                          // console.warn(response.data.Result.Bank_Url);
                          console.log(response.data);
                          self.props.navigation.navigate("ReserveMessage", {
                            message: response.data.Message
                          });
                        })
                        .catch(function(error) {
                          self.setState({ isLoading: false });
                          console.log(error);
                          console.warn(error);
                        });
                      // console.warn(this.state.reqIndex)
                    }

                    // axios({
                    //   url: baseUrl + "/flight/reserve",
                    //   method: "post",
                    //   data: payload
                    // })
                    //   .then(function(response) {
                    //     // self.props.navigation.navigate("FlighOptions", {
                    //     //   // passengerList: payload.Passengers,
                    //     //   // paymentPrimaryInfo: response.data.Result
                    //     // });
                    //     console.warn(response.data.Result.Bank_Url);
                    //   })
                    //   .catch(function(error) {
                    //     console.warn(error);
                    //   })
                    // console.warn(payload.Passengers)
                  }
                >
                  <BtnSearch type="پرداخت" />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>

        <WaterMark zIndexWatermark={0} />
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerContainer: {},
  backIcon: {
    color: "#ffffff",
    fontSize: RFPercentage(2.2),
    width: 40,
    height: 30,
    margin: 16
  },
  optionWrapper: {
    backgroundColor: "#FFFFFF",
    // margin: 10,
    width: 0.24 * deviceWidth,
    height: 0.24 * deviceWidth,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 6
  },
  bullet: {
    height: 0.02 * deviceWidth,
    width: 0.02 * deviceWidth,
    backgroundColor: "#15479F",
    borderRadius: 10
  },
  itemContainer: {
    padding: 5
  },
  infoText: {
    color: "#FFBA00",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2),
    alignSelf: "center"
  },
  detailContainer: {
    backgroundColor: "#E9EEF6",
    // width: "100%",
    borderRadius: 6,
    marginTop: "5%",
    justifyContent: "space-between",
    paddingBottom: 20,
    zIndex: 2
  },
  detailWrapper: {
    padding: "5%",
    justifyContent: "space-between",
    flex: 1
    // height : '65%',
  },
  samPayContainer: {
    backgroundColor: "#FFFFFF",
    borderRadius: 6,
    borderWidth: 1,
    borderColor: "#9EADC5",
    alignItems: "center",
    flexDirection: "row-reverse",
    padding: 10,
    height: PixelRatio.get() < 3 ? "18%" : "20%",
    marginBottom: "3%"
  },
  selectCircle: {
    width: 0.04 * deviceWidth,
    height: 0.04 * deviceWidth,
    borderRadius: 10,
    borderWidth: 1.5,
    alignSelf: "center",
    margin: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  credit: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.9),
    color: "#8A8795",
    alignSelf: "center",
    paddingRight: 10,
    textAlign: "center",
    flex: 1
  },
  travelSummery: {
    backgroundColor: "#CDDCF3",
    marginTop: 0,
    padding: 10,
    paddingHorizontal: 10,
    borderRadius: 6,
    height: PixelRatio.get() < 3 ? "17%" : "20%",
    justifyContent: "space-between",
    marginBottom: "3%"
  },
  date: {
    flexDirection: "row-reverse"
  },
  time: {
    flexDirection: "row"
  },
  timeTravel: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  timeText: {
    color: "#15479F",
    fontFamily: "Montserrat-Regular",
    fontSize: RFPercentage(1.7)
  },
  departureIcon: {
    color: "#15479F",
    fontSize: RFPercentage(2.2)
  },
  stopIcon: {
    color: "#91A9CD",
    fontSize: RFPercentage(1.8)
  },
  cityContainer: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(1.8),
    color: "#15479F"
  },
  priceDetails: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  priceText: {
    color: "#15479F",
    fontSize: RFPercentage(2),
    fontFamily: "Sahel"
    // padding : 5
  },
  finalPriceDetails: {
    flexDirection: "row-reverse",
    justifyContent: "space-between"
  },
  finalPriceText: {
    color: "#15479F",
    fontSize: RFPercentage(2.1),
    fontFamily: "Sahel-Bold"
  }
});
