import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  Animated,
  Easing,
  StatusBar
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import worldmark from "../Images/watermark.png";
import group from "../Images/group.png";
import PropTypes from "prop-types";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import indicator from "../Images/loading.png";
import TicketDetails from "./TicketDetails";
import axios from "axios";
import { baseUrl } from "../constants/BaseURL";
import _ from "lodash";
export default class Indicator extends Component {
  spinValue = new Animated.Value(0);

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      requestStatus: null,
      requestResultCount: "",
      resultCount: "0",
      offerMessage: null
    };
  }
  static navigationOptions = {
    header: {
      visible: false
    }
  };

  async componentWillMount() {
    const { navigation } = this.props;
    const departure = navigation.getParam("departure", "departure");
    const destination = navigation.getParam("destination", "destination");
    const departureDate = navigation.getParam("departureDate", "departureDate");
    const adults = navigation.getParam("adults", "adults");
    const child = navigation.getParam("child", "child");
    const infant = navigation.getParam("infant", "infant");
    const flightClass = navigation.getParam("flightClass", "flightClass");
    const returnDate = navigation.getParam("returnDate", null);
    const sourceCode = navigation.getParam("sourceCode", "sourceCode");
    var self = this;

    await axios
      .post(baseUrl + "/Utility/Messages", {
        Type: 1
      })
      .then(function(response) {
        self.setState({ offerMessage: response.data.Result.Text });
      })
      .catch(function(error) {
        console.log(error);
        console.warn(
          "error handling require here: **no connection to server!**"
        );
      });

    await axios
      .post(baseUrl + "/Flight/Search", {
        Origin: departure,
        Destination: destination,
        DepartureDate: departureDate,
        ReturnDate: returnDate,
        Adult: adults,
        Child: child,
        Infant: infant,
        FlightClass: flightClass
      })
      .then(function(response) {
        console.log(response)
        if (!response.data.Status) {
          self.props.navigation.goBack();
        } else if (response.data.Status) {
          self.props.navigation.navigate("AirLineSearchResult", {
            sourceCode: sourceCode,
            data: response.data.Result,
            reqId: response.data.Req_ID
          });
        }
      })
      .catch(function(error) {
        console.log(error);
        console.warn(
          "error handling require here: **no connection to server!**"
        );
      });
  }

  componentDidMount() {
    this.spin();
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: 4000,
      easing: Easing.linear
    }).start(() => this.spin());
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["360deg", "0deg"]
    });

    return (
      <LinearGradient
        colors={["#15479F", "#883D60", "#F63323"]}
        style={styles.LinearGradient}
      >
        <StatusBar backgroundColor="#15479F" />
        <View style={styles.container}>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Animated.Image
              source={indicator}
              style={{ transform: [{ rotate: spin }] }}
            />
            <View
              style={{
                position: "absolute",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Icon name="logo" style={styles.logo} color="#ffffff" />
              <Icon name="typography" style={styles.typo} color="#ffffff" />
            </View>
          </View>
          <Text numberOfLines={4} style={styles.indicatorStyle}>
            {this.state.offerMessage}
          </Text>
        </View>

        <Image
          source={worldmark}
          style={{ position: "absolute", marginLeft: "-160%", top: "10%" }}
        />
        <Image
          source={group}
          resizeMode="center"
          style={{ position: "absolute", width: "100%", bottom: 10 }}
        />
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  LinearGradient: {
    flex: 1
  },
  container: {
    justifyContent: "center",
    flex: 1,
    alignItems: "center",
    zIndex: 10
  },
  indicatorStyle: {
    color: "#ffffff",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2.2),
    marginLeft: "15%",
    marginRight: "15%",
    textAlign: "center",
    marginBottom: "20%",
    marginTop: 10
  },
  typo: {
    fontSize: RFPercentage(5.5),
    marginTop: 10
  },
  logo: {
    fontSize: RFPercentage(9)
  }
});

Indicator.propTypes = {
  indicatorText: PropTypes.string.isRequired
};

Indicator.defaultProps = {
  indicatorText:
    "مبلغ 10% از پرداخت این خرید به حساب شما باز خواهد گشت و در سفر بعدی قابل استفاده خواهد بود!"
};
