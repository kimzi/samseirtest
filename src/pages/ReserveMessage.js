import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  StatusBar,
  FlatList,
  Dimensions
} from "react-native";

import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import BtnGradient from "../components/BtnGradient";
import WatermarkIcon from "../components/WatermarkIconBlack";
import Logo from "../Images/pngLogo.png";
import {
  IS_WALKTHROUGH,
  IS_LOGIN_POSTPONED,
  USER_ID
} from "../constants/UserParams";
import activeSamPay from "../Images/activeSamPay.png";
import inactiveSamPay from "../Images/inactiveSamPay.png";
import activeCIP from "../Images/activeCIP.png";
import inactiveCIP from "../Images/inactiveCIP.png";
import activeGift from "../Images/activeGift.png";
import inactiveGift from "../Images/inactiveGift.png";
let deviceWidth = Dimensions.get("window").width;
import airplane from "../Images/success.png";
import AppbarTitle from "../components/BlackAppbarTitle";

export default class ReseveMessage extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      message: null
    };
  }
  navigateToBack() {
    this.props.navigation.navigate("FlightOptions");
  }
  //   componentWillMount(){
  //     const { navigation } = this.props;
  //     const message = navigation.getParam('message', 'message');
  //     this.setState({message : message})

  //   }

  render() {
    const { navigation } = this.props;
    const message = navigation.getParam("message", "message");

    return (
      <View style={{ flex: 1 }}>
        <AppbarTitle
          //   pageTitle=""
          navigateTo={this.navigateToBack.bind(this)}
        />

        <StatusBar backgroundColor="#fff" />
        <View style={styles.container}>
          <View style={styles.logoContainer}>
            <Image source={Logo} style={styles.logo} />
            <Icon
              name="typography"
              style={{ fontSize: RFPercentage(5), marginTop: 10 }}
              color="#194A99"
            />
          </View>
          <View>
            <Image source={airplane} style={{ alignSelf: "center" }} />
          </View>
          <View>
            <Text style={styles.infoText} numberOfLines={4}>
              {message}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-between",
    paddingBottom: "30%"
  },
  logoContainer: {
    marginTop: 50,
    alignItems: "center"
  },
  linearGradient: {
    width: 120,
    height: "50%",
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  boxContainer: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center"
  },
  boxes: {
    alignItems: "center",
    borderWidth: 2,
    borderColor: "#91A9CD",
    width: 90,
    height: 90,
    justifyContent: "center"
  },
  textBox: {
    fontFamily: "Montserrat-Bold",
    color: "#B5C9E8"
  },
  logo: {
    // width: "20%",
    // height: "23%"
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center"
  },
  okBotton: {
    color: "#fff",
    textAlign: "center",
    fontFamily: "Sahel-Bold",
    fontSize: RFPercentage(2.3)
  },
  infoText: {
    color: "#000000",
    fontSize: RFPercentage(2.6),
    textAlign: "center",
    fontFamily: "Sahel-Bold",
    marginRight: 20,
    marginLeft: 20
  },
  infoContainer: {
    alignItems: "center",
    flex: 1
  },
  group: {
    opacity: 0.35,
    marginBottom: 40
  },
  optionWrapper: {
    backgroundColor: "#FFFFFF",
    borderColor: "#15479F",
    borderWidth: 1,
    // margin: 10,
    width: 0.24 * deviceWidth,
    height: 0.24 * deviceWidth,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 6
  }
});
