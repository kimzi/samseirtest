import React, { Component } from "react";
import { StyleSheet, View, Text, TextInput } from "react-native";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

export default class Splash extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: 1 //login : 0 and register : 1
    };
  }
  render() {
    return (
      <View>
        <View style={styles.container}>
          <View style={styles.itemContainer}>
            <Text style={styles.itemText}>نام و نام خانوادگی</Text>
            <TextInput
              style={styles.textInputStyle}
              keyboardType="default"
              autoCapitalize="words"
              // onChangeText={phoneNumber => this.setState({ phoneNumber })}
              // value={this.state.phoneNumber}
              // maxLength={15}
            />
          </View>
          <View style={styles.itemContainer}>
            <Text style={styles.itemText}>شماره موبایل</Text>
            <TextInput
              style={styles.textInputStyle}
              keyboardType="default"
              autoCapitalize="words"
              // onChangeText={phoneNumber => this.setState({ phoneNumber })}
              // value={this.state.phoneNumber}
              // maxLength={15}
            />
          </View>
          <View style={styles.itemContainer}>
            <Text style={styles.itemText}>ایمیل</Text>
            <TextInput
              style={styles.textInputStyle}
              keyboardType="default"
              autoCapitalize="words"
              // onChangeText={phoneNumber => this.setState({ phoneNumber })}
              // value={this.state.phoneNumber}
              // maxLength={15}
            />
          </View>
          <View style={styles.itemContainer}>
            <Text style={styles.itemText}>رمزعبور</Text>
            <TextInput
              style={styles.textInputStyle}
              keyboardType="default"
              autoCapitalize="words"
              // onChangeText={phoneNumber => this.setState({ phoneNumber })}
              // value={this.state.phoneNumber}
              // maxLength={15}
            />
          </View>
          <View style={styles.itemContainer}>
            <Text style={styles.itemText}>تکرار رمزعبور</Text>
            <TextInput
              style={styles.textInputStyle}
              keyboardType="default"
              autoCapitalize="words"
              // onChangeText={phoneNumber => this.setState({ phoneNumber })}
              // value={this.state.phoneNumber}
              // maxLength={15}
            />
          </View>
        </View>
      </View>
    );
  }
}
export const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
    marginRight: 10
  },
  itemContainer: {
    flexDirection: "row-reverse",
    justifyContent: "center",
    alignItems: "center"
  },
  itemText: {
    textAlign: "right",
    marginTop: 20,
    width: "33%",
    fontSize: RFPercentage(1.7),
    fontFamily: "Sahel-Bold",
    color: "#15479F"
  },
  textInputStyle: {
    height: 30,
    borderColor: "gray",
    borderBottomWidth: 1,
    flex: 1,
    marginRight: 10
  }
});
