import axios from "axios";
import { baseUrl } from "../constants/BaseURL";
import { PermissionsAndroid, Platform, Permission } from "react-native";
// import Geolocation from '@react-native-community/geolocation';

// Geolocation.getCurrentPosition(info => console.warn(info.toString()));
export const loadLoc = () => {
  return (dispatch, getState) => {
    if (Platform.OS === "android") {
      //Calling the permission function
      requestLocationPermission();
    } else {
      alert("IOS device found");
    }
    async function requestLocationPermission() {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          //To Check, If Permission is granted
          dispatch({ type: "LOAD_LOC_START" });
          navigator.geolocation.getCurrentPosition(
            position => {
              // console.log("get current location", position.coords);
              axios
                .post(baseUrl + "/Airport/Nearest", {
                  Latitude: position.coords.latitude,
                  Longitude: position.coords.longitude
                })
                .then(function(response) {
                  dispatch({
                    type: "LOAD_LOC_SUCCESS",
                    payload: response.data.Result[0].ID,
                    payload1: response.data.Result[0].En_City,
                    payload2:
                      response.data.Result[0].En_Name +
                      "-" +
                      response.data.Result[0].Country
                  });
                })
                .catch(function(error) {
                  dispatch({ type: "LOAD_LOC_FAILURE", payload: error });
                  // console.log("error", error);
                });
            },
            error => {
              dispatch({ type: "LOAD_LOC_FAILURE", payload: error });
            }
            // { enableHighAccuracy: true, timeout: 8000, maximumAge: 1000 }
          );
          // alert("You can use the CAMERA");
        } else {
          alert("Location permission denied");
          dispatch({ type: "LOAD_LOC_FAILURE" });
        }
      } catch (err) {
        alert(err);
        dispatch({ type: "LOAD_LOC_FAILURE" });
      }
    }
  };
};
