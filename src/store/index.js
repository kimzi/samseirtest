import { createStore, applyMiddleware } from "redux";
import reducers from "../reducers";
import thunk from 'redux-thunk'
export default (store = createStore(reducers, applyMiddleware(thunk)));

// console.warn(store.getState(), "done")
// store.dispatch({
//     type: 'ADD_TODO',
//     text: 'Use Redux'
//   })
//   console.log(store.getState(), "done")

// export default store
