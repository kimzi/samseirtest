import React, { Component } from "react";
import { StyleSheet, View, Dimensions, PixelRatio, Text } from "react-native";
import {
  createMaterialTopTabNavigator,
  createStackNavigator,
  createAppContainer
} from "react-navigation";
import OneWay from "../../pages/MainSearch/OneWay";
import TwoWay from "../../pages/MainSearch/TwoWay";
import SeveralWay from "../../pages/MainSearch/SeveralWay";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

const FlightOptions = createMaterialTopTabNavigator(
  {
    // "چند مسیره": {
    //   screen: SeveralWay,
    //   navigationOptions: {
    //     header: null,
    //     headerMode: "none",
    //     swipeEnabled: false
    //   }
    // },
    "یک طرفه": {
      screen: OneWay,
      navigationOptions: {
        header: null,
        headerMode: "none",
        swipeEnabled: false
      }
    },
    "رفت و برگشت": {
      screen: TwoWay,
      navigationOptions: {
        header: null,
        headerMode: "none",
        swipeEnabled: false
      }
    }
  },
  {
    initialRouteName: "یک طرفه",
    // order: ["چند مسیره", "یک طرفه", "رفت و برگشت"],
    order: ["یک طرفه", "رفت و برگشت"],
    //   navigationOptions:{
    //     header: ( /* Your custom header */
    //      <View
    //        style={{
    //          height: 80,
    //          marginTop: 20 /* only for IOS to give StatusBar Space */
    //        }}
    //      >
    //        <Text>This is CustomHeader</Text>
    //      </View>
    //    )
    //  },

    tabBarOptions: {
      labelStyle: {
        fontSize: RFPercentage(2),
        color: "#FFFFFF",
        fontFamily: "Sahel-Bold",
        textAlign: "center"
      },
      style: {
        shadowRadius: 0,
        elevation: 0,
        backgroundColor: "#1B275A"
      },
      tabStyle: {
        height: PixelRatio.get() < 2 ? 32 : 52
      },
      indicatorStyle: {
        height: PixelRatio.get() < 2 ? 2 : 3.4,
        backgroundColor: "#FFFFFF",
        width: "26%",
        marginLeft: "8.9%"
      }
      // indicatorStyle: {
      //   height: PixelRatio.get() < 2 ? 2 : 3.4,
      //   backgroundColor: "#FFFFFF",
      //   width: "16%",
      //   marginLeft: "6.2%"
      // }
    }
  }
  // {
  //   // navigationOptions:{
  //   //     header: ( /* Your custom header */
  //   //      <View
  //   //        style={{
  //   //          height: 80,
  //   //          marginTop: 20 /* only for IOS to give StatusBar Space */
  //   //        }}
  //   //      >
  //   //        <Text>This is CustomHeader</Text>
  //   //      </View>
  //   //    )
  //   //  }

  //   // headerMode: "none",
  //   // navigationOptions: {
  //   //   headerVisible: false,
  //   //   tabBarVisible: false
  //   // }
  // }
);

export default createAppContainer(FlightOptions);
