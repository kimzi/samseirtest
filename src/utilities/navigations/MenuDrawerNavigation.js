// import {
//   createDrawerNavigator,
//   DrawerItems,
//   createAppContainer
// } from "react-navigation";

// import React, { Component } from "react";
// import { Text, View, StatusBar , Image,AsyncStorage,Dimensions } from "react-native";
// import Edits from "../../pages/Drawer/Edits";
// import purchases from "../../pages/Drawer/purchases";
// import About from "../../pages/Drawer/About";
// import Wallet from '../../pages/Drawer/Wallet';
// import Support from '../../pages/Drawer/Support';
// import SendFeedBack from '../../pages/Drawer/SendFeedBack';
// import Exit from '../../pages/Drawer/Exit'
// import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
// import { createIconSetFromFontello } from "react-native-vector-icons";
// import fontelloConfig from "../../config.json";
// import OneWayCalendar from "../../pages/MainSearch/oneWayCalendar";
// const Icon = createIconSetFromFontello(fontelloConfig);
// import { createStackNavigator } from "react-navigation";
// import Splash from "../../pages/Splash";
// import WalkThrough from "../../pages/WalkThrough";
// import SearchSetting from "../../pages/MainSearch/SearchSetting";
// import AirLineSearchResult from "../../pages/MainSearch/AirLineSearchResult";
// import Cregorian from "../../pages/MainSearch/Cregorian";
// import Indicator from "../../pages/Indicator";
// import PassengersInformation from "../../pages/PassengersInformation";
// import TicketDetails from "../../pages/TicketDetails";
// import TestPage from "../../pages/TestPage";
// import LoginAndRegister from "../../pages/LoginAndRegister";
// import FlightOptions from "./FlightOptions";
// import LoginTest from "../../pages/LoginTest";
// import OneWayTicketDetails from "../../pages/OneWayTicketDetails";
// import TwoWayTicketDetails from "../../pages/TwoWayTicketDetails";
// import TWCalendar from "../../pages/MainSearch/TWCalendar";
// import PriceDetails from '../../pages/PriceDetails';
// import MainPage from '../../pages/MainPage';
// import ContentComponent from './ContentComponent';
// import Payment from '../../pages/Payment'
// const deviceWidth = Dimensions.get("window").width;

// let globalLogin = null;

// const AppNavigator = createStackNavigator(
//   {
//     TestPage: TestPage,
//     // Splash: {
//     //   screen: Splash,
//     //   navigationOptions: {
//     //     header: null
//     //   }
//     // },
//     Payment: {
//       screen: Payment,
//       navigationOptions: {
//         header: null
//       }
//     },

//     // Login: {
//     //   screen: LoginTest,
//     //   navigationOptions: {
//     //     header: null
//     //   }
//     // },
//     MainPage: {
//       screen: MainPage,
//       navigationOptions: {
//         header: null
//       }
//     },
//     Cregorian: {
//       screen: Cregorian,
//       navigationOptions: {
//         header: null
//       }
//     },
//     // WalkThrough: {
//     //   screen: WalkThrough,
//     //   navigationOptions: {
//     //     header: null
//     //   }
//     // },
//     FlightOptions: {
//       screen: FlightOptions,
//       navigationOptions: ({ navigation }) => {
//         return {
//           header: (
//             <View style={{ backgroundColor: "#1B275A", paddingTop: 0 }}>
//               <StatusBar backgroundColor="#1B275A" />
//               <View
//                 style={{
//                   flexDirection: "row",
//                   alignItems: "center",
//                   justifyContent: "center"
//                 }}
//               >
//                 <Text
//                   style={{
//                     fontSize: RFPercentage(2.5),
//                     fontFamily: "Sahel-Bold",
//                     color: "#ffffff",
//                     padding: 10
//                   }}
//                 >
//                   پرواز (داخلی/خارجی)
//                 </Text>
//               </View>
//               <View>
//                 <View style={{ marginTop: 30, marginLeft: 16, width: "10%" }}>
//                   <Icon
//                     name="menu"
//                     color="#ffffff"
//                     onPress={() => navigation.openDrawer()}
//                     style={{ fontSize: RFPercentage(3) }}
//                   />
//                 </View>
//               </View>
//             </View>
//           )
//         };
//       }
//     },
//     Indicator: {
//       screen: Indicator,
//       navigationOptions: {
//         header: null
//       }
//     },
//     TicketDetails: {
//       screen: TicketDetails,
//       navigationOptions: {
//         header: null
//       }
//     },
//     OneWayTicketDetails: {
//       screen: OneWayTicketDetails,
//       navigationOptions: {
//         header: null
//       }
//     },
//     TwoWayTicketDetails: {
//       screen: TwoWayTicketDetails,
//       navigationOptions: {
//         header: null
//       }
//     },
//     // LoginAndRegister: {
//     //   screen: LoginAndRegister,
//     //   navigationOptions: {
//     //     header: null
//     //   }
//     // },

//     PassengersInformation: {
//       screen: PassengersInformation,
//       navigationOptions: {
//         header: null
//       }
//     },
//     SearchSetting: SearchSetting,
//     AirLineSearchResult: {
//       screen: AirLineSearchResult,
//       navigationOptions: {
//         header: null
//       }
//     },
//     OneWayCalendar: {
//       screen: OneWayCalendar,
//       navigationOptions: {
//         header: null
//       }
//     },
//     TWCalendar: {
//       screen: TWCalendar,
//       navigationOptions: {
//         header: null
//       }
//     },
//     PriceDetails: {
//       screen: PriceDetails,
//       navigationOptions: {
//         header: null
//       }
//     },
//     "خروج": {
//       screen: Exit,
//       navigationOptions: {
//         label: null
//       }
//     }

//   },
//   {
//     initialRouteName: "FightOptions",
//     navigationOptions: {
//       drawerLabel: () => null
//     }
//     //   navigationOptions:{
//     //     header: ( /* Your custom header */
//     //      <View
//     //        style={{
//     //          height: 80,
//     //          backgroundColor : 'red',
//     //          marginTop: 20 /* only for IOS to give StatusBar Space */
//     //        }}
//     //      >
//     //        <Text>This is CustomHeader</Text>
//     //      </View>
//     //    )
//     //  },

//     // headerMode: "none",
//     // // mode: 'modal',
//     // navigationOptions: {
//     //   headerVisible: false
//     // }

//     // navigationOptions: ({ navigation }) => {
//     //   // const { routeName } = navigation.state.routes[navigation.state.index];
//     //   return {
//     //     headerTitle: 'title'
//     //   };
//     // }
//   }
// );

// // export default createAppContainer(AppNavigator);

// const App = createDrawerNavigator(
//   {
//   'خریدهای قبلی': {
//     screen: purchases,
//     navigationOptions: {
//       // drawerIcon: () => <Icon name="purchase" color="#FFBA00" size={20} style={{opacity : 1}}/>
//     }
//   },
//   "کیف پول": {
//     screen: Wallet,
//     navigationOptions: {
//       // drawerIcon: () => <Icon name="wallet" color="#FFBA00" size={20} />
//     }
//   },
//   "ویرایش": {
//     screen: Edits,
//     navigationOptions: {
//       // drawerIcon: () => <Icon name="edits" color="#FFBA00" size={20} />
//     }
//   },
//   "درباره سام سیر": {
//     screen: About,
//     navigationOptions: {
//       drawerIcon: () => <Icon name="aboutUs" color="#FFBA00" size={20} />
//     }
//   },

//   "تماس با پشتیبانی": {
//     screen: Support,
//     navigationOptions: {
//       drawerIcon: () => <Icon name="support" color="#FFBA00" size={20} />
//     }
//   },
//   "ارسال نظرات": {
//     screen: SendFeedBack,
//     navigationOptions: {
//       drawerIcon: () => <Icon name="feedback" color="#FFBA00" size={20} />
//     }
//   },
//   Navigations: {
//     screen: AppNavigator,
//     navigationOptions: {
//       label: null
//     }
//   },
//   "ورود": {
//     screen: LoginTest,
//     navigationOptions: {
//       label: null
//     }
//   }

// },
//   {
//     initialRouteName: "Navigations",
//     drawerBackgroundColor: "#15479F",
//     drawerLockMode: "locked-closed",
//     drawerType: "front",
//     drawerWidth: (deviceWidth * 84) / 100,
//     contentOptions: {
//       labelStyle: {
//         fontFamily: "Sahel",
//         fontSize: RFPercentage(2.2),
//         color: "#ffffff"
//       },
//       itemStyle: {
//         justifyContent: "flex-start",
//         flexDirection: "row-reverse"
//       },
//       itemsContainerStyle: {
//         flexDirection: "column"
//       },
//       iconContainerStyle: {}
//     },
//     // contentComponent: props => (
//     //   <LinearGradient
//     //   colors={["#15479F", "#183579", "#1B275A"]}
//     //   // style={[styles.mainLinerGradient ]}
//     //   style={{ flex: 1,paddingTop : 25 , padding:20}}
//     // >
//     // <View style={{ alignItems : 'flex-end'}}>
//     //   <View>
//     //     <Icon name='back' style={{color : '#FFFFFF'}}/>
//     //   </View>
//     //   <Image source={drawer} style={{alignSelf : 'center'}}/>
//     //   <View style={{ alignSelf : 'center'}}>
//     //     <Text style={{fontSize : RFPercentage(2.5) , color : '#FFBA00', paddingTop:10 }}>developers@samsair.com</Text>
//     //   </View>
//     //   </View>
//     //   <View style={{borderWidth:0.5 , borderColor:'#FFBA00' , marginTop:10}}/>
//     //     <DrawerItems
//     //       {...props}
//     //       onItemPress={(customItem, focused) => {
//     //         props.onItemPress({ customItem, focused });
//     //         alert("Under Construnction!!!");
//     //       }}
//     //     />
//     //     </LinearGradient>
//     // )
//     contentComponent : ContentComponent
// }
// );
// const root = createStackNavigator(
//   {
//       Splash: {
//         screen: Splash,
//         navigationOptions: {
//           header: null
//         }
//       },
//       Drawer: {
//         screen: App,
//         navigationOptions: {
//           header: null
//         }
//       },
//       LoginTest: {
//         screen: LoginTest,
//         navigationOptions: {
//           header: null
//         }
//       },
//       WalkThrough: {
//         screen: WalkThrough,
//         navigationOptions: {
//           header: null
//         }
//       },

//   },
//       {
//           initialRouteName:'Splash'
//       }
//   );

// export default createAppContainer(root);
import {
  createDrawerNavigator,
  createAppContainer,
  createSwitchNavigator
} from "react-navigation";

import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  Image,
  AsyncStorage,
  Dimensions,
  Switch
} from "react-native";
import Edits from "../../pages/Drawer/Edits";
import purchases from "../../pages/Drawer/purchases";
import About from "../../pages/Drawer/About";
import Wallet from "../../pages/Drawer/Wallet";
import Support from "../../pages/Drawer/Support";
import SendFeedBack from "../../pages/Drawer/SendFeedBack";
import Exit from "../../pages/Drawer/Exit";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
import OneWayCalendar from "../../pages/MainSearch/oneWayCalendar";
const Icon = createIconSetFromFontello(fontelloConfig);
import { createStackNavigator } from "react-navigation";
import Splash from "../../pages/Splash";
import WalkThrough from "../../pages/WalkThrough";
import SearchSetting from "../../pages/MainSearch/SearchSetting";
import AirLineSearchResult from "../../pages/MainSearch/AirLineSearchResult";
import Cregorian from "../../pages/MainSearch/Cregorian";
import Indicator from "../../pages/Indicator";
import PassengersInformation from "../../pages/PassengersInformation";
import TicketDetails from "../../pages/TicketDetails";
import FlightOptions from "./FlightOptions";
import LoginTest from "../../pages/LoginTest";
import OneWayTicketDetails from "../../pages/OneWayTicketDetails";
import TwoWayTicketDetails from "../../pages/TwoWayTicketDetails";
import TWCalendar from "../../pages/MainSearch/TWCalendar";
import PriceDetails from "../../pages/PriceDetails";
import ContentComponent from "./ContentComponent";
import Payment from "../../pages/Payment";
import FilterSearchResultOptionsContainer from "../../components/FilterSearchResultOptionsContainer";
import LittleIndicator from '../../components/LittleIndicator';
import ReserveMessage from "../../pages/ReserveMessage";
import purchaseDetails from '../../pages/purchaseDetails'
const deviceWidth = Dimensions.get("window").width;

let globalLogin = null;
const AppNavigator = createStackNavigator(
  {
    Payment: {
      screen: Payment,
      navigationOptions: {
        header: null
      }
    },
    ReserveMessage: {
      screen: ReserveMessage,
      navigationOptions: {
        header: null
      }
    },
      LittleIndicator: {
      screen: LittleIndicator,
      navigationOptions: {
        header: null
      }
    },

    purchaseDetails: {
      screen: purchaseDetails,
      navigationOptions: {
        header: null
      }
    },

    Cregorian: {
      screen: Cregorian,
      navigationOptions: {
        header: null
      }
    },
    WalkThrough: {
      screen: WalkThrough,
      navigationOptions: {
        header: null
      }
    },
    FlightOptions: {
      screen: FlightOptions,
      navigationOptions: ({ navigation }) => {
        return {
          header: (
            <View
              style={{
                backgroundColor: "#1B275A",
                padding: "5%",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center"
                // backgroundColor: "black"
              }}
            >
              <StatusBar backgroundColor="#1B275A" />
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  position: "absolute"
                }}
              >
                <Text
                  style={{
                    fontSize: RFPercentage(2.5),
                    fontFamily: "Sahel-Bold",
                    color: "#ffffff",
                    includeFontPadding: false
                    // padding: 4
                  }}
                >
                  پرواز (داخلی/خارجی)
                </Text>
              </View>
              <View style={{ position: "absolute", start: "5%" }}>
                <Icon
                  name="menu"
                  color="#ffffff"
                  onPress={() => navigation.openDrawer()}
                  style={{ fontSize: RFPercentage(3) }}
                />
              </View>
            </View>
          )
        };
      }
    },
    Indicator: {
      screen: Indicator,
      navigationOptions: {
        header: null
      }
    },
    TicketDetails: {
      screen: TicketDetails,
      navigationOptions: {
        header: null
      }
    },
    OneWayTicketDetails: {
      screen: OneWayTicketDetails,
      navigationOptions: {
        header: null
      }
    },
    TwoWayTicketDetails: {
      screen: TwoWayTicketDetails,
      navigationOptions: {
        header: null
      }
    },
    PassengersInformation: {
      screen: PassengersInformation,
      navigationOptions: {
        header: null
      }
    },
    SearchSetting: SearchSetting,
    AirLineSearchResult: {
      screen: AirLineSearchResult,
      navigationOptions: {
        header: null
      }
    },
    OneWayCalendar: {
      screen: OneWayCalendar,
      navigationOptions: {
        header: null
      }
    },
    TWCalendar: {
      screen: TWCalendar,
      navigationOptions: {
        header: null
      }
    },
    PriceDetails: {
      screen: PriceDetails,
      navigationOptions: {
        header: null
      }
    },
    خروج: {
      screen: Exit,
      navigationOptions: {
        label: null
      }
    }
  },
  {
    initialRouteName: "FlightOptions",
    navigationOptions: {
      drawerLabel: () => null
    }
    //   navigationOptions:{
    //     header: ( /* Your custom header */
    //      <View
    //        style={{
    //          height: 80,
    //          backgroundColor : 'red',
    //          marginTop: 20 /* only for IOS to give StatusBar Space */
    //        }}
    //      >
    //        <Text>This is CustomHeader</Text>
    //      </View>
    //    )
    //  },

    // headerMode: "none",
    // // mode: 'modal',
    // navigationOptions: {
    //   headerVisible: false
    // }

    // navigationOptions: ({ navigation }) => {
    //   // const { routeName } = navigation.state.routes[navigation.state.index];
    //   return {
    //     headerTitle: 'title'
    //   };
    // }
  }
);

// export default createAppContainer(AppNavigator);

const App = createDrawerNavigator(
  {
    "خریدهای قبلی": {
      screen: purchases,
      navigationOptions: {
        // drawerIcon: () => <Icon name="purchase" color="#FFBA00" size={20} style={{opacity : 1}}/>
      }
    },
    "کیف پول": {
      screen: Wallet,
      navigationOptions: {
        // drawerIcon: () => <Icon name="wallet" color="#FFBA00" size={20} />
      }
    },
    ویرایش: {
      screen: Edits,
      navigationOptions: {
        // drawerIcon: () => <Icon name="edits" color="#FFBA00" size={20} />
      }
    },
    "درباره سام سیر": {
      screen: About,
      navigationOptions: {
        drawerIcon: () => <Icon name="aboutUs" color="#FFBA00" size={20} />
      }
    },

    "تماس با پشتیبانی": {
      screen: Support,
      navigationOptions: {
        drawerIcon: () => <Icon name="support" color="#FFBA00" size={20} />
      }
    },
    "ارسال نظرات": {
      screen: SendFeedBack,
      navigationOptions: {
        drawerIcon: () => <Icon name="feedback" color="#FFBA00" size={20} />
      }
    },
    Navigations: {
      screen: AppNavigator,
      navigationOptions: {
        label: null
      }
    },
    ورود: {
      screen: LoginTest,
      navigationOptions: {
        label: null
      }
    }
  },
  {
    initialRouteName: "Navigations",
    drawerBackgroundColor: "#15479F",
    drawerLockMode: "locked-closed",
    drawerType: "front",
    drawerWidth: (deviceWidth * 84) / 100,
    contentOptions: {
      labelStyle: {
        fontFamily: "Sahel",
        fontSize: RFPercentage(2.2),
        color: "#ffffff"
      },
      itemStyle: {
        justifyContent: "flex-start",
        flexDirection: "row-reverse"
      },
      itemsContainerStyle: {
        flexDirection: "column"
      },
      iconContainerStyle: {}
    },
    // contentComponent: props => (
    //   <LinearGradient
    //   colors={["#15479F", "#183579", "#1B275A"]}
    //   // style={[styles.mainLinerGradient ]}
    //   style={{ flex: 1,paddingTop : 25 , padding:20}}
    // >
    // <View style={{ alignItems : 'flex-end'}}>
    //   <View>
    //     <Icon name='back' style={{color : '#FFFFFF'}}/>
    //   </View>
    //   <Image source={drawer} style={{alignSelf : 'center'}}/>
    //   <View style={{ alignSelf : 'center'}}>
    //     <Text style={{fontSize : RFPercentage(2.5) , color : '#FFBA00', paddingTop:10 }}>developers@samsair.com</Text>
    //   </View>
    //   </View>
    //   <View style={{borderWidth:0.5 , borderColor:'#FFBA00' , marginTop:10}}/>
    //     <DrawerItems
    //       {...props}
    //       onItemPress={(customItem, focused) => {
    //         props.onItemPress({ customItem, focused });
    //         alert("Under Construnction!!!");
    //       }}
    //     />
    //     </LinearGradient>
    // )
    contentComponent: ContentComponent
  }
);
const switchNavigator = createSwitchNavigator(
  {
    Splash: {
      screen: Splash
    },
    WalkThrough: {
      screen: WalkThrough
    },
    Drawer: {
      screen: App
    },
    AppNavigator: {
      screen: AppNavigator
    },
    Login: {
      screen: LoginTest
    }
  },
  {
    initialRouteName: "Splash"
  }
);

export default createAppContainer(switchNavigator);
