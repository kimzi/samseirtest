import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  AsyncStorage,
  StyleSheet
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { createIconSetFromFontello } from "react-native-vector-icons";
import fontelloConfig from "../../config.json";
const Icon = createIconSetFromFontello(fontelloConfig);
import LinearGradient from "react-native-linear-gradient";
import drawer from "../../Images/drawer.png";
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { USER_ID, IS_LOGIN_POSTPONED, USER_EMAIL } from "../../constants/UserParams";
class CustomDrawerContentComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: null,
      email : null
    };
  }
  async componentDidMount() {
    try {
      const value = await AsyncStorage.getItem(USER_ID);
      const Email = await AsyncStorage.getItem(USER_EMAIL);

      if (value !== null) {
        this.setState({ isLogin: true , email : Email });
        console.log(value);
      }
    } catch (error) {
      // Error retrieving data
    }
  }
  async logOut() {
    try {
      await AsyncStorage.removeItem(USER_ID);
      await AsyncStorage.removeItem(IS_LOGIN_POSTPONED);
      await AsyncStorage.removeItem(USER_EMAIL);

      // alert("شما با موفقیت خارج شدید");
      this.props.navigation.navigate("Login");
      return true;
    } catch (exception) {
      return false;
    }
  }

  render() {
    return (
      <LinearGradient
        // colors={["#15479F", "#183579", "#1B275A"]}
        colors={["#1B275A", "#183579", "#15479F"]}
        style={{ flex: 1, paddingHorizontal: 20 }}
      >
          <View style={{paddingTop : '5%'}}>
            <View>
              <Icon
                name="back"
                style={{ color: "#FFFFFF", padding: 5 }}
                onPress={() => this.props.navigation.closeDrawer()}
              />
            </View>
            <Image source={drawer} style={{ alignSelf: "center" }} />
            <View style={{ alignSelf: "center" }}>
              <Text
                style={{
                  fontSize: RFPercentage(2.5),
                  color: "#FFBA00",
                  paddingTop: 10
                }}
              >
                {this.state.email}
              </Text>
            </View>
          </View>
          <View
            style={{
              borderWidth: 0.5,
              borderColor: "#FFBA00",
              marginTop: 10
            }}
          />
              <ScrollView
              showsVerticalScrollIndicator={false}
            >
    
          {this.state.isLogin == true ? (
            <View>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("خریدهای قبلی")}
              >
                <View style={styles.itemContainer}>
                  <View style={styles.iconContainer}>
                    <Icon
                      name="purchase"
                      color="#FFBA00"
                      size={20}
                      style={{ opacity: 1 }}
                      style={styles.iconStyle}
                    />
                  </View>
                  <Text style={styles.itemText}>خریدهای قبلی</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("کیف پول")}
              >
                <View style={styles.itemContainer}>
                  <View style={styles.iconContainer}>
                    <Icon
                      name="wallet"
                      color="#FFBA00"
                      size={20}
                      style={{ opacity: 1 }}
                      style={styles.iconStyle}
                    />
                  </View>
                  <Text style={styles.itemText}>کیف پول</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("همراهان من")}
              >
                <View style={styles.itemContainer}>
                  <View style={styles.iconContainer}>
                    <Icon
                      name="passengers"
                      color="#FFBA00"
                      size={20}
                      style={{ opacity: 1 }}
                      style={styles.iconStyle}
                    />
                  </View>
                  <Text style={styles.itemText}>همراهان من</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("ویرایش")}
              >
                <View style={styles.itemContainer}>
                  <View style={styles.iconContainer}>
                    <Icon
                      name="edits"
                      color="#FFBA00"
                      size={20}
                      style={{ opacity: 1 }}
                      style={styles.iconStyle}
                    />
                  </View>
                  <Text style={styles.itemText}>ویرایش</Text>
                </View>
              </TouchableOpacity>
              <View
                style={{
                  borderWidth: 0.5,
                  borderColor: "#FFBA00",
                  marginTop: 10
                }}
              />
                        <TouchableOpacity
            onPress={() => this.props.navigation.navigate("درباره سام سیر")}
          >
            <View style={styles.itemContainer}>
              <View style={styles.iconContainer}>
                <Icon
                  name="aboutus"
                  color="#FFBA00"
                  size={20}
                  style={{ opacity: 1 }}
                  style={styles.iconStyle}
                />
              </View>
              <Text style={styles.itemText}>درباره سام سیر</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("تماس با پشتیبانی")}
          >
            <View style={styles.itemContainer}>
              <View style={styles.iconContainer}>
                <Icon
                  name="support"
                  color="#FFBA00"
                  size={20}
                  style={{ opacity: 1 }}
                  style={styles.iconStyle}
                />
              </View>
              <Text style={styles.itemText}>تماس با پشتیبانی</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("ارسال نظرات")}
          >
            <View style={styles.itemContainer}>
              <View style={styles.iconContainer}>
                <Icon
                  name="feedback"
                  color="#FFBA00"
                  size={20}
                  style={{ opacity: 1 }}
                  style={styles.iconStyle}
                />
              </View>
              <Text style={styles.itemText}>ارسال نظرات</Text>
            </View>
          </TouchableOpacity>
          {this.state.isLogin == true ? (
            <TouchableOpacity
              // onPress={() => this.props.navigation.navigate("خروج")}
              onPress={() => this.logOut()}
            >
              <View style={styles.itemContainer}>
                <View style={styles.iconContainer}>
                  <Icon
                    name="exit"
                    color="#FFBA00"
                    size={20}
                    style={{ opacity: 1 }}
                    style={styles.iconStyle}
                  />
                </View>
                <Text style={styles.itemText}>خروج</Text>
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("ورود" , {fromAvailable : false})}
            >
              <View style={styles.itemContainer}>
                <View style={styles.iconContainer}>
                  <Icon
                    name="exit"
                    color="#FFBA00"
                    size={20}
                    style={{ opacity: 1 }}
                    style={styles.iconStyle}
                  />
                </View>
                <Text style={styles.itemText}>ورود</Text>
              </View>
            </TouchableOpacity>
          )}

            </View>
          ) : null}

        </ScrollView>
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({
  itemText: {
    fontFamily: "Sahel",
    fontSize: RFPercentage(2),
    color: "#ffffff",
    padding: 10
  },
  itemContainer: {
    flexDirection: "row-reverse"
  },
  iconStyle: {
    paddingTop: 10,
    paddingBottom: 10
  },
  iconContainer: {
    width: 40,
    justifyContent: "center",
    alignItems: "flex-end"
  }
});

export default CustomDrawerContentComponent;
