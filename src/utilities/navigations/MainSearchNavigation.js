// import React, { Component } from "react";
// import { StyleSheet, View, Dimensions, Image ,Platform , PixelRatio , Text} from "react-native";
// import {
//   createMaterialTopTabNavigator,
//   createAppContainer,
//   createStackNavigator
// } from "react-navigation";
// import Hotel from "../../pages/MainSearch/Hotel";
// import Travel from "../../pages/MainSearch/Travel";
// import activeHotel from "../../Images/activeHotel.png";
// import activeTravel from "../../Images/activeTravel.png";
// import deactiveHotel from "../../Images/inactiveHotel.png";
// import deactiveTravel from "../../Images/inactiveTravel.png";
// import { createIconSetFromFontello } from "react-native-vector-icons";
// import fontelloConfig from "../../config.json";
// const Icon = createIconSetFromFontello(fontelloConfig);
// import RF from "react-native-responsive-fontsize";
// import FlightOptions from "./FlightOptions";
// import { TabBarBottom, TabBarTop } from 'react-navigation';
// const TabBar = Platform.OS === 'ios' ? TabBarBottom : TabBarTop;


// import MenuDrawerNavigation from './MenuDrawerNavigation' ;
// const MainSearchNavigation = createMaterialTopTabNavigator(
// {
//     هتل: {
//       screen: Hotel,
//       navigationOptions: {
//         header: null,
//         headerMode: "none",
//         swipeEnabled: true,
//         tabBarIcon: ({ focused }) => {
//           const image = focused ? activeHotel : deactiveHotel;
//           return <Image source={image}  />;
//         }
//       }
//     },
//     "پرواز(داخلی/خارجی)": {
//       screen: FlightOptions,
//       navigationOptions: {

//         header: null,
//         headerMode: "none",
//         swipeEnabled: true,
//         tabBarIcon: ({ focused }) => {
//           const image = focused ? activeTravel : deactiveTravel;
//           return <Image source={image} />;
//         }
//       }
//     }
//   },
//   {
//     initialRouteName: "پرواز(داخلی/خارجی)",
//     lazy : true ,
//     tabBarPosition: "top",
//     // swipeEnabled: true,
//     tabBarOptions: {
//       allowFontScaling: true,
//       showIcon: true,
//       showLabel: PixelRatio.get() <2 ? false : true,
//       style: {
//         elevation: 0,
//         height: PixelRatio.get() <2 ? 60 : 90,
//         backgroundColor: "white",
//         flexDirection: "column",
//         width : '80%',
//         marginLeft : '20%'
//       },
//       labelStyle: {
//         fontSize: RF(1.7),
//         fontFamily: "Sahel-Bold",
//         marginTop: 5
//       },
//       tabStyle: {
//         paddingTop: 20,
//         backgroundColor: "#1B275A",
//         flexDirection: "column",
//         height: 90,
//       },
//       indicatorStyle: {
//         height: 0
//       }
//     }
//   },
//   {
//     lazy : true ,
//     headerMode: "none",
//     navigationOptions: {
//       headerVisible: false
//     }
//   }
// );



// export default createAppContainer(MainSearchNavigation);
