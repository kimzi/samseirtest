import React, { Component } from "react";
import { StyleSheet, View, Dimensions, Image } from "react-native";
import {
  createMaterialTopTabNavigator,
  createAppContainer,
  createStackNavigator
} from "react-navigation";

let deviceWidth = Dimensions.get("window").width;
let deviceHeight = Dimensions.get("window").height;
import Hotel from "./MainSearch/Hotel";
import MainSearchNavigation from "./MainSearchNavigation/";
import Calendar from "./MainSearch/Calendar";
import activeHotel from "./Images/activeHotel.png";
import activeTravel from "./Images/activeTravel.png";
import deactiveHotel from "./Images/inactiveHotel.png";
import deactiveTravel from "./Images/inactiveTravel.png";

const AppNavigator = createStackNavigator(
  {
    Calendar: Calendar,
    MainSearchNavigation: MainSearchNavigation
  },
  {
    initialRouteName: "MainSearchNavigation"
  },
  {
    headerMode: "none",
    navigationOptions: {
      headerVisible: false
    }
  }
);

export default createAppContainer(AppNavigator);
